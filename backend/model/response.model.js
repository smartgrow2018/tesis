var makeResult = function (type, message) {
    let code = 0;
    switch (type) {
        case 'error':
            code = 0;
            break;
        case 'success':
            code = 1;
            break;
    }
    var response = {
        "code": code,
        "message": message
    }
    return response;
};

exports.getResponse = function (type, message, data) {
    return {
        "result": makeResult(type, message),
        "data": data
    };
};