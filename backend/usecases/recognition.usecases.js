const recognitionRepository = require('../repositories/http.recognition');
const recognitionRepositoryDB = require('../repositories/db.recognition');
const response = require('../model/response.model');
const util = require('../util/util');

const saveRecognition = async function (data, urlImage, idUser) {
    recognitionRepositoryDB.saveRecognition(data, urlImage, idUser).then((results) => {
        util.consoleLog("process correct", results);
    }).catch((error) => {
        util.consoleLog("process error ", error);
    });
}

/**
 * Actualiza la prediccion con el plague_id basado en el concept
 * @param {*} results 
 */
const updateRecognition = async function (results, urlImage, idUser) {
    try {
        let data = []
        await Promise.all(results.map(async (rs) => {
            const contents = await recognitionRepositoryDB.getRecognitionObject(rs);
            (contents.plague_id !== undefined && Number(contents.value) !== 0.0000) ? data.push(contents): "";
        }));
        if (data.length <= 0) {
            data = [];
            //util.consoleLog("NOT_DATA_RESULTS");
            //throw "NOT_DATA_RESULTS";
        }
        saveRecognition(data, urlImage, idUser);
        return response.getResponse("success", "success", data);
    } catch (error) {
        util.consoleLog("error ", error);
        return new Promise((resolve, reject) => {
            reject(response.getResponse("error", error, {}));
        });
    }
};

exports.recognize = function (obj) {
    return new Promise((resolve, reject) => {
        recognitionRepository.recognize(obj).then((results) => {
            if (results.status.code === 10000 && results.status.description === "Ok") {
                results = results.outputs[0].data.concepts;
                resolve(updateRecognition(results, obj.urlImage, obj.idUser));
            } else {
                reject(response.getResponse("error", results, {}));
            }
        }).catch((error) => {
            reject(response.getResponse("error", error, {}));
        })
    });
};