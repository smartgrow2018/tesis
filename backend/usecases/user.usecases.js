var userRepository = require('../repositories/db.user');
var requestUtil = require('../util/requestUtil');
var response = require('../model/response.model');

exports.createUser = function (userObject) {
    return new Promise(function (resolve, reject) {
        userRepository.validUsername(userObject.username_user).then((valid) => {
            userRepository.createUser(userObject).then(function (results) {
                resolve(response.getResponse("success", "success", results));
            }).catch(function (error) {
                reject(response.getResponse("error", error, {}));
            });
        }).catch((error) => {
            reject(response.getResponse("error", error, {}));
        });
    });
};

exports.editUser = function (code_user, name) {
    return new Promise(function (resolve, reject) {
        userRepository.editUser(code_user, name).then(function (results) {
            resolve(response.getResponse("success", "success", results));
        }).catch(function (error) {
            reject(response.getResponse("error", error, {}));
        });
    });
};

exports.loginUser = function (username, password) {
    return new Promise(function (resolve, reject) {
        userRepository.loginUser(username, password).then(function (results) {
            resolve(response.getResponse("success", "success", results));
        }).catch(function (error) {
            reject(response.getResponse("error", error, {}));
        });
    });
};

exports.infoUser = async function (username) {
    try {
        let results = await userRepository.infoUser(username);
        return response.getResponse("success", "success", results);
    } catch (error) {
        return new Promise((resolve, reject) => {
            reject(response.getResponse("error", error, {}));
        });
    }
};