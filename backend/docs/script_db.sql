-- create database tesis; 
use tesis;

create table user (
 code_user int not null auto_increment,
 username_user varchar(150) not null unique,
 password_user varchar(150) not null,
 name_user varchar(150) not null,
 email_user varchar(150) null unique,
 create_date_user datetime not null default CURRENT_TIMESTAMP,
 update_date_user datetime not null default CURRENT_TIMESTAMP,
 constraint codigo_user_pk primary key (code_user)
) ENGINE=INNODB;
