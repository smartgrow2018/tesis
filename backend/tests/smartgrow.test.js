process.env.ENVIROMENT = 'TEST';

let expect = require('chai').expect;
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../apptesis');
let should = chai.should();
chai.use(chaiHttp);
let _ = require('lodash');

const HTTP_STATUS = {
    OK: 200,
    CREATED: 201,
    NO_CONTENT: 204,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    UNPROCESSABLE: 422,
    TOO_MANY_REQUEST: 429
};

describe('Users', () => {
    /*before(done => {
        util.consoleLog('\n\n-------------------------\n--\n-- START TEST\n--\n-------------------------');
        done();
    });
    after(done => {
        util.consoleLog('\n\n-------------------------\n--\n-- END TEST\n--\n-------------------------');
        done();
    });*/
    describe('/POST users', () => {
        it('it create account user', (done) => {
            const USER_ACCOUNT = {
                username: "test",
                password: "test",
                name: "test",
                email: "test@smartgrow.com"
            }
            done();
            /*chai.request(server)
                .post('/users')
                .send(USER_ACCOUNT)
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });*/
        });
    });
    describe('/POST login ', () => {
        const STRINGS_LOGIN = {
            SUCCESS_CODE: 1,
            WARNING_CODE: 0,
            SUCCESS: "success",
            WRONG_USERNAME_OR_PASSWORD: "WRONG_USERNAME_OR_PASSWORD",
            INVALID_PARAMETER_SIZE: "INVALID_PARAMETER_SIZE"
        };
        it('it login user correct', (done) => {
            let USER_LOGIN = {
                username: "molivars",
                password: "1234"
            }
            chai.request(server)
                .post('/login')
                .send(USER_LOGIN)
                .end((err, res) => {
                    res.should.have.status(HTTP_STATUS.OK);
                    res.body.should.be.a('object');
                    res.body.should.have.property('result');
                    res.body.result.should.have.property('code').eql(STRINGS_LOGIN.SUCCESS_CODE);
                    res.body.result.should.have.property('message').eql(STRINGS_LOGIN.SUCCESS);
                    res.body.data.should.be.a('object');
                    done();
                });
        });
        it('it login user password incorrect', (done) => {
            let USER_LOGIN = {
                username: "test",
                password: "incorrect"
            }
            chai.request(server)
                .post('/login')
                .send(USER_LOGIN)
                .end((err, res) => {
                    res.should.have.status(HTTP_STATUS.UNPROCESSABLE);
                    res.body.should.be.a('object');
                    res.body.should.have.property('result');
                    res.body.result.should.have.property('code').eql(STRINGS_LOGIN.WARNING_CODE);
                    res.body.result.should.have.property('message').eql(STRINGS_LOGIN.WRONG_USERNAME_OR_PASSWORD);
                    done();
                });
        });
        it('it login user bad request', (done) => {
            let USER_LOGIN = {
                username: "test"
            }
            chai.request(server)
                .post('/login')
                .send(USER_LOGIN)
                .end((err, res) => {
                    res.should.have.status(HTTP_STATUS.UNPROCESSABLE);
                    res.body.should.be.a('object');
                    res.body.should.have.property('result');
                    res.body.result.should.have.property('code').eql(STRINGS_LOGIN.WARNING_CODE);
                    res.body.result.should.have.property('message').eql(STRINGS_LOGIN.INVALID_PARAMETER_SIZE);
                    done();
                });
        });
    });
});

describe('Recognition', () => {
    describe('/POST :user/recognitions ', () => {
        const STRINGS_LOGIN = {
            SUCCESS_CODE: 1,
            WARNING_CODE: 0,
            SUCCESS: "success",
            WRONG_USERNAME_OR_PASSWORD: "WRONG_USERNAME_OR_PASSWORD",
            INVALID_PARAMETER_SIZE: "INVALID_PARAMETER_SIZE"
        };
        it('it recognition correct', (done) => {
            let USER_LOGIN = {
                username: "molivars",
                password: "1234"
            }
            chai.request(server)
                .post('/login')
                .send(USER_LOGIN)
                .end((err, res) => {
                    res.should.have.status(HTTP_STATUS.OK);
                    res.body.should.be.a('object');
                    res.body.should.have.property('result');
                    res.body.result.should.have.property('code').eql(STRINGS_LOGIN.SUCCESS_CODE);
                    res.body.result.should.have.property('message').eql(STRINGS_LOGIN.SUCCESS);
                    res.body.data.should.be.a('object');
                    done();
                });
        });
        it('it recognition ', (done) => {
            let USER_LOGIN = {
                username: "test",
                password: "incorrect"
            }
            chai.request(server)
                .post('/login')
                .send(USER_LOGIN)
                .end((err, res) => {
                    res.should.have.status(HTTP_STATUS.UNPROCESSABLE);
                    res.body.should.be.a('object');
                    res.body.should.have.property('result');
                    res.body.result.should.have.property('code').eql(STRINGS_LOGIN.WARNING_CODE);
                    res.body.result.should.have.property('message').eql(STRINGS_LOGIN.WRONG_USERNAME_OR_PASSWORD);
                    done();
                });
        });
        it('it recognition bad request', (done) => {
            let USER_LOGIN = {
                username: "test"
            }
            chai.request(server)
                .post('/login')
                .send(USER_LOGIN)
                .end((err, res) => {
                    res.should.have.status(HTTP_STATUS.UNPROCESSABLE);
                    res.body.should.be.a('object');
                    res.body.should.have.property('result');
                    res.body.result.should.have.property('code').eql(STRINGS_LOGIN.WARNING_CODE);
                    res.body.result.should.have.property('message').eql(STRINGS_LOGIN.INVALID_PARAMETER_SIZE);
                    done();
                });
        });
    });
});