var express = require('express');
var userRouter = express.Router();
var util = require('../../util/util');


// middleware that is specific to this router
userRouter.use(function timeLog(req, res, next) {
    util.consoleLog('<-', req.path.substring(1, req.path.length));
    next();
});

// Applying middleware to all routes in the router
userRouter.use(function (req, res, next) {
    var token = req.headers['authorization'];
    util.consoleLog(token);
    if (req.user === 'mayer') {
        next()
    } else {
        res.status(403).send('Forbidden')
    }
});

userRouter.route('/user')
    .get(function (req, res) {
        util.consoleLog("user GET");
        res.json({
            "returnBook": "hello"
        });
    })
    .post(function (req, res) {
        res.send("POST METHOD");
    });

userRouter.post('/login', function (req, res) {
    res.send("users GET");
});

module.exports = userRouter;