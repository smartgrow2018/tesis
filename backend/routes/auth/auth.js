const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const express = require('express');
const authRouter = express.Router();
var util = require('../../util/util');

/**
 * Crear Hash de contraseña
 * Almacenar usuario
 * Crear Token
 */
authRouter.post('/register', function (req, res) {
    util.consoleLog("<- register");
    var hashedPassword = bcrypt.hashSync(req.body.password, 8);
    // create a token
    var token = jwt.sign({
        username: req.body.username
    }, "secret", {
        expiresIn: 86400 // expires in 24 hours 86400 seg
    });
    res.status(200).send({
        auth: true,
        token: token
    });
});

authRouter.get('/secure', (req, res) => {
    var token = req.headers['authorization'];
    if (!token) {
        res.status(401).send({
            error: "Es necesario el token de autenticación"
        })
        return;
    }
    token = token.replace('Bearer ', '');
    jwt.verify(token, "secret", function (err, user) {
        if (err) {
            res.status(401).send({
                error: err.message
            });
        } else {
            res.send({
                message: 'Awwwww yeah!!!!',
                user: user
            });
        }
    });
});

module.exports = authRouter;