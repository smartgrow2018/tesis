var dbPool = require('./db_pool.mysql');
var util = require('../util/util');


exports.createUser = function (userObject) {
    return new Promise(function (resolve, reject) {
        var sql = "insert into user (username, password, name, email) values (?,?,?,?)";
        dbPool.connection.query(sql, [userObject.username_user, userObject.password_user, userObject.name_user, userObject.email_user]).on('result', function (results) {
            (results !== null && results !== undefined && results.affectedRows > 0) ? resolve({}): reject("DB_USER_CREATEUSER");
        }).on('error', function (err) {
            util.consoleLog("ERROR: db.user.js in createUser: ", err);
            reject("EXISTS_USERNAME_OR_EMAIL");
        });
    });
};

exports.editUser = function (code_user, name) {
    return new Promise(function (resolve, reject) {
        var sql = "update user set name_user = ? where code_user = ?";
        dbPool.connection.query(sql, [name, code_user]).on('result', function (results) {
            (results !== null && results !== undefined && results.affectedRows > 0) ? resolve({}): reject("DB_USER_EDITUSER");
        }).on('error', function (err) {
            util.consoleLog("ERROR: db.user.js in editUser: ", err);
            reject("DB_USER_EDITUSER");
        });
    });
};

exports.loginUser = function (username, password) {
    return new Promise(function (resolve, reject) {
        var sql = "select id as code_user, username as username_user, " +
            " password as password_user, name as name_user, email as email_user from user " +
            " where username = ? and password = ?";
        dbPool.connection.query(sql, [username, password]).on('result', function (results) {
            (results !== null && results !== undefined && results.length > 0) ? resolve(results[0]): reject("WRONG_USERNAME_OR_PASSWORD");
        }).on('error', function (err) {
            util.consoleLog("ERROR: db.user.js in loginUser: ", err);
            reject("DB_USER_LOGINUSER");
        });
    });
};

exports.infoUser = function (username) {
    return new Promise(function (resolve, reject) {
        let sql = "select id as idUser from user where username = ?;";
        dbPool.connection.query(sql, [username]).on('result', function (results) {
            (results !== null && results !== undefined && results.length > 0) ? resolve(results[0]): reject("NOT_FOUND_USER");
        }).on('error', function (err) {
            util.consoleLog("ERROR: db.user.js in infoUser: ", err);
            reject("DB_USER_INFOUSER");
        });
    });
};

exports.validUsername = function (username) {
    return new Promise(function (resolve, reject) {
        let sql = "select id as idUser from user where username = ?;";
        dbPool.connection.query(sql, [username]).on('result', function (results) {
            (results !== null && results !== undefined && results.length > 0) 
            ? reject("USER_EXIST"): resolve(true);
        }).on('error', function (err) {
            util.consoleLog("ERROR: db.user.js in infoUser: ", err);
            reject("DB_USER_INFOUSER");
        });
    });
};