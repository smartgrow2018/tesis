var mysql = require('mysql');
var config = require('../config.json');
var util = require('../util/util');
let conect = {
    connectionLimit: 10,
    host: config.mysql_host,
    user: config.mysql_user,
    password: config.mysql_pass,
    database: config.mysql_name
};
var pool = mysql.createPool(conect);

exports.pool = pool;
process.on('uncaughtException', function (err) {
    pool = mysql.createPool(conect);
    util.consoleLog('error', 'UNCAUGHT EXCEPTION - keeping process alive:', err);
});

pool.query('SELECT 1 + 1 AS solution', function (error, results, fields) {
    if (error) throw error;
    util.consoleLog('The solution is: ', results[0].solution);
});

exports.connection = (function () {
    var _query = function (query, params) {
        var eventNameIndex = {};
        pool.getConnection(function (err, connection) {
            if (err) {
                if (typeof connection !== 'undefined' && connection) {
                    connection.release();
                }
                util.consoleLog("err: ", err);
                util.consoleLog("connection: ", connection);
                if (eventNameIndex.error) {
                    util.consoleLog("error", err);
                    eventNameIndex.error(err);
                }
            }
            connection.query(query, params, function (err, rows) {
                connection.release();
                if (!err) {
                    eventNameIndex.result(rows);
                } else {
                    if (eventNameIndex.error) {
                        eventNameIndex.error(err);
                    }
                }
            });
            connection.on('error', function (err) {
                connection.release();
                if (eventNameIndex.error) {
                    util.consoleLog("error", err);
                    eventNameIndex.error(err);
                }
                connection.removeListener(this);
            });
        });
        return {
            on: function (eventName, callback) {
                eventNameIndex[eventName] = callback;
                return this;
            }
        };
    };
    return {
        query: _query
    };
})();