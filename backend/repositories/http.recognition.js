var requestUtil = require('../util/requestUtil');
var config = require('../config.json');
const Clarifai = require('clarifai');
var util = require('../util/util');

var data = {
    "status": {
        "code": 10000,
        "description": "Ok"
    },
    "outputs": [{
        "id": "a006cd6703ca45349272cccad98f9a89",
        "status": {
            "code": 10000,
            "description": "Ok"
        },
        "created_at": "2018-10-07T00:20:06.097266066Z",
        "model": {
            "id": "demo",
            "name": "demo",
            "created_at": "2018-08-22T20:05:25.519990Z",
            "app_id": "df8dcc3497d2430189cf0bd2164646f5",
            "output_info": {
                "output_config": {
                    "concepts_mutually_exclusive": false,
                    "closed_environment": false,
                    "max_concepts": 0,
                    "min_value": 0
                },
                "message": "Show output_info with: GET /models/{model_id}/output_info",
                "type": "concept",
                "type_ext": "concept"
            },
            "model_version": {
                "id": "1a6fbbd8a89840fb8b054d0b58ebb748",
                "created_at": "2018-08-27T21:38:08.888242Z",
                "status": {
                    "code": 21100,
                    "description": "Model trained successfully"
                },
                "total_input_count": 2
            }
        },
        "input": {
            "id": "7a88068b5cb344aa98e53d5ecab27f00",
            "data": {
                "image": {
                    "url": "https://www.diariomotor.com/imagenes/picscache/1920x1600c/ferrari-458-italia-1010_1920x1600c.jpg"
                }
            }
        },
        "data": {
            "concepts": [{
                    "id": "demo",
                    "name": "demo",
                    "value": 0.07372695,
                    "app_id": "df8dcc3497d2430189cf0bd2164646f5"
                },
                {
                    "id": "ferrari",
                    "name": "ferrari",
                    "value": 4.027603e-11,
                    "app_id": "df8dcc3497d2430189cf0bd2164646f5"
                }
            ]
        }
    }],
    "rawData": {
        "status": {
            "code": 10000,
            "description": "Ok"
        },
        "outputs": [{
            "id": "a006cd6703ca45349272cccad98f9a89",
            "status": {
                "code": 10000,
                "description": "Ok"
            },
            "created_at": "2018-10-07T00:20:06.097266066Z",
            "model": {
                "id": "demo",
                "name": "demo",
                "created_at": "2018-08-22T20:05:25.519990Z",
                "app_id": "df8dcc3497d2430189cf0bd2164646f5",
                "output_info": {
                    "output_config": {
                        "concepts_mutually_exclusive": false,
                        "closed_environment": false,
                        "max_concepts": 0,
                        "min_value": 0
                    },
                    "message": "Show output_info with: GET /models/{model_id}/output_info",
                    "type": "concept",
                    "type_ext": "concept"
                },
                "model_version": {
                    "id": "1a6fbbd8a89840fb8b054d0b58ebb748",
                    "created_at": "2018-08-27T21:38:08.888242Z",
                    "status": {
                        "code": 21100,
                        "description": "Model trained successfully"
                    },
                    "total_input_count": 2
                }
            },
            "input": {
                "id": "7a88068b5cb344aa98e53d5ecab27f00",
                "data": {
                    "image": {
                        "url": "https://www.diariomotor.com/imagenes/picscache/1920x1600c/ferrari-458-italia-1010_1920x1600c.jpg"
                    }
                }
            },
            "data": {
                "concepts": [{
                        "id": "demo",
                        "name": "demo",
                        "value": 0.07372695,
                        "app_id": "df8dcc3497d2430189cf0bd2164646f5"
                    },
                    {
                        "id": "ferrari",
                        "name": "ferrari",
                        "value": 4.027603e-11,
                        "app_id": "df8dcc3497d2430189cf0bd2164646f5"
                    }
                ]
            }
        }]
    }
};

exports.recognize = function (obj) {
    util.consoleLog(obj);
    return new Promise((resolve, reject) => {
        const clariApp = new Clarifai.App({
            apiKey: '2dbf121e9b634d64aa1bd171f2683bfe'
        });
        resolve(data);
        return;
        util.consoleLog("call clarifai");
        clariApp.models.predict(config.clari_model, obj.urlImage, {
            maxConcepts: 5
        }).then((response) => {
            util.consoleLog(response);
            resolve(response);
        }).catch((error) => {
            util.consoleLog(error);
            reject(error);
        });

        /*var headers = {
            "Authorization": 'Key 2dbf121e9b634d64aa1bd171f2683bfe',
            "Content-Type": "application/json"
        };
        var body = {
            "inputs": [{
                "data": {
                    "image": {
                        "url": obj.urlImage
                    }
                }
            }]
        };
        var path = config.clari_path + "demo/outputs";
        requestUtil.httpsRawRequest(config.clari_host, config.clari_port,
            path, 'POST', headers, body,
            function (response) {
                resolve(response);
            },
            function (error) {
                util.consoleLog(error);
                reject(error);
            });*/
    });
};