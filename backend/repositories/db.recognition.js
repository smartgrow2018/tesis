const dbPool = require('./db_pool.mysql');
const util = require('../util/util');

var saveResults = function (data) {
    return new Promise(function (resolve, reject) {
        if (data.length <= 0) {
            resolve("SUCCESS_NOT_RESULTS");
            return;
        }
        util.consoleLog(data);
        let sql = "INSERT INTO result (concept, value, plague_id, query_id) VALUES ? ;";
        dbPool.connection.query(sql, [data]).on('result', function (results) {
            if (results !== null && results !== undefined && results.affectedRows > 0) {
                resolve(results);
            } else {
                reject("ERROR_NOT_REGISTERED");
            }
        }).on('error', function (err) {
            util.consoleLog("ERROR: db.predict.js in saveResults: ", err);
            reject("DB_ERROR");
        });
    });
};

exports.saveRecognition = function (data, urlImage, idUser) {
    return new Promise(function (resolve, reject) {
        let sql = "INSERT INTO query (url_image, user_id ) VALUES (?,?) ;";
        dbPool.connection.query(sql, [urlImage, idUser]).on('result', function (results) {
            if (results !== null && results !== undefined && results.affectedRows > 0) {
                let params = [];
                let item = [];
                // validar que haya resultados, pero al final si registra la consulta.
                for (let i = 0; i < data.length; i++) {
                    item.push(data[i].name_clarifai);
                    item.push(data[i].value);
                    item.push(data[i].plague_id);
                    item.push(results.insertId);
                    params.push(item);
                    item = [];
                }
                resolve(saveResults(params));
            } else {
                reject(util.makeObjectError("warning", "ERROR_NOT_REGISTERED"));
            }
        }).on('error', function (err) {
            util.consoleLog("ERROR: db.predict.js in savePredict: ", err);
            reject("DB_ERROR");
        });
    });
};

exports.getPlagueId = function (concept) {
    return new Promise(function (resolve, reject) {
        let sql = "select id as plague_id from plague where name = ? limit 1;";
        dbPool.connection.query(sql, [concept]).on('result', function (results) {
            (results !== null && results !== undefined && results.length > 0) ? resolve(results[0]):
                reject("NOT_FOUND");
        }).on('error', function (err) {
            util.consoleLog("ERROR: db.predict.js in getPlagueId: ", err);
            reject("DB_PREDICT");
        });
    });
};

exports.getRecognitionObject = function (obj) {
    return new Promise(function (resolve, reject) {
        let sql = "select id as plague_id, url_image, name from plague where name_clarifai = ? limit 1;";
        dbPool.connection.query(sql, [obj.name]).on('result', function (results) {
            if (results !== null && results !== undefined && results.length > 0) {
                obj.plague_id = results[0].plague_id;
                obj.name_clarifai = obj.name;
                obj.name = results[0].name;
                obj.url_image_plague = results[0].url_image;
                obj.value = util.number(obj.value, 4);
                obj.app_id = undefined;
                obj.id = undefined;
                resolve(obj);
            } else {
                resolve(obj);
            }
        }).on('error', function (err) {
            util.consoleLog("ERROR: db.predict.js in getPlagueId: ", err);
            reject("DB_PREDICT");
        });
    });
};