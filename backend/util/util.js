exports.asyncForEach = async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array)
    }
}

exports.trunc = function (x, posiciones = 0) {
    var s = x.toString()
    var l = s.length
    var decimalLength = s.indexOf('.') + 1
    var numStr = s.substr(0, decimalLength + posiciones)
    return Number(numStr)
}

exports.number = function (x, posiciones = 0) {
    try {
        return Number(x).toFixed(4);
    } catch (error) {
        return 0;
    }
}


exports.cleanDuplicate = function (array, prop) {
    let newArray = [];
    let lookupObject = {};
    for (var i in array) {
        lookupObject[array[i][prop]] = array[i];
    }
    for (i in lookupObject) {
        newArray.push(lookupObject[i]);
    }
    return newArray;
};

exports.clone = function (obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
};

//configuration
const ENV = {
    PRODUCCTION: "PROD",
    DEVELOPMENT: "DEV",
    TESTING: "TEST"
}
let enviroment = process.env.ENVIROMENT || ENV.PRODUCCTION;

exports.consoleLog = (msg) => {
    if (enviroment === ENV.TESTING) {
        return;
    }
    console.log(msg);
}