var querystring = require('querystring');
var http = require('http');
var https = require('https');
var requestLib = require('request');

var request = function (type, host, port, path, method, headers, parameters, endCallback, errorCallback) {
    var post_data = null;
    if (method.toLowerCase() === 'post' && parameters !== undefined && parameters !== null && JSON.stringify(parameters) !== JSON.stringify({})) {
        post_data = querystring.stringify(parameters);
        if (headers === undefined || headers === null) {
            headers = {};
        }
        headers["Content-Type"] = "application/x-www-form-urlencoded";
        headers["Content-Length"] = Buffer.byteLength(post_data);
    }
    var options = {
        host: host,
        port: port,
        path: path,
        method: method,
        headers: headers
    };
    var buffer = '';
    var request = null;
    var requestCallback = function (result) {
        result.setEncoding('utf8');
        var onData = function (chunk) {
            buffer += chunk;
        };
        result.on('data', onData);
        result.once('end', function () {
            //result.removeListener(onData);
            endCallback(buffer);
        });
    };
    if (type.toLowerCase() === "http") {
        request = http.request(options, requestCallback);
    } else if (type.toLowerCase() === "https") {
        request = https.request(options, requestCallback);
    }
    request.once('error', function (e) {
        errorCallback(e);
    });
    if (post_data !== null) {
        request.write(post_data);
    }
    request.end();
};

var rawRequest = function (type, host, port, path, method, headers, body, endCallback, errorCallback) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    var vars = {
        url: type + '://' + host + ':' + port + path,
        method: method,
        headers: headers,
        body: JSON.stringify(body)
    };
    requestLib(vars, function (error, response, body) {
        if (error) {
            errorCallback(error);
        } else if (response.statusCode >= 400) {
            errorCallback('HTTP Error: ' + response.statusCode + ' - ' + response.statusMessage);
        } else {
            endCallback(body);
        }
    });
};

exports.httpRequest = function (host, port, path, method, headers, parameters, endCallback, errorCallback) {
    request("http", host, port, path, method, headers, parameters, endCallback, errorCallback);
};
exports.httpsRequest = function (host, port, path, method, headers, parameters, endCallback, errorCallback) {
    request("https", host, port, path, method, headers, parameters, endCallback, errorCallback);
};

exports.httpRawRequest = function (host, port, path, method, headers, body, endCallback, errorCallback) {
    rawRequest("http", host, port, path, method, headers, body, endCallback, errorCallback);
};
exports.httpsRawRequest = function (host, port, path, method, headers, body, endCallback, errorCallback) {
    rawRequest("https", host, port, path, method, headers, body, endCallback, errorCallback);
};