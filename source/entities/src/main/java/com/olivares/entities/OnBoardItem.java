package com.olivares.entities;

public class OnBoardItem {
    private String title;
    private String description;
    private int image;
    private String animation;

    public OnBoardItem(String title, String description, int image, String animation) {
        this.title = title;
        this.description = description;
        this.image = image;
        this.animation = animation;
    }

    public OnBoardItem() {
    }

    public String getAnimation() {
        return animation;
    }

    public void setAnimation(String animation) {
        this.animation = animation;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
