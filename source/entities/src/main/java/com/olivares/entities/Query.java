package com.olivares.entities;

import java.util.ArrayList;
import java.util.List;

public class Query {
    private int queryId;
    private String urlImageQuery;
    private String date;
    private List<Result> resultList;

    public Query(String urlImageQuery) {
        this.urlImageQuery = urlImageQuery;
        this.resultList = new ArrayList<>();
    }

    public Query() {
        this.resultList = new ArrayList<>();
    }

    public String getUrlImageQuery() {
        return urlImageQuery;
    }

    public void setUrlImageQuery(String urlImageQuery) {
        this.urlImageQuery = urlImageQuery;
    }

    public List<Result> getResultList() {
        return resultList;
    }

    public void addResult(Result result) {
        this.resultList.add(result);
    }

    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(int queryId) {
        this.queryId = queryId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


}
