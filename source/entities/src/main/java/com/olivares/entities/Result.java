package com.olivares.entities;

public class Result {
    private double percentResult;
    private String namePlague;
    private String descriptionPlague;
    private String nameClarifai;
    private String urlImagePlague;
    private int plagueId;

    public Result() {
        // no require implementation
    }

    public Result(int plagueId, String namePlague, String descriptionPlague,
                  double percentResult, String nameClarifai, String urlImagePlague) {
        this.plagueId = plagueId;
        this.percentResult = percentResult;
        this.namePlague = namePlague;
        this.descriptionPlague = descriptionPlague;
        this.nameClarifai = nameClarifai;
        this.urlImagePlague = urlImagePlague;
    }

    public int getPlagueId() {
        return plagueId;
    }

    public void setPlagueId(int plagueId) {
        this.plagueId = plagueId;
    }

    public double getPercentResult() {
        return percentResult;
    }

    public void setPercentResult(double percentResult) {
        this.percentResult = percentResult;
    }

    public String getNamePlague() {
        return namePlague;
    }

    public void setNamePlague(String namePlague) {
        this.namePlague = namePlague;
    }

    public String getDescriptionPlague() {
        return descriptionPlague;
    }

    public void setDescriptionPlague(String descriptionPlague) {
        this.descriptionPlague = descriptionPlague;
    }

    public String getUrlImagePlague() {
        return urlImagePlague;
    }

    public void setUrlImagePlague(String urlImagePlague) {
        this.urlImagePlague = urlImagePlague;
    }

    public String getNameClarifai() {
        return nameClarifai;
    }

    public void setNameClarifai(String nameClarifai) {
        this.nameClarifai = nameClarifai;
    }
}
