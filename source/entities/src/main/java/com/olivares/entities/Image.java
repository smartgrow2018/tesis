package com.olivares.entities;

public class Image {
    private String link;
    private String name;
    private String imageData;

    public Image(String link) {
        this.link = link;
    }

    public Image(String name, String imagedata) {
        this.name = name;
        this.imageData = imagedata;
    }


    public Image() {
        // no require implementation
    }


    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageData() {
        return imageData;
    }

    public void setImageData(String imageData) {
        this.imageData = imageData;
    }
}
