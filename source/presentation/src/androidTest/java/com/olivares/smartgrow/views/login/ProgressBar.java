package com.olivares.smartgrow.views.login;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;

public class ProgressBar extends android.widget.ProgressBar {
    public ProgressBar(Context context) {
        super(context);
        setUpView();
    }

    public ProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        setUpView();
    }

    public ProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setUpView();
    }

    private void setUpView() {
        this.setVisibility(GONE);
    }

    @Override
    public void setVisibility(int v) {
        // Progressbar should never show
        v = GONE;
        super.setVisibility(v);
    }

    @Override
    public void startAnimation(Animation animation) {
        // Do nothing in test cases, to not block ui thread
    }
}
