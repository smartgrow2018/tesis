package com.olivares.smartgrow.views;

public final class Util {
    public static void await(int sleep) throws Exception {
        Thread.sleep(sleep);
    }
}
