package com.olivares.smartgrow.views.camera;


/**
 * Created by User on 6/5/2018.
 */

public interface IMainActivity {

    void setCameraBackFacing();

    boolean isCameraBackFacing();

    void setBackCameraId(String cameraId);

    String getBackCameraId();

    void hideStatusBar();

    void showStatusBar();

    void showAlert();
}
