package com.olivares.smartgrow.views.onboarding;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.olivares.smartgrow.R;
import com.olivares.smartgrow.di.components.ApplicationComponent;
import com.olivares.smartgrow.di.components.DaggerOnBoardingComponent;
import com.olivares.smartgrow.di.components.OnBoardingComponent;
import com.olivares.smartgrow.di.injector.DependencyInjector;
import com.olivares.entities.OnBoardItem;
import com.olivares.smartgrow.views.login.LoginActivity;
import com.olivares.smartgrow.views.register.RegisterActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OnBoardingActivity extends AppCompatActivity implements OnBoardingPresenter.OnBoardingView {
    private static final String TAG = OnBoardingActivity.class.getName();
    @BindView(R.id.pager_introduction)
    ViewPager onboardpager;
    @BindView(R.id.btn_get_started)
    Button btnGetStarted;
    @BindView(R.id.btn_next)
    Button btnNext;
    int previousPos = 0;
    @BindView(R.id.viewPagerCountDots)
    LinearLayout pagerIndicator;
    private int dotsCount;
    private ImageView[] dots;
    @BindView(R.id.create_account_onboard)
    TextView createAccount;

    @Inject
    OnboardingAdapter onboardingAdapter;
    @Inject
    OnBoardingPresenter onBoardingPresenter;
    OnBoardingComponent onBoardingComponent;

    public static Intent getCallIntent(Context context) {
        return new Intent(context, OnBoardingActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding);
        ButterKnife.bind(this);
        this.setUpView();
        this.setUpViewPager();
    }

    private void setUpView() {
        this.initializeInjector(DependencyInjector.applicationComponent());
        this.onBoardingComponent.inject(this);
        this.onBoardingPresenter.setOnBoardingView(this);
        this.loadOnBoardingList();
    }

    public void setUpViewPager() {
        this.onboardpager.setAdapter(onboardingAdapter);
        onboardpager.setCurrentItem(0);
        onboardpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                for (int i = 0; i < dotsCount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(OnBoardingActivity.this, R.drawable.non_selected_item_dot));
                }
                dots[position].setImageDrawable(ContextCompat.getDrawable(OnBoardingActivity.this, R.drawable.selected_item_dot));
                int pos = position + 1;
                if (pos == dotsCount && previousPos == (dotsCount - 1))
                    onBoardingPresenter.showAnimation();
                else if (pos == (dotsCount - 1) && previousPos == dotsCount)
                    onBoardingPresenter.hideAnimation();
                previousPos = pos;
            }

            @Override
            public void onPageSelected(int position) {
                // no require implementation
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // no require implementation
            }
        });

        btnGetStarted.setOnClickListener((View view) -> onBoardingPresenter.goToLogin());
        btnNext.setOnClickListener((View view) -> {
            Log.d(TAG, "position: " + previousPos);
            onboardpager.setCurrentItem(previousPos);
        });
        setUiPageViewController();
    }

    private void loadOnBoardingList() {
        this.onBoardingPresenter.loadOnBoardingList();
    }

    private void initializeInjector(ApplicationComponent applicationComponent) {
        this.onBoardingComponent = DaggerOnBoardingComponent.builder()
                .applicationComponent(applicationComponent)
                .build();
    }

    private void setUiPageViewController() {
        dotsCount = onboardingAdapter.getCount();
        dots = new ImageView[dotsCount];
        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(ContextCompat.getDrawable(OnBoardingActivity.this, R.drawable.non_selected_item_dot));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(6, 0, 6, 0);
            pagerIndicator.addView(dots[i], params);
        }
        dots[0].setImageDrawable(ContextCompat.getDrawable(OnBoardingActivity.this, R.drawable.selected_item_dot));
    }

    @Override
    public void showOnBoardingList(List<OnBoardItem> onBoardItemList) {
        this.onboardingAdapter.setOnBoardItemList(onBoardItemList);
    }

    @Override
    public void showAnimation() {
        Animation show = AnimationUtils.loadAnimation(this, R.anim.slide_up_anim);
        Animation next = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        btnGetStarted.startAnimation(show);
        btnNext.startAnimation(next);
        createAccount.startAnimation(next);
        show.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                btnGetStarted.setVisibility(View.VISIBLE);
                btnNext.setVisibility(View.INVISIBLE);
                createAccount.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                btnGetStarted.clearAnimation();
            }
        });
    }

    @Override
    public void hideAnimation() {
        Animation hide = AnimationUtils.loadAnimation(this, R.anim.slide_down_anim);
        Animation next = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        btnGetStarted.startAnimation(hide);
        btnNext.startAnimation(next);
        createAccount.startAnimation(next);
        hide.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // no require implementation
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                btnGetStarted.clearAnimation();
                btnGetStarted.setVisibility(View.GONE);
                btnNext.setVisibility(View.VISIBLE);
                createAccount.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void goToLogin() {
        Log.d(TAG, "showLogin");
        startActivity(LoginActivity.getCallIntent(this));
        finish();
    }

    @OnClick(R.id.create_account_onboard)
    public void onClickCreateAccount() {
        this.goToCreateAccount();
    }

    @Override
    public void goToCreateAccount() {
        startActivity(RegisterActivity.getCallIntent(this));
    }
}
