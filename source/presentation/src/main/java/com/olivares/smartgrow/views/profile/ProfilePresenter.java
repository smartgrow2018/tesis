package com.olivares.smartgrow.views.profile;

import android.annotation.SuppressLint;

import com.olivares.entities.Query;
import com.olivares.entities.User;
import com.olivares.smartgrow.di.scopes.PerActivity;
import com.olivares.smartgrow.views.LoadView;
import com.olivares.smartgrow.views.Presenter;
import com.olivares.smartgrow.views.SwipeRefreshView;
import com.olivares.usecase.usecase.history.GetHistoryRecognitionUseCase;
import com.olivares.usecase.usecase.history.HideHistoryRecognitionUseCase;
import com.olivares.usecase.usecase.user.GetUserAccountDataUseCase;
import com.olivares.usecase.usecase.user.UpdateProfileUserUseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@PerActivity
public class ProfilePresenter implements Presenter {
    private GetUserAccountDataUseCase getUserAccountDataUseCase;
    private UpdateProfileUserUseCase updateProfileUserUseCase;
    private ProfileView profileView;

    private RecognitionsHistoryView recognitionsHistoryView;
    private final GetHistoryRecognitionUseCase getHistoryRecognitionUseCase;
    private final HideHistoryRecognitionUseCase hideHistoryRecognitionUseCase;

    @Inject
    public ProfilePresenter(GetUserAccountDataUseCase getUserAccountDataUseCase,
                            UpdateProfileUserUseCase updateProfileUserUseCase,
                            GetHistoryRecognitionUseCase getHistoryRecognitionUseCase,
                            HideHistoryRecognitionUseCase hideHistoryRecognitionUseCase) {
        this.getUserAccountDataUseCase = getUserAccountDataUseCase;
        this.updateProfileUserUseCase = updateProfileUserUseCase;
        this.getHistoryRecognitionUseCase = getHistoryRecognitionUseCase;
        this.hideHistoryRecognitionUseCase = hideHistoryRecognitionUseCase;
    }

    public void setProfileView(ProfileView profileView) {
        this.profileView = profileView;
    }

    public void setRecognitionsHistoryView(RecognitionsHistoryView recognitionsHistoryView) {
        this.recognitionsHistoryView = recognitionsHistoryView;
    }

    @Override
    public void destroy() {
        this.profileView = null;
    }

    @SuppressLint("CheckResult")
    public void getUser() {
        Single.create((SingleOnSubscribe<User>) emitter -> emitter.onSuccess(this.getUserAccountDataUseCase.getUserLogged()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> {
                    if (this.profileView == null) return;
                    this.profileView.showLoading();
                })
                .doFinally(() -> {
                    if (this.profileView == null) return;
                    this.profileView.hideLoading();
                })
                .subscribe(
                        user -> {
                            if (this.profileView == null) return;
                            if (user != null)
                                this.profileView.showProfile(user);
                            else
                                this.profileView.showError("Error al obtener usuario");
                        },
                        error -> {
                            if (this.profileView == null) return;
                            this.profileView.showError(error.getMessage());
                        }
                );
    }

    @SuppressLint("CheckResult")
    public void loadUserProfile(String username) {
        Single.create((SingleOnSubscribe<User>) emmiter -> emmiter.onSuccess(this.getUserAccountDataUseCase.getUserAccount(username)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> {
                    if (this.profileView == null) return;
                    this.profileView.showLoading();
                })
                .doFinally(() -> {
                    if (this.profileView == null) return;
                    this.profileView.hideLoading();
                })
                .subscribe(
                        userAccount -> {
                            if (this.profileView == null) return;
                            this.profileView.showProfile(userAccount);
                        },
                        error -> {
                            if (this.profileView == null) return;
                            this.profileView.showError(error.getMessage());
                        }
                );
    }

    @SuppressLint("CheckResult")
    public void updateProfile(User user) {
        Single.create((SingleOnSubscribe<User>) emmiter -> emmiter.onSuccess(this.updateProfileUserUseCase.updateProfileUser(user)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> {
                    if (this.profileView == null) return;
                    this.profileView.showLoading();
                })
                .doFinally(() -> {
                    if (this.profileView == null) return;
                    this.profileView.hideLoading();
                })
                .subscribe(
                        userAccount -> {
                            if (this.profileView == null) return;
                            //this.profileView.showProfile(userAccount);
                        },
                        error -> {
                            if (this.profileView == null) return;
                            this.profileView.showError(error.getMessage());
                        }
                );
    }

    @SuppressLint("CheckResult")
    public void loadRecognitionsHistory() {
        try {
            Single.create((SingleOnSubscribe<List<Query>>) emmiter -> emmiter.onSuccess(this.getHistoryRecognitionUseCase.getHistoryRecognition()))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(__ -> {
                        if (this.recognitionsHistoryView == null) return;
                        this.recognitionsHistoryView.showRefresh();
                    })
                    .doFinally(() -> {
                        if (this.recognitionsHistoryView == null) return;
                        this.recognitionsHistoryView.hideRefresh();
                    })
                    .subscribe(
                            recognitions -> {
                                if (this.recognitionsHistoryView == null) return;
                                this.recognitionsHistoryView.showRecognitionsHistory(recognitions);
                            },
                            error -> {
                                if (this.recognitionsHistoryView == null) return;
                                this.recognitionsHistoryView.showRefreshError(error.getMessage());
                            }
                    );
        } catch (Exception e) {
            if (this.recognitionsHistoryView == null) return;
            this.recognitionsHistoryView.showRefreshError(e.getMessage());
        }
    }

    public void goToHistoryDetail(Query query) {
        this.recognitionsHistoryView.showRecognitionView(query);
    }

    public void showRecognitionImage(Query query) {
        this.recognitionsHistoryView.showRecognitionImage(query);
    }

    public void goToShare(Query query) {
        this.recognitionsHistoryView.shareRecognition(query);
    }

    @SuppressLint("CheckResult")
    public void goToRemove(Query query) {
        Single.create((SingleOnSubscribe<Query>) emmiter -> emmiter.onSuccess(this.hideHistoryRecognitionUseCase.hideHistoryRecognitionLocal(query)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> {
                    if (this.recognitionsHistoryView == null) return;
                    this.recognitionsHistoryView.showRefresh();
                })
                .doFinally(() -> {
                    if (this.recognitionsHistoryView == null) return;
                    this.recognitionsHistoryView.hideRefresh();
                })
                .subscribe(
                        recognition -> {
                            if (this.recognitionsHistoryView == null) return;
                            this.recognitionsHistoryView.removeRecognition(recognition);
                        },
                        error -> {
                            if (this.recognitionsHistoryView == null) return;
                            this.recognitionsHistoryView.showRefreshError(error.getMessage());
                        }
                );
    }

    public interface ProfileView extends LoadView {
        void showProfile(User userAccout);

        void getProfile(User user);
    }

    public interface RecognitionsHistoryView extends SwipeRefreshView {
        void showRecognitionsHistory(List<Query> queryList);

        void showRecognitionView(Query query);

        void showRecognitionImage(Query query);

        void shareRecognition(Query query);

        void removeRecognition(Query query);
    }
}
