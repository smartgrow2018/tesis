package com.olivares.smartgrow.views.camera.result;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import com.google.gson.Gson;
import com.olivares.smartgrow.R;
import com.olivares.smartgrow.di.components.ApplicationComponent;
import com.olivares.smartgrow.di.components.CameraComponent;
import com.olivares.smartgrow.di.components.DaggerCameraComponent;
import com.olivares.smartgrow.di.injector.DependencyInjector;
import com.olivares.smartgrow.util.Util;
import com.olivares.smartgrow.views.BaseActivity;
import com.olivares.entities.Result;
import com.olivares.smartgrow.views.camera.CameraPresenter;
import com.olivares.smartgrow.views.home.HomeActivity;
import com.olivares.smartgrow.views.plagues.detail.PlagueDetailActivity;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResultActivity extends BaseActivity implements CameraPresenter.ResultView {
    public static final String INTENT_EXTRA_RESULTS = "INTENT_RESULTS_MODEL";
    private static final String TAG = ResultActivity.class.getName();
    @BindView(R.id.recycler_result_activity)
    RecyclerView recyclerView;

    @Inject
    ResultAdapter resultAdapter;
    @Inject
    CameraPresenter cameraPresenter;
    @Inject
    Gson gson;

    CameraComponent cameraComponent;

    private ResultAdapter.ResultItemClickListener resultItemClickListener = this::goToResultItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        ButterKnife.bind(this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Resultados");
        setUpView();
        setUpDataResults();
    }

    private void setUpView() {
        this.initializeInjector(DependencyInjector.applicationComponent());
        this.cameraComponent.inject(this);
        this.cameraPresenter.setResultView(this);
        this.setUpRecyclerView();
    }

    private void setUpRecyclerView() {
        this.resultAdapter.setResultItemClickListener(resultItemClickListener);
        this.resultAdapter.setNormalType(true);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(resultAdapter);
        LayoutAnimationController layoutAnimationController = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_history);
        recyclerView.setLayoutAnimation(layoutAnimationController);
        recyclerView.scheduleLayoutAnimation();
    }

    private void setUpDataResults() {
        List<Result> resultList = Util.toList(getIntent().getStringExtra(INTENT_EXTRA_RESULTS), Result[].class);
        this.cameraPresenter.loadResultList(resultList);
    }

    private void initializeInjector(ApplicationComponent applicationComponent) {
        this.cameraComponent = DaggerCameraComponent.builder()
                .applicationComponent(applicationComponent)
                .build();
    }

    public static Intent getCallIntent(Context context, String results) {
        Intent intent = new Intent(context, ResultActivity.class);
        intent.putExtra(INTENT_EXTRA_RESULTS, results);
        return intent;
    }

    @Override
    public void showResultList(List<Result> resultList) {
        this.resultAdapter.setResultList(resultList);
    }

    @Override
    public void showResultView(Result result) {
        // start activity Detail Plaga
        Log.d(TAG, "showResultDetail");
        startActivity(PlagueDetailActivity.getCallIntent(this, gson.toJson(result), 1));
    }

    @Override
    public void showLoading() {
        Log.d(TAG, "showLoading");
    }

    @Override
    public void hideLoading() {
        Log.d(TAG, "hideLoading");
    }

    @Override
    public void showError(String messageError) {
        Log.d(TAG, "showError:" + messageError);
    }

    public void goToResultItem(Result result) {
        this.cameraPresenter.goToResultDetail(result);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        // volver al fragment anterior
        if (item.getItemId() == android.R.id.home) {
            startActivity(HomeActivity.getCallIntent(this));
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(HomeActivity.getCallIntent(this));
        finish();
    }

}
