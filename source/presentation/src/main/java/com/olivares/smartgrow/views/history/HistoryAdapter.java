package com.olivares.smartgrow.views.history;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.olivares.entities.Query;
import com.olivares.smartgrow.R;
import com.olivares.smartgrow.util.Util;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = HistoryAdapter.class.getName();
    private List<Query> historyList;
    private HistoryItemClickListener historyItemClickListener;
    private HistoryItemMenuClickListener historyItemMenuClickListener;
    private HistoryImageClickListener historyImageClickListener;
    private final LayoutInflater layoutInflater;
    private final Picasso picasso;
    private int lastPosition = -1;
    private static final int CLASIC_TYPE = 0;
    private static final int PROFILE_TYPE = 1;
    private int viewType = PROFILE_TYPE;

    @Inject
    public HistoryAdapter(Context context, Picasso picasso) {
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.picasso = picasso;
        this.historyList = Collections.emptyList();
    }

    public void setHistoryList(List<Query> historyList) {
        this.historyList = historyList;
        this.notifyDataSetChanged();
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public void setHistoryItemClickListener(HistoryItemClickListener historyItemClickListener) {
        this.historyItemClickListener = historyItemClickListener;
    }

    public void setHistoryImageClickListener(HistoryImageClickListener historyImageClickListener) {
        this.historyImageClickListener = historyImageClickListener;
    }

    public void setHistoryItemMenuClickListener(HistoryItemMenuClickListener historyItemMenuClickListener) {
        this.historyItemMenuClickListener = historyItemMenuClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == CLASIC_TYPE) {
            final View view = this.layoutInflater.inflate(R.layout.card_history_3, parent, false);
            return new HistoryViewHolder(view);
        } else {
            final View view = this.layoutInflater.inflate(R.layout.card_history, parent, false);
            return new HistoryUserViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final Query query = historyList.get(position);
        if (holder instanceof HistoryViewHolder) {
            final HistoryViewHolder viewHolder = (HistoryViewHolder) holder;
            viewHolder.textDateHistory.setText(Util.getDate(query.getDate()));
            viewHolder.namePlagueHistory.setText(query.getResultList().get(0).getNamePlague());
            this.loadImagePicasso(viewHolder.imageViewHistory, query.getUrlImageQuery());
            viewHolder.imageViewHistory.setOnClickListener(view ->
                    this.historyItemClickListener.onHistoryItemClick(query));
            viewHolder.menuCardHistory.setOnClickListener(view ->
                    this.historyItemMenuClickListener.onHistoryItemMenuClick(query, viewHolder.menuCardHistory));
            setFadeInAnimation(holder.itemView);
        } else {
            final HistoryUserViewHolder viewHolder = (HistoryUserViewHolder) holder;
            Log.d(TAG, "URLIMAGE: " + query.getUrlImageQuery());
            viewHolder.textDateHistory.setText(Util.getDate(query.getDate()));
            viewHolder.namePlagueHistory.setText(query.getResultList().get(0).getNamePlague());
            this.loadImagePicasso(viewHolder.imageViewProfile, query.getUrlImageQuery());
            viewHolder.itemHistory.setOnClickListener(view ->
                    this.historyItemClickListener.onHistoryItemClick(query));
            viewHolder.menuCardHistory.setOnClickListener(view ->
                    this.historyItemMenuClickListener.
                            onHistoryItemMenuClick(query, viewHolder.menuCardHistory));
            viewHolder.imageViewProfile.setOnClickListener(view ->
                    this.historyImageClickListener.onImageClick(query));
            animate(holder.itemView, position);
        }
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
        holder.itemView.clearAnimation();
    }

    @Override
    public int getItemViewType(int position) {
        if (this.viewType == CLASIC_TYPE)
            return CLASIC_TYPE;
        return PROFILE_TYPE;
    }

    public void setFadeInAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }

    public void setFadeOutAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(1.0f, 0.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }

    public void animate(View view, int position) {
        Animation animation = AnimationUtils.loadAnimation(view.getContext(),
                (position > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_from_top);
        view.startAnimation(animation);
        lastPosition = position;
    }

    private void loadImagePicasso(ImageView imageView, String url) {
        this.picasso.load(url)
                .placeholder(R.drawable.smartgrow_with_text)
                .resize(60, 60)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(imageView);
    }

    @Override
    public int getItemCount() {
        return (this.historyList != null) ? this.historyList.size() : 0;
    }


    public static class HistoryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_date_history)
        TextView textDateHistory;
        @BindView(R.id.text_plague_history)
        TextView namePlagueHistory;
        @BindView(R.id.image_history)
        ImageView imageViewHistory;
        @BindView(R.id.item_history)
        CardView itemHistory;
        @BindView(R.id.iv_menu_history)
        ImageView menuCardHistory;

        public HistoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class HistoryUserViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_date_history)
        TextView textDateHistory;
        @BindView(R.id.text_plague_history)
        TextView namePlagueHistory;
        @BindView(R.id.item_history)
        CardView itemHistory;
        @BindView(R.id.iv_menu_history)
        ImageView menuCardHistory;
        @BindView(R.id.iv_user_profile)
        ImageView imageViewProfile;

        public HistoryUserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface HistoryItemClickListener {
        void onHistoryItemClick(Query query);
    }

    public interface HistoryItemMenuClickListener {
        void onHistoryItemMenuClick(Query query, View view);
    }

    public interface HistoryShareClickListener {
        void onShareClick(Query query);
    }

    public interface HistoryRemoveClickListener {
        void onRemoveClick(Query query);
    }

    public interface HistoryImageClickListener {
        void onImageClick(Query query);
    }


}
