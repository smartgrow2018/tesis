package com.olivares.smartgrow.util;

import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Util {

    public Util() {
    }

    public static <T> List<T> toList(String json, Class<T[]> clazz) {
        if (null == json) {
            return Collections.emptyList();
        }
        Gson gson = new Gson();
        T[] arr = gson.fromJson(json, clazz);
        return Arrays.asList(arr);
    }

    public static int numberProgress(double number) {
        if (number > 1) {
            number = 1;
        }
        number = number * 100;
        return (int) number;
    }

    public static String percentFormat(double number) {
        if (number > 1) {
            number = 1;
        }
        DecimalFormat df = new DecimalFormat("#%");
        return df.format(number);
    }

    public static String getDate(String fecha) {
        try {
            SimpleDateFormat dateFormatInput = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH);
            SimpleDateFormat dateFormatOutput = new SimpleDateFormat("dd/MMM/yy hh:mm aa", Locale.ENGLISH);
            Date date = dateFormatInput.parse(fecha, new ParsePosition(0));
            String returnDate = dateFormatOutput.format(date);
            return returnDate.replace("/", " ").replace(".", "");
        } catch (Exception ex) {
            return "";
        }
    }

    public static String formatResumeLibrary(String txt) {
        if (txt != null && txt.length() > 120) {
            return txt.substring(0, 100) + "...";
        }
        return txt;
    }

}
