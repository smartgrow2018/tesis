package com.olivares.smartgrow.views.home;

import android.annotation.SuppressLint;

import com.olivares.smartgrow.views.LoadView;
import com.olivares.smartgrow.views.Presenter;
import com.olivares.usecase.usecase.user.SeeUserTutorialUseCase;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;

public class HomePresenter implements Presenter {

    /*
     *
     * https://comunidad.leroymerlin.es/t5/Bricopedia-Jardiner%C3%ADa/C%C3%B3mo-cuidar-y-cultivar-pepino-dulce/ta-p/138895
     * https://www.hortalizas.com/proteccion-de-cultivos/controla-el-mildiu-del-pepino/
     * https://elhuerto20.wordpress.com/2012/08/25/el-pepino-dulce-en-el-huerto/
     * https://i.pinimg.com/originals/b3/b3/19/b3b319d0432d778367280644faeeea74.jpg
     *
     *
     * */

    private HomeView homeView;
    private SeeUserTutorialUseCase seeUserTutorialUseCase;

    @Inject
    public HomePresenter(SeeUserTutorialUseCase seeUserTutorialUseCase) {
        this.seeUserTutorialUseCase = seeUserTutorialUseCase;
    }

    public void setHomeView(@NonNull HomeView homeView) {
        this.homeView = homeView;
    }

    public void goToProfile() {
        this.homeView.showProfile();
    }

    @SuppressLint("CheckResult")
    public void seePlagueTutorial() {
        Single.create((SingleOnSubscribe<Boolean>) emmiter -> emmiter.onSuccess(this.seeUserTutorialUseCase.seePlagueTutorial()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> {
                    if (this.homeView == null) return;
                    this.homeView.showLoading();
                })
                .doFinally(() -> {
                    if (this.homeView == null) return;
                    this.homeView.hideLoading();
                })
                .subscribe(
                        seeTutorial -> {
                            if (this.homeView == null) return;
                            this.homeView.seePlagueTutorial(seeTutorial);
                        },
                        error -> {
                            if (this.homeView == null) return;
                            this.homeView.showError(error.getMessage());
                        }
                );
    }

    @SuppressLint("CheckResult")
    public void seeHistoryTutorial() {
        Single.create((SingleOnSubscribe<Boolean>) emmiter -> emmiter.onSuccess(this.seeUserTutorialUseCase.seeHistoryTutorial()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> {
                    if (this.homeView == null) return;
                    this.homeView.showLoading();
                })
                .doFinally(() -> {
                    if (this.homeView == null) return;
                    this.homeView.hideLoading();
                })
                .subscribe(
                        seeTutorial -> {
                            if (this.homeView == null) return;
                            this.homeView.seeHistoryTutorial(seeTutorial);
                        },
                        error -> {
                            if (this.homeView == null) return;
                            this.homeView.showError(error.getMessage());
                        }
                );
    }

    @SuppressLint("CheckResult")
    public void deleteAllUserPrefs() {
        Single.create((SingleOnSubscribe<Boolean>) emmiter -> emmiter.onSuccess(this.seeUserTutorialUseCase.deleteAllUserPref()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> {
                    if (this.homeView == null) return;
                })
                .doFinally(() -> {
                    if (this.homeView == null) return;
                })
                .subscribe(
                        seeTutorial -> {
                            if (this.homeView == null) return;
                        },
                        error -> {
                            if (this.homeView == null) return;
                        }
                );
    }

    @Override
    public void destroy() {
        this.homeView = null;
    }

    public interface HomeView extends LoadView {
        void showProfile();

        void seePlagueTutorial(Boolean seeTutorial);

        void seeHistoryTutorial(Boolean seeTutorial);
    }

}
