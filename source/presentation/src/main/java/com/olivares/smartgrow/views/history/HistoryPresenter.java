package com.olivares.smartgrow.views.history;

import android.annotation.SuppressLint;
import android.util.Log;

import com.olivares.entities.Query;
import com.olivares.smartgrow.views.Presenter;
import com.olivares.smartgrow.views.SwipeRefreshView;
import com.olivares.usecase.usecase.history.GetHistoryRecognitionUseCase;
import com.olivares.usecase.usecase.history.HideHistoryRecognitionUseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class HistoryPresenter implements Presenter {
    private static final String TAG = HistoryPresenter.class.getName();
    private RecognitionsHistoryView recognitionsHistoryView;
    private final GetHistoryRecognitionUseCase getHistoryRecognitionUseCase;
    private final HideHistoryRecognitionUseCase hideHistoryRecognitionUseCase;

    @Inject
    public HistoryPresenter(GetHistoryRecognitionUseCase getHistoryRecognitionUseCase,
                            HideHistoryRecognitionUseCase hideHistoryRecognitionUseCase) {
        this.getHistoryRecognitionUseCase = getHistoryRecognitionUseCase;
        this.hideHistoryRecognitionUseCase = hideHistoryRecognitionUseCase;
    }

    @SuppressLint("CheckResult")
    public void loadRecognitionsHistory() {
        Single.create((SingleOnSubscribe<List<Query>>) emmiter -> emmiter.onSuccess(this.getHistoryRecognitionUseCase.getHistoryRecognition()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> {
                    if (this.recognitionsHistoryView == null) return;
                    this.recognitionsHistoryView.showRefresh();
                })
                .doFinally(() -> {
                    if (this.recognitionsHistoryView == null) return;
                    this.recognitionsHistoryView.hideRefresh();
                })
                .subscribe(
                        recognitions -> {
                            if (this.recognitionsHistoryView == null) return;
                            this.recognitionsHistoryView.showRecognitionsHistory(recognitions);
                        },
                        error -> {
                            if (this.recognitionsHistoryView == null) return;
                            this.recognitionsHistoryView.showRefreshError(error.getMessage());
                        }
                );
    }

    public void setRecognitionsHistoryView(RecognitionsHistoryView recognitionsHistoryView) {
        this.recognitionsHistoryView = recognitionsHistoryView;
    }

    public void goToHistoryDetail(Query query) {
        this.recognitionsHistoryView.showRecognitionView(query);
    }

    public void goToShare(Query query) {
        this.recognitionsHistoryView.shareRecognition(query);
    }

    @SuppressLint("CheckResult")
    public void goToRemove(Query query) {
        Log.d(TAG, "asssssssssss ----------------->");
        Single.create((SingleOnSubscribe<Query>) emmiter -> emmiter.onSuccess(this.hideHistoryRecognitionUseCase.hideHistoryRecognitionLocal(query)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> {
                    if (this.recognitionsHistoryView == null) return;
                    this.recognitionsHistoryView.showRefresh();
                })
                .doFinally(() -> {
                    if (this.recognitionsHistoryView == null) return;
                    this.recognitionsHistoryView.hideRefresh();
                })
                .subscribe(
                        recognition -> {
                            if (this.recognitionsHistoryView == null) return;
                            this.recognitionsHistoryView.removeRecognition(recognition);
                        },
                        error -> {
                            if (this.recognitionsHistoryView == null) return;
                            this.recognitionsHistoryView.showRefreshError(error.getMessage());
                        }
                );
    }

    @Override
    public void destroy() {
        this.recognitionsHistoryView = null;
    }

    public interface RecognitionsHistoryView extends SwipeRefreshView {
        void showRecognitionsHistory(List<Query> queryList);

        void showRecognitionView(Query query);

        void shareRecognition(Query query);

        void removeRecognition(Query query);
    }

}
