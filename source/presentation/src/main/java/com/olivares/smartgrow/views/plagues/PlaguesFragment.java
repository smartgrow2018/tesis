package com.olivares.smartgrow.views.plagues;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import com.google.gson.Gson;
import com.olivares.smartgrow.views.plagues.detail.PlagueDetailActivity;
import com.olivares.smartgrow.R;
import com.olivares.smartgrow.di.components.ApplicationComponent;
import com.olivares.smartgrow.di.components.DaggerLibraryComponent;
import com.olivares.smartgrow.di.components.LibraryComponent;
import com.olivares.smartgrow.di.injector.DependencyInjector;
import com.olivares.entities.Plague;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PlaguesFragment extends Fragment implements PlaguesPresenter.PlaguesView {
    public static final String TAG = PlaguesFragment.class.getName();
    @BindView(R.id.recycler_library_fragment)
    RecyclerView recyclerViewLibrary;
    @BindView(R.id.swipe_refresh_library_fragent)
    SwipeRefreshLayout swipeRefreshLayoutLibrary;

    @Inject
    PlaguesAdapter plaguesAdapter;
    @Inject
    PlaguesPresenter plaguesPresenter;
    @Inject
    Gson gson;

    public PlaguesFragment() {
        // no require implementation
    }

    LibraryComponent libraryComponent;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_library, container, false);
        ButterKnife.bind(this, view);
        this.setUpView();
        return view;
    }

    private void setUpView() {
        this.initializeInjector(DependencyInjector.applicationComponent());
        this.libraryComponent.inject(this);
        this.plaguesPresenter.setPlaguesView(this);
        this.swipeRefreshLayoutLibrary.setOnRefreshListener(this::initLoaPlagues);
        this.setUpRecyclerView();
        this.initLoaPlagues();
    }

    private void initLoaPlagues() {
        this.plaguesPresenter.loadPlagues();
    }

    private void setUpRecyclerView() {
        this.plaguesAdapter.setLibraryItemClickListener(libraryItemClickListener);
        recyclerViewLibrary.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        recyclerViewLibrary.setLayoutManager(layoutManager);
        recyclerViewLibrary.setAdapter(plaguesAdapter);
        LayoutAnimationController layoutAnimationController = AnimationUtils.loadLayoutAnimation(getContext(), R.anim.layout_animation_history);
        recyclerViewLibrary.setLayoutAnimation(layoutAnimationController);
        recyclerViewLibrary.scheduleLayoutAnimation();
    }

    private void initializeInjector(ApplicationComponent applicationComponent) {
        this.libraryComponent = DaggerLibraryComponent.builder()
                .applicationComponent(applicationComponent)
                .build();
    }

    @Override
    public void showPlagues(List<Plague> plagueList) {
        // call web service
        this.plaguesAdapter.setLibraryList(plagueList);
        this.recyclerViewLibrary.scheduleLayoutAnimation();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.plaguesPresenter.destroy();
    }

    @Override
    public void showPlagueView(Plague plague) {
        // start activity Detail Plaga
        Log.d(TAG, "showPlagueDetail");
        startActivity(PlagueDetailActivity.getCallIntent(getContext(), gson.toJson(plague), 0));
    }

    @Override
    public void showLoading() {
        this.swipeRefreshLayoutLibrary.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        this.swipeRefreshLayoutLibrary.setRefreshing(false);
    }

    private PlaguesAdapter.PlagueItemClickListener libraryItemClickListener = (this::goToLibraryItem);

    @Override
    public void showError(String messageError) {
        this.swipeRefreshLayoutLibrary.setRefreshing(false);
        Log.d(TAG, "showError:" + messageError);
    }

    public void goToLibraryItem(Plague plague) {
        this.plaguesPresenter.goToPlagueDetail(plague);
    }
}
