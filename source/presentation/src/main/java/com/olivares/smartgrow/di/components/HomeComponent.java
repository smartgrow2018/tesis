package com.olivares.smartgrow.di.components;


import com.olivares.smartgrow.di.modules.ActivityModule;
import com.olivares.smartgrow.di.scopes.PerActivity;
import com.olivares.smartgrow.views.home.HomeActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class})
public interface HomeComponent {
    void inject(HomeActivity homeActivity);
}
