package com.olivares.smartgrow.views.camera.alert;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.olivares.smartgrow.R;
import com.olivares.entities.AlertItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AlertAdapter extends RecyclerView.Adapter<AlertAdapter.AlertViewHolder> {

    private List<AlertItem> alertItemList;
    private final LayoutInflater layoutInflater;

    public AlertAdapter(Context context) {
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.alertItemList = Collections.emptyList();
        this.loadData();
    }

    private void loadData() {
        List<AlertItem> lista = new ArrayList<>();
        lista.add(new AlertItem(
                "El cultivo está demasiado lejos, se pierden los colores.",
                "Buena iluminación, todos los colores son visibles.",
                1,
                2));
        lista.add(new AlertItem(
                "Buena distancia, el daño es claramente visible.",
                "Demasiado lejos, el patrón de la enfermedad no es visible.",
                1,
                2));
        lista.add(new AlertItem(
                "Buen enfoque, el daño está bien enfocado y nítido.",
                "El daño no está enfocado.",
                1,
                2));
        this.alertItemList = lista;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AlertViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = this.layoutInflater.inflate(R.layout.item_alert, parent, false);
        return new AlertViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AlertViewHolder holder, int position) {
        final AlertItem alertItem = this.alertItemList.get(position);
        holder.textBadAlert.setText(alertItem.getTxtBadImage());
        holder.textGoodAlert.setText(alertItem.getTxtGoodImage());
    }

    @Override
    public int getItemCount() {
        return (this.alertItemList != null) ? this.alertItemList.size() : 0;
    }

    static class AlertViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_good_alert)
        TextView textGoodAlert;
        @BindView(R.id.txt_bad_alert)
        TextView textBadAlert;
        @BindView(R.id.iv_good_alert)
        ImageView imageViewGoodAlert;
        @BindView(R.id.iv_bad_alert)
        ImageView imageViewBadAlert;

        AlertViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
