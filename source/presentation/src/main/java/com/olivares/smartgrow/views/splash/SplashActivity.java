package com.olivares.smartgrow.views.splash;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.olivares.smartgrow.di.components.ApplicationComponent;
import com.olivares.smartgrow.di.components.DaggerLoginComponent;
import com.olivares.smartgrow.di.components.LoginComponent;
import com.olivares.smartgrow.di.injector.DependencyInjector;
import com.olivares.smartgrow.views.home.HomeActivity;
import com.olivares.smartgrow.views.login.LoginPresenter;
import com.olivares.smartgrow.views.onboarding.OnBoardingActivity;
import com.testfairy.TestFairy;

import javax.inject.Inject;

public class SplashActivity extends AppCompatActivity implements LoginPresenter.UserLoggedSessionView {
    private static final String TAG = SplashActivity.class.getName();

    @Inject
    LoginPresenter loginPresenter;
    LoginComponent loginComponent;
    @Inject
    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TestFairy.begin(this, "a89b3b3c04d22119ff22a4dda34c5511ec26ea70");
        this.initializeInjector(DependencyInjector.applicationComponent());
        this.loginComponent.inject(this);
        this.loginPresenter.setUserLoggedSessionView(this);
        this.initLoggedUser();
    }

    public void initLoggedUser() {
        this.loginPresenter.getUser();
    }

    private void initializeInjector(ApplicationComponent applicationComponent) {
        this.loginComponent = DaggerLoginComponent.builder()
                .applicationComponent(applicationComponent)
                .build();
    }

    @Override
    public void showHome() {
        startActivity(HomeActivity.getCallIntent(SplashActivity.this));
        finish();
    }

    @Override
    public void existUser(Boolean logged) {
        Log.d(TAG, "logged: " + logged);
        if (logged) {
            this.showHome();
        } else {
            Log.d(TAG, "nuevo inicio");
            showOnBoarding();
        }
    }

    private void showOnBoarding() {
        startActivity(OnBoardingActivity.getCallIntent(SplashActivity.this));
        finish();
    }

    @Override
    public void showLoading() {
        Log.d(TAG, "Show loading");
    }

    @Override
    public void hideLoading() {
        Log.d(TAG, "Hide loading");
    }

    @Override
    public void showError(String messageError) {
        Log.d(TAG, "error: " + messageError);
        showOnBoarding();
    }
}
