package com.olivares.smartgrow.di.components;

import com.olivares.smartgrow.di.modules.ActivityModule;
import com.olivares.smartgrow.di.scopes.PerActivity;
import com.olivares.smartgrow.views.login.LoginActivity;
import com.olivares.smartgrow.views.splash.SplashActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class})
public interface LoginComponent {

    void inject(LoginActivity loginActivity);

    void inject(SplashActivity splashActivity);

}
