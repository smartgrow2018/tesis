package com.olivares.smartgrow.di.modules;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.olivares.smartgrow.BuildConfig;
import com.olivares.smartgrow.application.AndroidApplication;
import com.olivares.remote.network.RestApi;
import com.olivares.remote.network.RestApi2;
import com.squareup.picasso.Picasso;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApplicationModule {
    private final AndroidApplication androidApplication;

    public ApplicationModule(AndroidApplication androidApplication) {
        this.androidApplication = androidApplication;

    }

    @Singleton
    @Provides
    Context providerContext() {
        return this.androidApplication;
    }


    @Singleton
    @Provides
    RxJavaCallAdapterFactory providerRxJavaCallAdapterFactory() {
        return RxJavaCallAdapterFactory.create();
    }

    @Singleton
    @Provides
    GsonConverterFactory providerGsonConverterFactory() {
        return GsonConverterFactory.create();
    }

    @Singleton
    @Provides
    @Named("ok-1")
    OkHttpClient providerOkHttpClient1() {
        return new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build();
    }

    @Singleton
    @Provides
    @Named("ok-2")
    OkHttpClient providerOkHttpClient2() {
        return new OkHttpClient.Builder()
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .build();
    }

    @Singleton
    @Provides
    Retrofit providerRetrofit(@Named("ok-1") OkHttpClient client, GsonConverterFactory converterFactory, RxJavaCallAdapterFactory adapterFactory) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL_BASE)
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(adapterFactory)
                .client(client)
                .build();
    }

    @Singleton
    @Provides
    @Named("api-2")
    Retrofit providerRetrofit2(@Named("ok-2") OkHttpClient client, GsonConverterFactory converterFactory, RxJavaCallAdapterFactory adapterFactory) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL_BASE_IMGUR)
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(adapterFactory)
                .client(client)
                .build();
    }

    @Singleton
    @Provides
    OkHttp3Downloader providerOkHttp3Downloader(@Named("ok-1") OkHttpClient okHttpClient) {
        return new OkHttp3Downloader(okHttpClient);
    }

    @Singleton
    @Provides
    Picasso providerPicasso(Context context, OkHttp3Downloader okHttp3Downloader) {
        return new Picasso.Builder(context).
                downloader(okHttp3Downloader).
                build();
    }

    @Singleton
    @Provides
    Gson providerGson() {
        return new Gson();
    }

    @Singleton
    @Provides
    RestApi providerApiService(Retrofit retrofit) {
        return retrofit.create(RestApi.class);
    }

    @Singleton
    @Provides
    RestApi2 providerApiService2(@Named("api-2") Retrofit retrofit) {
        return retrofit.create(RestApi2.class);
    }

    @Singleton
    @Provides
    RealmConfiguration providerRealm(Context context) {
        Realm.init(context);
        RealmConfiguration.Builder builder = new RealmConfiguration.Builder();
        builder.name("appBase");
        return builder.build();
    }

    @Singleton
    @Provides
    SharedPreferences providerSharedPreferences(Context context) {
        final String NAME_PREF = androidApplication.getPackageName() + "USER_PREFS";
        return context.getSharedPreferences(NAME_PREF, Context.MODE_PRIVATE);
    }

}
