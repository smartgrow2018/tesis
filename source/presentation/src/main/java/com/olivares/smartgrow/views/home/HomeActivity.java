package com.olivares.smartgrow.views.home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.Gson;
import com.olivares.entities.User;
import com.olivares.smartgrow.views.BaseActivity;
import com.olivares.smartgrow.R;
import com.olivares.smartgrow.views.camera.Camera2Fragment;
import com.olivares.smartgrow.di.components.ApplicationComponent;
import com.olivares.smartgrow.di.components.DaggerHomeComponent;
import com.olivares.smartgrow.di.components.HomeComponent;
import com.olivares.smartgrow.di.injector.DependencyInjector;
import com.olivares.smartgrow.views.history.HomeFragment;
import com.olivares.smartgrow.views.login.LoginActivity;
import com.olivares.smartgrow.views.login.LoginPresenter;
import com.olivares.smartgrow.views.plagues.PlaguesFragment;
import com.olivares.smartgrow.views.profile.ProfileActivity;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt;
import uk.co.samuelwall.materialtaptargetprompt.extras.focals.RectanglePromptFocal;


public class HomeActivity extends BaseActivity implements HomePresenter.HomeView, LoginPresenter.UserCloseSessionView {
    public static final String TAG = HomeActivity.class.getName();
    private static final String INTENT_EXTRA_USER = "INTENT_EXTRA_USER_MODEL";

    @BindView(R.id.navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    HomePresenter homePresenter;
    @Inject
    LoginPresenter loginPresenter;
    HomeComponent homeComponent;
    @Inject
    Gson gson;

    User user;

    public static Intent getCallIntent(Context context) {
        return new Intent(context, HomeActivity.class);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        this.initializeInjector(DependencyInjector.applicationComponent());
        user = new User();
        setUpView();
        setUpFragmentHome();
        initNavigation();
        showToolbar();
        setUpDataUser();
    }

    private void setUpDataUser() {
        this.user = this.gson.fromJson(getIntent().getStringExtra(INTENT_EXTRA_USER), User.class);
    }

    private void setUpView() {
        this.initializeInjector(DependencyInjector.applicationComponent());
        this.homeComponent.inject(this);
        this.homePresenter.setHomeView(this);
        this.loginPresenter.setUserCloseSessionView(this);
    }

    private void initializeInjector(ApplicationComponent applicationComponent) {
        this.homeComponent = DaggerHomeComponent.builder()
                .applicationComponent(applicationComponent)
                .build();
    }

    private void showToolbar() {
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Smart Grow");
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    private void setUpFragmentHome() {
        HomeFragment homeFragment = new HomeFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_layout_content, homeFragment).commit();
    }

    public void initNavigation() {
        this.bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    showHistoryTutorial();
                    setUpFragment(new HomeFragment());
                    break;
                case R.id.navigation_camera:
                    setUpFragment(Camera2Fragment.newInstance());
                    break;
                case R.id.navigation_library:
                    showPlagueTutorial();
                    setUpFragment(new PlaguesFragment());
                    break;
            }
            return true;
        });
    }

    MaterialTapTargetPrompt materialTapTargetPrompt;

    @SuppressLint("NewApi")
    public void showTutorial(String title, String subtitle, View view) {
        if (materialTapTargetPrompt != null) {
            return;
        }
        SpannableStringBuilder secondaryText = new SpannableStringBuilder(subtitle);
        secondaryText.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorGreenDark)), 8, 15, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        SpannableStringBuilder primaryText = new SpannableStringBuilder(title);
        primaryText.setSpan(new BackgroundColorSpan(ContextCompat.getColor(this, R.color.colorGreenDark)), 0, 4, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        materialTapTargetPrompt = new MaterialTapTargetPrompt.Builder(HomeActivity.this)
                .setTarget(view)
                .setFocalPadding(R.dimen.padding_promt_library)
                .setPrimaryText(String.valueOf(primaryText))
                .setSecondaryText(String.valueOf(secondaryText))
                .setBackButtonDismissEnabled(true)
                .setPromptFocal(new RectanglePromptFocal())
                .setAnimationInterpolator(new FastOutSlowInInterpolator())
                .setBackgroundColour(this.getColor(R.color.colorGreenDark))
                .setPromptStateChangeListener((prompt, state) -> {
                    if (state == MaterialTapTargetPrompt.STATE_FOCAL_PRESSED
                            || state == MaterialTapTargetPrompt.STATE_DISMISSING) {
                        materialTapTargetPrompt = null;
                    }
                })
                .create();
        Objects.requireNonNull(materialTapTargetPrompt).show();
    }

    private void setUpFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout_content, fragment).commit();
    }

    private void showPlagueTutorial() {
        this.homePresenter.seePlagueTutorial();
    }

    private void showHistoryTutorial() {
        this.homePresenter.seeHistoryTutorial();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.homePresenter.destroy();
        this.loginPresenter.destroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.logout:
                this.loginPresenter.logoutUser();
                return true;
            case R.id.update:
                this.homePresenter.goToProfile();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showProfile() {
        // start activity profile
        startActivity(ProfileActivity.getCallIntent(this));
    }

    @Override
    public void showLoading() {
        Log.d(TAG, "show loading ...");
    }

    @Override
    public void hideLoading() {
        Log.d(TAG, "hide loading ...");
    }

    @Override
    public void showError(String messageError) {
        Log.d(TAG, "error " + messageError);
    }

    @Override
    public void logout(boolean isLogout) {
        if (isLogout) {
            this.startActivity(LoginActivity.getCallIntent(this));
            finish();
        } else {
            this.showToastMessage("Error al intentar cerrar sesion");
            Log.d(TAG, "logout: error");
        }
    }

    public void seePlagueTutorial(Boolean seeTutorial) {
        Log.d(TAG, "seeTutorial: " + seeTutorial);
        String title = "Plagas que afectan al pepino dulce.";
        String subtitle = "En este sección puedes ver toda la colección de las principales plagas.";
        View view = findViewById(R.id.navigation_library);
        if (seeTutorial) {
            this.showTutorial(title, subtitle, view);
        }
    }

    @Override
    public void seeHistoryTutorial(Boolean seeTutorial) {
        Log.d(TAG, "seeTutorial: " + seeTutorial);
        String title = "Rconocimiento de plagas.";
        String subtitle = "En este sección puedes ver el historial de reconocimiento de plagas.";
        View view = findViewById(R.id.navigation_home);
        if (seeTutorial) {
            this.showTutorial(title, subtitle, view);
        }
    }
}
