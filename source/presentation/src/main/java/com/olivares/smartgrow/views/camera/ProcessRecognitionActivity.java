package com.olivares.smartgrow.views.camera;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Environment;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.olivares.entities.Image;
import com.olivares.entities.Query;
import com.olivares.entities.User;
import com.olivares.smartgrow.R;
import com.olivares.smartgrow.di.components.ApplicationComponent;
import com.olivares.smartgrow.di.components.CameraComponent;
import com.olivares.smartgrow.di.components.DaggerCameraComponent;
import com.olivares.smartgrow.di.injector.DependencyInjector;
import com.olivares.smartgrow.views.custom.ViewDialog;
import com.olivares.smartgrow.views.camera.result.ResultActivity;
import com.olivares.smartgrow.views.home.HomeActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProcessRecognitionActivity extends AppCompatActivity implements CameraPresenter.CameraView {
    private static final String TAG = ProcessRecognitionActivity.class.getName();
    private static final String IMAGE_NAME_RECOGNITION = "smartgrow";
    File imagefile = new File(Environment.getExternalStorageDirectory() + "/DCIM/", IMAGE_NAME_RECOGNITION + ".jpg");
    String imgPath = imagefile.getAbsolutePath();

    @BindView(R.id.iv_captured_image)
    ImageView mImageViewCapturedImage;
    @BindView(R.id.progress_container)
    ConstraintLayout mConstraintLayoutProgress;

    @Inject
    CameraPresenter cameraPresenter;
    CameraComponent cameraComponent;
    @Inject
    Gson gson;

    Bitmap mBitmapAdjust = null;
    Bitmap mBitmapImage = null;
    ViewDialog viewProgressDialog = null;

    public static Intent getCallIntent(Context context) {
        return new Intent(context, ProcessRecognitionActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process_recognition);
        ButterKnife.bind(this);
        setUpView();
        viewProgressDialog = new ViewDialog(this);
        viewProgressDialog.setiListenerViewDialog(this::cancelProcess);
        setUpData();
    }

    private void setUpView() {
        this.initializeInjector(DependencyInjector.applicationComponent());
        this.cameraComponent.inject(this);
        this.cameraPresenter.setCameraView(this);
    }

    private void setUpData() {
        Log.d(TAG, "setUpData");
        try {
            mBitmapImage = BitmapFactory.decodeFile(imgPath);
            ExifInterface exif = new ExifInterface(imgPath);
            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int rotationInDegrees = exifToDegrees(rotation);
            Matrix matrix = new Matrix();
            if (rotation != 0) {
                matrix.preRotate(rotationInDegrees);
            }
            mBitmapAdjust = Bitmap.createBitmap(mBitmapImage, 0, 0,
                    mBitmapImage.getWidth(), mBitmapImage.getHeight(), matrix, true);
            mImageViewCapturedImage.setImageBitmap(mBitmapAdjust);
            uploadFile(mBitmapAdjust);
        } catch (Exception e) {
            Log.d(TAG, "Error al cargar imagen");
            Toast.makeText(getApplicationContext(), "Error al cargar imagen", Toast.LENGTH_LONG).show();
        } finally {
            Log.d(TAG, "finally");
            if (mBitmapImage != null && !mBitmapImage.isRecycled()) {
                mBitmapImage.recycle();
                mBitmapImage = null;
            }
        }
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    private void initializeInjector(ApplicationComponent applicationComponent) {
        this.cameraComponent = DaggerCameraComponent.builder()
                .applicationComponent(applicationComponent)
                .build();
    }

    private void uploadFile(Bitmap bitmap) {
        Log.d(TAG, "call service uploadFile ------------------------------> ");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] b = baos.toByteArray();
        String encoded = Base64.encodeToString(b, Base64.DEFAULT);
        Log.d(TAG, encoded);
        this.cameraPresenter.uploadImage(new com.olivares.entities.Image(IMAGE_NAME_RECOGNITION, encoded));
    }

    @Override
    public void uploadSuccess(Image image) {
        Log.d(TAG, "uploadSuccess");
        this.cameraPresenter.recognitionImage(new User(1, "mayer", "molivars", "xxx"), image);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.cameraPresenter.destroy();
        if (viewProgressDialog != null) {
            viewProgressDialog.closeDialog();
            viewProgressDialog = null;
        }
        Log.d(TAG, "destoy --------------------------> ");
        if (mBitmapAdjust != null && !mBitmapAdjust.isRecycled()) {
            mBitmapAdjust.recycle();
            mBitmapAdjust = null;
        }
        if (mBitmapImage != null && !mBitmapImage.isRecycled()) {
            mBitmapImage.recycle();
            mBitmapImage = null;
        }
    }

    @Override
    public void reconizeSuccess(Query query) {
        Log.d(TAG, "reconizeSuccess");
        Log.d(TAG, "reconizeSuccess: " + query.getUrlImageQuery() + query.getResultList().get(0).getNamePlague());
        Intent intent = ResultActivity.getCallIntent(getApplicationContext(), gson.toJson(query.getResultList()));
        startActivity(intent);
        finish();
    }

    @Override
    public void showLoading() {
        Log.d(TAG, "showLoading");
        viewProgressDialog.showDialog("Procesando");
    }

    @Override
    public void hideLoading() {
        Log.d(TAG, "hideLoading");
        if (viewProgressDialog != null) {
            viewProgressDialog.closeDialog();
        }
    }

    @Override
    public void showError(String messageError) {
        Log.d(TAG, "showError: " + messageError);
        if (viewProgressDialog != null) {
            viewProgressDialog.closeDialog();
        }
        Toast.makeText(getApplicationContext(), messageError, Toast.LENGTH_LONG).show();
    }

    public void cancelProcess() {
        Log.d(TAG, "cancelProcess ----------->");
        this.cameraPresenter.destroy();
        if (viewProgressDialog != null) {
            viewProgressDialog.closeDialog();
            viewProgressDialog = null;
        }
        Intent intent = HomeActivity.getCallIntent(getApplicationContext());
        startActivity(intent);
        finish();
    }
}
