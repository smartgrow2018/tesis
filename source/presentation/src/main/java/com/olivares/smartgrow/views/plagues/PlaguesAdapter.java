package com.olivares.smartgrow.views.plagues;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.olivares.smartgrow.R;
import com.olivares.entities.Plague;
import com.olivares.smartgrow.util.Util;
import com.squareup.picasso.Picasso;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlaguesAdapter extends RecyclerView.Adapter<PlaguesAdapter.PlagueViewHolder> {
    private List<Plague> plagueList;
    private PlagueItemClickListener plagueItemClickListener;
    private final LayoutInflater layoutInflater;
    private final Picasso picasso;

    @Inject
    public PlaguesAdapter(Context context, Picasso picasso) {
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.plagueList = Collections.emptyList();
        this.picasso = picasso;
    }

    public void setLibraryList(Collection<Plague> plaguesList) {
        this.plagueList = (List<Plague>) plaguesList;
        this.notifyDataSetChanged();
    }

    public void setLibraryItemClickListener(PlagueItemClickListener plagueItemClickListener) {
        this.plagueItemClickListener = plagueItemClickListener;
    }

    private void loadImagePicasso(ImageView imageView, String url) {
        this.picasso.load(url)
                .placeholder(R.drawable.smartgrow_with_text)
                .into(imageView);
    }

    @NonNull
    @Override
    public PlagueViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = this.layoutInflater.inflate(R.layout.card_library2, parent, false);
        return new PlagueViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PlagueViewHolder holder, int position) {
        final Plague plague = this.plagueList.get(position);
        holder.textNamePlague.setText(plague.getName());
        holder.textDescriptionPlague.setText(Util.formatResumeLibrary(plague.getDescription()));
        this.loadImagePicasso(holder.imageViewPlague, plague.getUrlImage());
        holder.itemLibrary.setOnClickListener(view -> this.plagueItemClickListener.onPlagueItemClick(plague));
        setFadeInAnimation(holder.itemView);
    }

    public void setFadeInAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }


    @Override
    public void onViewDetachedFromWindow(@NonNull PlagueViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }

    @Override
    public int getItemCount() {
        return (this.plagueList != null) ? this.plagueList.size() : 0;
    }

    public static class PlagueViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_name_plague)
        TextView textNamePlague;
        @BindView(R.id.text_description_plague)
        TextView textDescriptionPlague;
        @BindView(R.id.image_plague)
        ImageView imageViewPlague;
        @BindView(R.id.item_library)
        CardView itemLibrary;

        public PlagueViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface PlagueItemClickListener {
        void onPlagueItemClick(Plague plague);
    }

}
