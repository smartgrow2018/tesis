package com.olivares.smartgrow.views.home;

import android.annotation.SuppressLint;

import com.olivares.entities.Query;
import com.olivares.smartgrow.views.LoadView;
import com.olivares.smartgrow.views.Presenter;
import com.olivares.usecase.usecase.history.GetHistoryRecognitionUseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class HistoryPresenter implements Presenter {
    private HistoryView historyView;
    private final GetHistoryRecognitionUseCase getHistoryRecognitionUseCase;

    @SuppressLint("CheckResult")
    public void loadHistory() {
        Single.create((SingleOnSubscribe<List<Query>>) emmiter -> emmiter.onSuccess(this.getHistoryRecognitionUseCase.getHistoryRecognition()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> {
                    if (this.historyView == null) return;
                    this.historyView.showLoading();
                })
                .doFinally(() -> {
                    if (this.historyView == null) return;
                    this.historyView.hideLoading();
                })
                .subscribe(
                        querylist -> {
                            if (this.historyView == null) return;
                            this.historyView.showHistoryList(querylist);
                        },
                        error -> {
                            if (this.historyView == null) return;
                            this.historyView.showError(error.getMessage());
                        }
                );
    }

    @Inject
    public HistoryPresenter(GetHistoryRecognitionUseCase getHistoryRecognitionUseCase) {
        this.getHistoryRecognitionUseCase = getHistoryRecognitionUseCase;
    }

    public void setHistoryView(HistoryView historyView) {
        this.historyView = historyView;
    }

    public void goToHistoryDetail(Query query) {
        this.historyView.showHistoryView(query);
    }

    public void goToShare(Query query) {
        this.historyView.shareQuery(query);
    }

    public void goToRemove(Query query) {
        // call service for delete query
        this.historyView.removeQuery(query);
    }

    @Override
    public void destroy() {
        this.historyView = null;
    }

    public interface HistoryView extends LoadView {
        void showHistoryList(List<Query> queryList);

        void showHistoryView(Query query);

        void shareQuery(Query query);

        void removeQuery(Query query);
    }

}
