package com.olivares.smartgrow.views.onboarding;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.olivares.smartgrow.R;
import com.olivares.entities.OnBoardItem;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

public class OnboardingAdapter extends PagerAdapter {

    private final LayoutInflater layoutInflater;
    private List<OnBoardItem> onBoardItemList;

    @Inject
    public OnboardingAdapter(Context context) {
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.onBoardItemList = Collections.emptyList();
    }

    public void setOnBoardItemList(Collection<OnBoardItem> onBoardItemList) {
        this.onBoardItemList = (List<OnBoardItem>) onBoardItemList;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        final View view = this.layoutInflater.inflate(R.layout.onboard_item, null);
        ImageView icon = view.findViewById(R.id.iv_onboard);
        TextView title = view.findViewById(R.id.tv_header);
        TextView description = view.findViewById(R.id.tv_desc);
        final OnBoardItem onBoardItem = this.onBoardItemList.get(position);
        title.setText(onBoardItem.getTitle());
        description.setText(onBoardItem.getDescription());
        icon.setImageResource(onBoardItem.getImage());
        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }

    @Override
    public int getCount() {
        return (this.onBoardItemList != null) ? this.onBoardItemList.size() : 0;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view == object);
    }
}
