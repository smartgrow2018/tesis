package com.olivares.smartgrow.di.components;

import android.content.Context;

import com.google.gson.Gson;
import com.olivares.smartgrow.di.modules.ApplicationModule;
import com.olivares.smartgrow.di.modules.RepositoryModule;
import com.olivares.usecase.repository.history.HistoryRecognitionRepositoryLocal;
import com.olivares.usecase.repository.history.HistoryRecognitionRepositoryRemote;
import com.olivares.usecase.repository.photography.PhotographyRepositoryRemote;
import com.olivares.usecase.repository.plagues.PlaguesRepositoryLocal;
import com.olivares.usecase.repository.plagues.PlaguesRepositoryRemote;
import com.olivares.usecase.repository.recognize.PlaguesRecognitionRepositoryRemote;
import com.olivares.usecase.repository.user.UserRepositoryLocal;
import com.olivares.usecase.repository.user.UserRepositoryPref;
import com.olivares.usecase.repository.user.UserRepositoryRemote;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, RepositoryModule.class})
public interface ApplicationComponent {
    Context context();

    Gson gson();

    Picasso picasso();

    UserRepositoryRemote loginRepositoryRemote();

    UserRepositoryLocal userRepositoryLocal();

    UserRepositoryPref userRepositoryPref();

    PhotographyRepositoryRemote imageRepositoryRemote();

    PlaguesRecognitionRepositoryRemote recognitionRepositoryRemote();

    PlaguesRepositoryRemote plaguesRepositoryRemote();

    PlaguesRepositoryLocal plaguesRespositoryLocal();

    HistoryRecognitionRepositoryRemote historyRepositoryRemote();

    HistoryRecognitionRepositoryLocal historyRepositoryLocal();

}
