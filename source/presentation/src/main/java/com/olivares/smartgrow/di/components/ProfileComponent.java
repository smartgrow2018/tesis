package com.olivares.smartgrow.di.components;


import com.olivares.smartgrow.di.modules.ActivityModule;
import com.olivares.smartgrow.di.scopes.PerActivity;
import com.olivares.smartgrow.views.history.HomeFragment;
import com.olivares.smartgrow.views.profile.ProfileActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class})
public interface ProfileComponent {
    void inject(ProfileActivity profileActivity);

    void inject(HomeFragment homeFragment);
}
