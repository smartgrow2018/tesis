package com.olivares.smartgrow.views.login;

import javax.inject.Inject;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import com.olivares.entities.Plague;
import com.olivares.smartgrow.di.scopes.PerActivity;
import com.olivares.smartgrow.views.LoadView;
import com.olivares.smartgrow.views.Presenter;
import com.olivares.entities.User;
import com.olivares.usecase.usecase.history.GetHistoryRecognitionUseCase;
import com.olivares.usecase.usecase.plagues.GetPlaguesUseCase;
import com.olivares.usecase.usecase.user.GetUserAccountDataUseCase;
import com.olivares.usecase.usecase.user.LoginUserAccountUseCase;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@PerActivity
public class LoginPresenter implements Presenter {
    private final LoginUserAccountUseCase loginUserAccountUseCase;
    private final GetUserAccountDataUseCase getUserAccountDataUseCase;
    private final GetHistoryRecognitionUseCase getHistoryRecognitionUseCase;
    private final GetPlaguesUseCase getPlaguesUseCase;
    private LoginView loginView;
    private UserCloseSessionView userCloseSessionView;
    private UserLoggedSessionView userLoggedSessionView;

    @Inject
    public LoginPresenter(LoginUserAccountUseCase loginUserAccountUseCase,
                          GetUserAccountDataUseCase getUserAccountDataUseCase,
                          GetHistoryRecognitionUseCase getHistoryRecognitionUseCase,
                          GetPlaguesUseCase getPlaguesUseCase) {
        this.loginUserAccountUseCase = loginUserAccountUseCase;
        this.getUserAccountDataUseCase = getUserAccountDataUseCase;
        this.getHistoryRecognitionUseCase = getHistoryRecognitionUseCase;
        this.getPlaguesUseCase = getPlaguesUseCase;
    }

    public void setLoginView(@NonNull LoginView loginView) {
        this.loginView = loginView;
    }

    public void setUserCloseSessionView(UserCloseSessionView userCloseSessionView) {
        this.userCloseSessionView = userCloseSessionView;
    }

    public LoginView getLoginView() {
        return loginView;
    }

    public void setUserLoggedSessionView(UserLoggedSessionView userLoggedSessionView) {
        this.userLoggedSessionView = userLoggedSessionView;
    }

    @Override
    public void destroy() {
        this.loginView = null;
        this.userCloseSessionView = null;
        this.userLoggedSessionView = null;
    }

    @SuppressLint("CheckResult")
    public void loginUser(User user) {
        Single.create((SingleOnSubscribe<User>) emmiter -> emmiter
                .onSuccess(this.loginUserAccountUseCase.loginUser(user)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> {
                    if (this.loginView == null) return;
                    this.loginView.showLoading();
                })
                .doFinally(() -> {
                    if (this.loginView == null) return;
                    this.loginView.hideLoading();
                })
                .subscribe(
                        user1 -> {
                            if (this.loginView == null) return;
                            this.loginView.isLogin(user1);
                        },
                        error -> {
                            if (this.loginView == null) return;
                            this.loginView.showError(error.getMessage());
                        }
                );
    }

    @SuppressLint("CheckResult")
    public void loadPlagues() {
        Single.create((SingleOnSubscribe<List<Plague>>) emmiter -> emmiter
                .onSuccess(this.getPlaguesUseCase.getPlagues()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> {
                    if (this.loginView == null) return;
                    this.loginView.showLoading();
                })
                .doFinally(() -> {
                    if (this.loginView == null) return;
                    this.loginView.hideLoading();
                })
                .subscribe(
                        success -> {
                            if (this.loginView == null) return;
                            this.loginView.isLoadPlagues(true);
                        },
                        error -> {
                            if (this.loginView == null) return;
                            this.loginView.showError(error.getMessage());
                        }
                );
    }

    @SuppressLint("CheckResult")
    public void loadRecognitions() {
        Single.create((SingleOnSubscribe<Boolean>) emmiter -> emmiter
                .onSuccess(this.getHistoryRecognitionUseCase.loadHistoryRecognition()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> {
                    if (this.loginView == null) return;
                    this.loginView.showLoading();
                })
                .doFinally(() -> {
                    if (this.loginView == null) return;
                    this.loginView.hideLoading();
                })
                .subscribe(
                        success -> {
                            if (this.loginView == null) return;
                            this.loginView.isLoadRecognitions(success);
                        },
                        error -> {
                            if (this.loginView == null) return;
                            this.loginView.showError(error.getMessage());
                        }
                );
    }

    public void goToHome() {
        this.userLoggedSessionView.showHome();
    }

    @SuppressLint("CheckResult")
    public void saveUserAccount(User user) {
        Single.create((SingleOnSubscribe<Boolean>) emitter -> emitter
                .onSuccess(this.loginUserAccountUseCase.saveUserLogged(user)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> {
                    if (this.loginView == null) return;
                    this.loginView.showLoading();
                })
                .doFinally(() -> {
                    if (this.loginView == null) return;
                    this.loginView.hideLoading();
                })
                .subscribe(
                        success -> {
                            if (this.loginView == null) return;
                            this.loginView.isSave(success);
                        },
                        error -> {
                            if (this.loginView == null) return;
                            this.loginView.showError(error.getMessage());
                        }
                );
    }

    @SuppressLint("CheckResult")
    public void getUser() {
        Single.create((SingleOnSubscribe<User>) emitter -> emitter
                .onSuccess(this.getUserAccountDataUseCase.getUserLogged()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> {
                    if (this.userLoggedSessionView == null) return;
                    this.userLoggedSessionView.showLoading();
                })
                .doFinally(() -> {
                    if (this.userLoggedSessionView == null) return;
                    this.userLoggedSessionView.hideLoading();
                })
                .subscribe(
                        user -> {
                            if (this.userLoggedSessionView == null) return;
                            if (user != null)
                                this.userLoggedSessionView.existUser(true);
                            else
                                this.userLoggedSessionView.existUser(false);
                        },
                        error -> {
                            if (this.userLoggedSessionView == null) return;
                            this.userLoggedSessionView.showError(error.getMessage());
                        }
                );
    }

    @SuppressLint("CheckResult")
    public void logoutUser() {
        Single.create((SingleOnSubscribe<Boolean>) emitter -> emitter.onSuccess(
                this.loginUserAccountUseCase.logoutUser()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> {
                    if (this.loginView == null) return;
                    this.userCloseSessionView.showLoading();
                })
                .doFinally(() -> {
                    if (this.userCloseSessionView == null) return;
                    this.userCloseSessionView.hideLoading();
                })
                .subscribe(
                        isLogout -> {
                            if (this.userCloseSessionView == null) return;
                            this.userCloseSessionView.logout(isLogout);
                        },
                        error -> {
                            if (this.userCloseSessionView == null) return;
                            this.userCloseSessionView.showError(error.getMessage());
                        }
                );
    }


    public interface LoginView extends LoadView {
        void isLogin(User user);

        void isSave(Boolean isSave);

        void isLoadPlagues(Boolean isLoad);

        void isLoadRecognitions(Boolean isLoad);
    }

    public interface UserLoggedSessionView extends LoadView {
        void showHome();

        void existUser(Boolean exist);
    }

    public interface UserCloseSessionView extends LoadView {
        void logout(boolean isLogout);
    }

}
