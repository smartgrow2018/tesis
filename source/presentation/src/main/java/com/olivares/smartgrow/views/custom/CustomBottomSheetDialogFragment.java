package com.olivares.smartgrow.views.custom;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.olivares.entities.Result;
import com.olivares.smartgrow.R;
import com.olivares.smartgrow.di.components.ApplicationComponent;
import com.olivares.smartgrow.di.components.CameraComponent;
import com.olivares.smartgrow.di.components.DaggerCameraComponent;
import com.olivares.smartgrow.di.injector.DependencyInjector;
import com.olivares.smartgrow.util.Connectivity;
import com.olivares.smartgrow.views.camera.CameraPresenter;
import com.olivares.smartgrow.views.camera.result.ResultAdapter;
import com.olivares.smartgrow.views.plagues.detail.PlagueDetailActivity;

import java.util.ArrayList;
import java.util.List;


public class CustomBottomSheetDialogFragment extends android.support.design.widget.BottomSheetDialogFragment implements CameraPresenter.ResultView {
    private static final String TAG = CustomBottomSheetDialogFragment.class.getName();
    RecyclerView recyclerView;
    ProgressBar mProgressBar;
    RecyclerView.LayoutManager layoutManager;
    ResultAdapter resultAdapter;
    CameraPresenter cameraPresenter;
    Gson mGson;

    CameraComponent cameraComponent;
    private List<Result> resultList = new ArrayList<>();
    private ResultAdapter.ResultItemClickListener resultItemClickListener = this::goToResultItem;

    public static CustomBottomSheetDialogFragment newInstance() {
        return new CustomBottomSheetDialogFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Log.d(TAG, "onCreateDialog");
        return super.onCreateDialog(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.bottom_sheet, container, false);
        recyclerView = view.findViewById(R.id.rv_results_history);
        mProgressBar = view.findViewById(R.id.progressBar_bottom_sheet);
        setUpView();
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        this.resultAdapter = null;
        this.cameraPresenter = null;
        this.mGson = null;
        this.cameraComponent = null;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        Log.d(TAG, "setupDialog");
        super.setupDialog(dialog, style);
    }

    public void setResultAdapter(ResultAdapter resultAdapter) {
        this.resultAdapter = resultAdapter;
    }

    public void setCameraPresenter(CameraPresenter cameraPresenter) {
        this.cameraPresenter = cameraPresenter;
    }

    public void setmGson(Gson mGson) {
        this.mGson = mGson;
    }

    protected void setUpView() {
        this.initializeInjector(DependencyInjector.applicationComponent());
        this.cameraComponent.inject(this);
        this.cameraPresenter.setResultView(this);
        setUpRecyclerView();
    }

    private void initializeInjector(ApplicationComponent applicationComponent) {
        this.cameraComponent = DaggerCameraComponent.builder()
                .applicationComponent(applicationComponent)
                .build();
    }

    private void setUpRecyclerView() {
        this.resultAdapter.setResultItemClickListener(resultItemClickListener);
        this.resultAdapter.setNormalType(false);
        this.recyclerView.setHasFixedSize(true);
        this.layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        this.recyclerView.setLayoutManager(layoutManager);
        this.recyclerView.setAdapter(resultAdapter);
        LayoutAnimationController layoutAnimationController = AnimationUtils.loadLayoutAnimation(getContext(), R.anim.layout_animation_history2);
        this.recyclerView.setLayoutAnimation(layoutAnimationController);
        this.recyclerView.scheduleLayoutAnimation();
    }

    public void goToResultItem(Result result) {
        Log.d(TAG, "ver detalles plaga");
        if (Connectivity.isConnected(getActivity())) {
            this.showResultView(result);
        } else {
            Toast.makeText(getActivity(), "No tiene conexion a internet", Toast.LENGTH_LONG).show();
        }
    }

    public void setResultList(List<Result> resultList) {
        this.resultList.addAll(resultList);
        this.showResultList(this.resultList);
    }

    @Override
    public void showResultList(List<Result> resultList) {
        this.resultAdapter.setResultList(resultList);
    }

    @Override
    public void showResultView(Result result) {
        Log.d(TAG, "showResultDetail");
        startActivity(PlagueDetailActivity.getCallIntent(getContext(), mGson.toJson(result), 1));
    }

    @Override
    public void showLoading() {
        Log.d(TAG, "showLoading");
    }

    @Override
    public void hideLoading() {
        Log.d(TAG, "hideLoading");
    }

    @Override
    public void showError(String messageError) {
        Log.d(TAG, "showError: " + messageError);
    }
}
