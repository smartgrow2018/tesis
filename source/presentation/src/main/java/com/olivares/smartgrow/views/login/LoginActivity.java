package com.olivares.smartgrow.views.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.olivares.smartgrow.R;
import com.olivares.smartgrow.di.components.ApplicationComponent;
import com.olivares.smartgrow.di.components.DaggerLoginComponent;
import com.olivares.smartgrow.di.components.LoginComponent;
import com.olivares.smartgrow.di.injector.DependencyInjector;
import com.olivares.smartgrow.views.BaseActivity;
import com.olivares.smartgrow.views.home.HomeActivity;
import com.olivares.entities.User;
import com.olivares.smartgrow.views.register.RegisterActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements LoginPresenter.LoginView, LoginPresenter.UserLoggedSessionView {

    private static final String TAG = LoginActivity.class.getName();

    @BindView(R.id.edit_user_login_activity)
    EditText mEditUsername;
    @BindView(R.id.edit_password_login_activity)
    EditText mEditPassword;
    @BindView(R.id.button_login_activity)
    Button mButtonLogin;
    @BindView(R.id.text_forgot_password_login_activity)
    TextView mTextViewForgotPassword;
    @BindView(R.id.progressBar_login_activity)
    ProgressBar mProgressBar;
    private User user;

    @Inject
    LoginPresenter loginPresenter;
    LoginComponent loginComponent;

    @Inject
    Gson gson;


    public static Intent getCallIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        this.initializeInjector(DependencyInjector.applicationComponent());
        this.loginComponent.inject(this);
        this.loginPresenter.setLoginView(this);
        this.loginPresenter.setUserLoggedSessionView(this);
        this.initLoggedUser();
        this.user = new User();
        this.initEditTextFocus();
    }


    private void initEditTextFocus() {
        mEditUsername.setOnFocusChangeListener((View view, boolean b) -> {
            if (b) {
                mEditUsername.setBackgroundResource(R.drawable.edit_style2);
            } else {
                mEditUsername.setBackgroundResource(R.drawable.edit_style);
            }
        });

        mEditPassword.setOnFocusChangeListener((View view, boolean b) -> {
            if (b) {
                mEditPassword.setBackgroundResource(R.drawable.edit_style2);
            } else {
                mEditPassword.setBackgroundResource(R.drawable.edit_style);
            }
        });
    }

    private void enableViews(Boolean enable) {
        mEditUsername.setEnabled(enable);
        mEditPassword.setEnabled(enable);
    }

    private void initLoggedUser() {
        this.loginPresenter.getUser();
    }

    private void initializeInjector(ApplicationComponent applicationComponent) {
        this.loginComponent = DaggerLoginComponent.builder()
                .applicationComponent(applicationComponent)
                .build();
    }

    @OnClick(R.id.button_login_activity)
    public void clickButtonLoginActivity() {
        if (validateEdiText()) {
            Log.d(TAG, mEditUsername.getText().toString());
            this.user.setUsername(mEditUsername.getText().toString().trim());
            this.user.setPassword(mEditPassword.getText().toString().trim());
            this.loginUser();
        } else {
            this.showToastMessage("Ingresa Usuario o Contraseña");
        }
    }

    @OnClick(R.id.text_forgot_password_login_activity)
    public void clickTextViewFogotPassword() {
        //code for forgot password
        Log.d(TAG, "forgot password");
    }

    @OnClick(R.id.text_create_account)
    public void clickTextCreateAccount() {
        startActivity(RegisterActivity.getCallIntent(this));
    }

    private void loginUser() {
        this.loginPresenter.loginUser(user);
    }

    private boolean validateEdiText() {
        return !mEditUsername.getText().toString().trim().isEmpty() && !mEditPassword.getText().toString().isEmpty();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.loginPresenter.destroy();
    }

    @Override
    public void isLogin(User user) {
        if (user != null) {
            this.loginPresenter.saveUserAccount(user);
        } else {
            this.showToastMessage("Usuario o contraseña incorrecta.");
            Log.d(TAG, "isLogin: error user");
        }
    }

    @Override
    public void isSave(Boolean isSave) {
        if (isSave) {
            this.loginPresenter.loadPlagues();
        } else {
            this.showToastMessage("Error al guardar cuenta de usuario");
            Log.d(TAG, "isSave: error Save");
        }
    }

    @Override
    public void isLoadPlagues(Boolean isLoad) {
        if (isLoad) {
            this.loginPresenter.loadRecognitions();
        } else {
            Log.d(TAG, "Error: isLoadPlagues");
            this.showError("Error al cargar plagas");
        }
    }

    @Override
    public void isLoadRecognitions(Boolean isLoad) {
        Log.d(TAG, "isLoadRecognitions");
        if (isLoad) {
            this.loginPresenter.goToHome();
        } else {
            Log.d(TAG, "Error: isLoadRecognitions");
            this.showError("Error al cargar historial de reconocimientos");
        }
    }

    @Override
    public void showHome() {
        Log.d(TAG, "showHome");
        startActivity(HomeActivity.getCallIntent(this));
        finish();
    }

    @Override
    public void existUser(Boolean exist) {
        if (exist) {
            this.showHome();
        }
    }

    @Override
    public void showLoading() {
        Log.d(TAG, "showLoading");
        this.enableViews(false);
        this.mButtonLogin.setVisibility(View.INVISIBLE);
        this.mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        this.enableViews(true);
        this.mButtonLogin.setVisibility(View.VISIBLE);
        this.mProgressBar.setVisibility(View.INVISIBLE);
        Log.d(TAG, "hideLoading");
    }

    @Override
    public void showError(String messageError) {
        this.enableViews(true);
        this.mButtonLogin.setVisibility(View.VISIBLE);
        this.mProgressBar.setVisibility(View.INVISIBLE);
        Log.d(TAG, "showError: " + messageError);
        if (!messageError.equals("NOT_FOUND")) {
            this.showToastMessage(messageError);
        }
    }
}
