package com.olivares.smartgrow.di.components;

import com.olivares.smartgrow.di.modules.ActivityModule;
import com.olivares.smartgrow.di.scopes.PerActivity;
import com.olivares.smartgrow.views.camera.Camera2Fragment;
import com.olivares.smartgrow.views.camera.ProcessRecognitionActivity;
import com.olivares.smartgrow.views.camera.result.ResultActivity;
import com.olivares.smartgrow.views.custom.CustomBottomSheetDialogFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class})
public interface CameraComponent {

    void inject(Camera2Fragment camera2Fragment);

    void inject(ResultActivity resultActivity);

    void inject(CustomBottomSheetDialogFragment customBottomSheetDialogFragment);

    void inject(ProcessRecognitionActivity processRecognitionActivity);
}
