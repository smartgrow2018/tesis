package com.olivares.smartgrow.views.custom;


import android.content.Context;
import android.content.Intent;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.olivares.entities.Image;
import com.olivares.entities.Plague;
import com.olivares.smartgrow.R;
import com.olivares.smartgrow.di.components.ApplicationComponent;
import com.olivares.smartgrow.di.components.DaggerLibraryComponent;
import com.olivares.smartgrow.di.components.LibraryComponent;
import com.olivares.smartgrow.di.injector.DependencyInjector;
import com.olivares.smartgrow.views.plagues.PlaguesPresenter;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ImageFullScreenScreenActivity extends AppCompatActivity implements PlaguesPresenter.PlagueImageFullScreenView {
    private static final String TAG = ImageFullScreenScreenActivity.class.getName();
    @BindView(R.id.toolbar_image_fullscreen)
    Toolbar toolbarImageFullscreen;
    @BindView(R.id.image_full_screen_plague)
    ImageView imageView;
    private static final String TYPE = "type";
    private static final String PLAGUE = "plague";
    @Inject
    Picasso picasso;
    LibraryComponent libraryComponent;
    @Inject
    PlaguesPresenter plaguesPresenter;
    @Inject
    Gson gson;

    // These matrices will be used to move and zoom image
    Matrix matrix = new Matrix();
    Matrix savedMatrix = new Matrix();
    // We can be in one of these 3 states
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    int mode = NONE;
    // Remember some things for zooming
    PointF start = new PointF();
    PointF mid = new PointF();
    float oldDist = 1f;
    int clickCount = 0;
    /*variable for storing the time of first click*/
    long startTime;
    /* variable for calculating the total time*/
    long duration;
    /* constant for defining the time duration between the click that can be considered as double-tap */
    static final int MAX_DURATION = 300;


    public static Intent getCallIntent(Context context, String plague, int type) {
        Intent intent = new Intent(context, ImageFullScreenScreenActivity.class);
        intent.putExtra(PLAGUE, plague);
        intent.putExtra(TYPE, type);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_image_full_screen);
        ButterKnife.bind(this);
        setUpView();
        retriveArguments();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        this.plaguesPresenter.onInitPositionImageView();
    }

    private void setUpView() {
        this.initializeInjector(DependencyInjector.applicationComponent());
        this.libraryComponent.inject(this);
        this.plaguesPresenter.setPlagueImageFullScreenView(this);
    }

    private void initializeInjector(ApplicationComponent applicationComponent) {
        this.libraryComponent = DaggerLibraryComponent.builder()
                .applicationComponent(applicationComponent)
                .build();
    }

    public void retriveArguments() {
        final int type = getIntent().getIntExtra(TYPE, 3);
        if (type == 1) {
            Plague mPlague = this.gson.fromJson(getIntent().getStringExtra(PLAGUE), Plague.class);
            loadImagePicasso(imageView, mPlague.getUrlImage());
            showToolbar(mPlague.getName());
        } else if (type == 0) {
            final Image image = this.gson.fromJson(getIntent().getStringExtra(PLAGUE), Image.class);
            loadImagePicasso(imageView, image.getLink());
            showToolbar(image.getName());
        } else {
            Log.d(TAG, "error no hay imagen para mostrar");
            finish();
        }
        imageView.setImageMatrix(matrix);
    }

    private void loadImagePicasso(ImageView imageView, String url) {
        this.picasso.load(url)
                .placeholder(R.drawable.not_image)
                .into(imageView);
    }

    private void showToolbar(String title) {
        setSupportActionBar(toolbarImageFullscreen);
        Objects.requireNonNull(getSupportActionBar()).setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void onInitPositionImageView() {
        Log.d(TAG, "init view image");
        float imageWidth = imageView.getDrawable().getIntrinsicWidth();
        float imageHeight = imageView.getDrawable().getIntrinsicHeight();
        RectF drawableRect = new RectF(0, 0, imageWidth, imageHeight);
        RectF viewRect = new RectF(0, 0, imageView.getWidth(),
                imageView.getHeight());
        Matrix matrix = new Matrix();
        matrix.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.CENTER);
        imageView.setImageMatrix(matrix);
        this.matrix.set(matrix);
        imageView.invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                savedMatrix.set(matrix);
                start.set(event.getX(), event.getY());
                Log.d(TAG, "mode=DRAG");
                startTime = System.currentTimeMillis();
                clickCount++;
                mode = DRAG;
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                oldDist = spacing(event);
                Log.d(TAG, "oldDist=" + oldDist);
                if (oldDist > 10f) {
                    savedMatrix.set(matrix);
                    midPoint(mid, event);
                    mode = ZOOM;
                    Log.d(TAG, "mode=ZOOM");
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                mode = NONE;
                Log.d(TAG, "mode=NONE");
                Log.d(TAG, "clickcount -> " + clickCount);
                long time = System.currentTimeMillis() - startTime;
                duration = duration + time;
                if (clickCount == 2) {
                    if (duration <= MAX_DURATION) {
                        this.plaguesPresenter.onInitPositionImageView();
                    }
                    clickCount = 0;
                    duration = 0;
                    break;
                }
                if (duration > MAX_DURATION) {
                    clickCount = 0;
                    duration = 0;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                Log.d(TAG, "mode=MOVE");
                if (mode == DRAG) {
                    matrix.set(savedMatrix);
                    matrix.postTranslate(event.getX() - start.x, event.getY() - start.y);
                } else if (mode == ZOOM) {
                    float newDist = spacing(event);
                    Log.d(TAG, "newDist=" + newDist);
                    if (newDist > 10f) {
                        matrix.set(savedMatrix);
                        float scale = newDist / oldDist;
                        matrix.postScale(scale, scale, mid.x, mid.y);
                    }
                }
                break;
        }
        imageView.setImageMatrix(matrix);
        return true;
    }


    /**
     * Determine the space between the first two fingers
     */
    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);
    }

    /**
     * Calculate the mid point of the first two fingers
     */
    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }
}
