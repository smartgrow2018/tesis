package com.olivares.smartgrow.views.custom;

import android.app.Activity;
import android.app.Dialog;
import android.widget.Button;
import android.widget.TextView;

import com.olivares.smartgrow.R;

public class ViewDialog {
    private Dialog dialog;
    private IListenerViewDialog iListenerViewDialog;

    public ViewDialog(Activity activity) {
        dialog = new Dialog(activity);
    }

    public void setiListenerViewDialog(IListenerViewDialog iListenerViewDialog) {
        this.iListenerViewDialog = iListenerViewDialog;
    }

    public void showDialog(String msg) {
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        TextView text = dialog.findViewById(R.id.text_dialog);
        text.setText(msg);
        Button dialogButton = dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(v -> {
            dialog.dismiss();
            iListenerViewDialog.cancelProcess();
        });
        dialog.show();
    }

    public void closeDialog() {
        dialog.dismiss();
    }

    public interface IListenerViewDialog {
        void cancelProcess();
    }
}
