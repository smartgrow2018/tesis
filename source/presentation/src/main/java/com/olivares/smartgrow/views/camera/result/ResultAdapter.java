package com.olivares.smartgrow.views.camera.result;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.olivares.smartgrow.R;
import com.olivares.entities.Result;
import com.olivares.smartgrow.util.Util;
import com.squareup.picasso.Picasso;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResultAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Result> resultList;
    private ResultItemClickListener resultItemClickListener;
    private LayoutInflater layoutInflater;
    private final Picasso picasso;
    private static final int TYPE_NORMAL = 0;
    private static final int TYPE_BOTTOM_SHEET = 1;
    private boolean normalType;

    @Inject
    public ResultAdapter(Context context, Picasso picasso) {
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.resultList = Collections.emptyList();
        this.picasso = picasso;
    }


    public void setResultList(Collection<Result> resultList) {
        this.resultList = (List<Result>) resultList;
        this.notifyDataSetChanged();
    }

    public void setResultItemClickListener(ResultItemClickListener resultItemClickListener) {
        this.resultItemClickListener = resultItemClickListener;
    }

    public void setNormalType(boolean normalType) {
        this.normalType = normalType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_NORMAL) {
            final View view = this.layoutInflater.inflate(R.layout.row_result, parent, false);
            return new ResultViewHolder(view);
        } else {
            final View view = this.layoutInflater.inflate(R.layout.card_result_item, parent, false);
            return new CustomResultViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final Result result = this.resultList.get(position);
        if (holder instanceof ResultViewHolder) {
            ResultViewHolder viewHolder = (ResultViewHolder) holder;
            viewHolder.txtResultNamePlague.setText(result.getNamePlague());
            viewHolder.txtResultDescriptionPlague.setText(result.getNamePlague());
            viewHolder.txtResultPercent.setText(Util.percentFormat(result.getPercentResult()));
            loadImagePicasso(viewHolder.imageResult, result.getUrlImagePlague());
            viewHolder.itemResult.setOnClickListener(view -> this.resultItemClickListener.onResultItemClick(result));
        } else {
            CustomResultViewHolder viewHolder = (CustomResultViewHolder) holder;
            viewHolder.txtResultNamePlague.setText(result.getNamePlague());
            viewHolder.txtResultDescriptionPlague.setText(Util.formatResumeLibrary(result.getDescriptionPlague()));
            viewHolder.txtResultPercent.setText(Util.percentFormat(result.getPercentResult()));
            viewHolder.progressBar.setProgress(Util.numberProgress(result.getPercentResult()));
            loadImagePicasso(viewHolder.imageResult, result.getUrlImagePlague());
            viewHolder.itemResult.setOnClickListener(view -> this.resultItemClickListener.onResultItemClick(result));
        }
    }

    private void loadImagePicasso(ImageView imageView, String url) {
        this.picasso.load(url)
                .placeholder(R.drawable.ai)
                .into(imageView);
    }

    @Override
    public int getItemViewType(int position) {
        if (this.normalType)
            return TYPE_NORMAL;
        return TYPE_BOTTOM_SHEET;
    }

    @Override
    public int getItemCount() {
        return (this.resultList != null) ? this.resultList.size() : 0;
    }

    public static class ResultViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.result_name_plague)
        TextView txtResultNamePlague;
        @BindView(R.id.result_description_plague)
        TextView txtResultDescriptionPlague;
        @BindView(R.id.text_result_plague)
        Button txtResultPercent;
        @BindView(R.id.item_result)
        ConstraintLayout itemResult;
        @BindView(R.id.image_plague_result)
        ImageView imageResult;

        public ResultViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class CustomResultViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.result_name_plague)
        TextView txtResultNamePlague;
        @BindView(R.id.result_description_plague)
        TextView txtResultDescriptionPlague;
        @BindView(R.id.text_result_plague)
        TextView txtResultPercent;
        @BindView(R.id.image_plague_result)
        ImageView imageResult;
        @BindView(R.id.item_result)
        CardView itemResult;
        @BindView(R.id.progressBar_percent_result)
        ProgressBar progressBar;

        public CustomResultViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface ResultItemClickListener {
        void onResultItemClick(Result result);
    }

}
