package com.olivares.smartgrow.views.plagues;

import android.annotation.SuppressLint;

import com.olivares.smartgrow.di.scopes.PerActivity;
import com.olivares.smartgrow.views.LoadView;
import com.olivares.entities.Plague;
import com.olivares.smartgrow.views.Presenter;
import com.olivares.usecase.usecase.plagues.GetPlagueDataUseCase;
import com.olivares.usecase.usecase.plagues.GetPlaguesUseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@PerActivity
public class PlaguesPresenter implements Presenter {
    private PlaguesView plaguesView;
    private PlaguesDetailView plaguesDetailView;
    private PlagueImageFullScreenView plagueImageFullScreenView;
    private final GetPlaguesUseCase getPlaguesUseCase;
    private final GetPlagueDataUseCase getPlagueDataUseCase;

    @Inject
    public PlaguesPresenter(GetPlaguesUseCase getPlaguesUseCase, GetPlagueDataUseCase getPlagueDataUseCase) {
        this.getPlaguesUseCase = getPlaguesUseCase;
        this.getPlagueDataUseCase = getPlagueDataUseCase;
    }

    public void setPlaguesView(PlaguesView plaguesView) {
        this.plaguesView = plaguesView;
    }

    public void setPlaguesDetailView(PlaguesDetailView plaguesDetailView) {
        this.plaguesDetailView = plaguesDetailView;
    }

    public void setPlagueImageFullScreenView(PlagueImageFullScreenView plagueImageFullScreenView) {
        this.plagueImageFullScreenView = plagueImageFullScreenView;
    }

    @SuppressLint("CheckResult")
    public void loadPlagues() {
        try {
            Single.create((SingleOnSubscribe<List<Plague>>) emmiter -> emmiter.onSuccess(this.getPlaguesUseCase.getPlagues()))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(__ -> {
                        if (this.plaguesView == null) return;
                        this.plaguesView.showLoading();
                    })
                    .doFinally(() -> {
                        if (this.plaguesView == null) return;
                        this.plaguesView.hideLoading();
                    })
                    .subscribe(
                            plagues -> {
                                if (this.plaguesView == null) return;
                                this.plaguesView.showPlagues(plagues);
                            },
                            error -> {
                                if (this.plaguesView == null) return;
                                this.plaguesView.showError(error.getMessage());
                            }
                    );
        } catch (Exception e) {
            if (this.plaguesView == null) return;
            this.plaguesView.showError(e.getMessage());
        }
    }

    @SuppressLint("CheckResult")
    public void loadPlagueData(int plagueId) {
        try {
            Single.create((SingleOnSubscribe<Plague>) emmiter -> emmiter.onSuccess(this.getPlagueDataUseCase.getPlagueData(plagueId)))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(__ -> {
                        if (this.plaguesDetailView == null) return;
                        this.plaguesDetailView.showLoading();
                    })
                    .doFinally(() -> {
                        if (this.plaguesDetailView == null) return;
                        this.plaguesDetailView.hideLoading();
                    })
                    .subscribe(
                            plague -> {
                                if (this.plaguesDetailView == null) return;
                                this.plaguesDetailView.showPlagueView(plague);
                            },
                            error -> {
                                if (this.plaguesDetailView == null) return;
                                this.plaguesDetailView.showError(error.getMessage());
                            }
                    );
        } catch (Exception e) {
            if (this.plaguesDetailView == null) return;
            this.plaguesDetailView.showError(e.getMessage());
        }
    }

    public void goToPlagueDetail(Plague plague) {
        this.plaguesView.showPlagueView(plague);
    }

    public void showImageFullScreen(Plague plague) {
        this.plaguesDetailView.showImageFullScreen(plague);
    }

    public void onInitPositionImageView() {
        this.plagueImageFullScreenView.onInitPositionImageView();
    }

    @Override
    public void destroy() {
        this.plaguesView = null;
        this.plaguesDetailView = null;
        this.plagueImageFullScreenView = null;
    }

    public interface PlaguesView extends LoadView {
        void showPlagues(List<Plague> plagues);

        void showPlagueView(Plague plague);
    }

    public interface PlaguesDetailView extends LoadView {
        void showPlagueView(Plague plague);

        void showImageFullScreen(Plague plague);

    }

    public interface PlagueImageFullScreenView {
        void onInitPositionImageView();

    }
}
