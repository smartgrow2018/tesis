package com.olivares.smartgrow.views.camera;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.olivares.entities.Image;
import com.olivares.entities.Query;
import com.olivares.entities.User;
import com.olivares.smartgrow.BuildConfig;
import com.olivares.smartgrow.R;
import com.olivares.smartgrow.di.components.ApplicationComponent;
import com.olivares.smartgrow.di.components.CameraComponent;
import com.olivares.smartgrow.di.components.DaggerCameraComponent;
import com.olivares.smartgrow.di.injector.DependencyInjector;
import com.olivares.smartgrow.views.custom.ViewDialog;
import com.olivares.smartgrow.views.camera.result.ResultActivity;

import java.io.File;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

public class Camera2Fragment extends Fragment implements CameraPresenter.CameraView {
    private static final String TAG = Camera2Fragment.class.getName();
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 1234;
    private static final int CAPTURE_IMAGE = 2203;
    private static final String IMAGE_NAME_RECOGNITION = "smartgrow";
    private boolean mPermissions;
    @BindView(R.id.take_picture_camera2)
    Button mButtonTakePicture;
    @BindView(R.id.txt_title_camera_frag)
    TextView mTextViewTitle;
    @BindView(R.id.txt_description_camera_frag)
    TextView mTextViewDescription;
    @BindView(R.id.cv_smartgrow_frag_cam2)
    CardView mCardViewSmartgrow;

    @BindView(R.id.recognize_container)
    ConstraintLayout mConstraintLayoutRecognize;

    @Inject
    CameraPresenter cameraPresenter;
    CameraComponent cameraComponent;
    @Inject
    Gson gson;


    File file = null;
    String imgPath = null;
    ViewDialog viewProgressDialog = null;

    public Camera2Fragment() {
        // Required empty public constructor
    }

    public static Camera2Fragment newInstance() {
        return new Camera2Fragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera2, container, false);
        ButterKnife.bind(this, view);
        Animation fromTopToBottom = AnimationUtils.loadAnimation(getContext(), R.anim.down_from_top);
        Animation fromBottomToTop = AnimationUtils.loadAnimation(getContext(), R.anim.up_from_bottom);
        mTextViewTitle.startAnimation(fromTopToBottom);
        mTextViewDescription.startAnimation(fromTopToBottom);
        mCardViewSmartgrow.startAnimation(fromBottomToTop);
        mButtonTakePicture.startAnimation(fromBottomToTop);
        setUpView();
        viewProgressDialog = new ViewDialog(getActivity());
        return view;
    }

    public void setUpView() {
        this.initializeInjector(DependencyInjector.applicationComponent());
        this.cameraComponent.inject(this);
        this.cameraPresenter.setCameraView(this);
    }

    private void initializeInjector(ApplicationComponent applicationComponent) {
        this.cameraComponent = DaggerCameraComponent.builder()
                .applicationComponent(applicationComponent)
                .build();
    }

    @OnClick(R.id.take_picture_camera2)
    public void examplePermission() {
        init();
    }

    private void init() {
        if (mPermissions) {
            if (checkCameraHardware()) {
                openCamera();
            } else {
                Toast.makeText(getContext(), "You need a camera to use this application", Toast.LENGTH_LONG).show();
            }
        } else {
            verifyPermissions();
        }
    }

    private boolean checkCameraHardware() {
        return Objects.requireNonNull(getContext()).getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    public void verifyPermissions() {
        Log.d(TAG, "verifyPermissions: asking user for permissions.");
        String[] permissions = {
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA};
        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(this.getActivity()),
                permissions[0]) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this.getActivity(),
                permissions[1]) == PackageManager.PERMISSION_GRANTED) {
            mPermissions = true;
            openCamera();
        } else {
            ActivityCompat.requestPermissions(getActivity(), permissions, REQUEST_CODE_ASK_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (REQUEST_CODE_ASK_PERMISSIONS == requestCode) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(), "OK Permissions granted ! " + Build.VERSION.SDK_INT, Toast.LENGTH_LONG).show();
                openCamera();
            } else {
                Toast.makeText(getContext(), "Permissions are not granted ! " + Build.VERSION.SDK_INT, Toast.LENGTH_LONG).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        viewProgressDialog.closeDialog();
        viewProgressDialog = null;
    }

    private void openCamera() {

        File folder = new File(Environment.getExternalStorageDirectory() + "/DCIM/");
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }
        if (success) {
            file = new File(Environment.getExternalStorageDirectory() + "/DCIM/" + IMAGE_NAME_RECOGNITION + ".jpg");
            Uri imgUri = FileProvider.getUriForFile(Objects.requireNonNull(getActivity()), BuildConfig.APPLICATION_ID
                    + ".provider", file);
            imgPath = file.getAbsolutePath();
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
            startActivityForResult(intent, CAPTURE_IMAGE);
        } else {
            Toast.makeText(getActivity(), "NO PUEDE CREAR DIRECCTORIO", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_CANCELED) {
            if (requestCode == CAPTURE_IMAGE && resultCode == RESULT_OK) {
                Log.d(TAG, "Correct Action " + resultCode);
                updloadImage();
            } else {
                Log.d(TAG, "other Action " + resultCode);
                super.onActivityResult(requestCode, resultCode, data);
            }
        } else {
            Log.d(TAG, "Cancel Action " + resultCode);
        }
    }

    private void updloadImage() {
        Log.d(TAG, "updloadImage ------------------------>");
        startActivity(ProcessRecognitionActivity.getCallIntent(getActivity()));
    }

    @Override
    public void uploadSuccess(Image image) {
        Log.d(TAG, "uploadSuccess");
        this.cameraPresenter.recognitionImage(new User(1, "mayer", "molivars", "xxx"), image);
    }

    @Override
    public void reconizeSuccess(Query query) {
        Log.d(TAG, "reconizeSuccess");
        Log.d(TAG, "reconizeSuccess: " + query.getUrlImageQuery() + query.getResultList().get(0).getNamePlague());
        // TODO: la imagen ya se reconocio, pasa a la otra activity enviando la lista de parametros
        Intent intent = ResultActivity.getCallIntent(getActivity(), gson.toJson(query.getResultList()));
        startActivity(intent);
    }

    @Override
    public void showLoading() {
        Log.d(TAG, "showLoading");
        //mConstraintLayoutRecognize.setVisibility(View.INVISIBLE);
        viewProgressDialog.showDialog("Procesando fotografía");
    }

    @Override
    public void hideLoading() {
        Log.d(TAG, "hideLoading");
        //mConstraintLayoutRecognize.setVisibility(View.VISIBLE);
        viewProgressDialog.closeDialog();
    }

    @Override
    public void showError(String messageError) {
        Log.d(TAG, "showError");
        //mConstraintLayoutRecognize.setVisibility(View.VISIBLE);
        viewProgressDialog.closeDialog();
        Toast.makeText(getActivity(), "Error: " + messageError, Toast.LENGTH_LONG).show();
    }
}
