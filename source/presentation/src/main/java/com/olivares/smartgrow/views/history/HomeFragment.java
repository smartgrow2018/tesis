package com.olivares.smartgrow.views.history;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.olivares.entities.Image;
import com.olivares.entities.Query;
import com.olivares.entities.User;
import com.olivares.smartgrow.R;
import com.olivares.smartgrow.di.components.ApplicationComponent;
import com.olivares.smartgrow.di.components.DaggerProfileComponent;
import com.olivares.smartgrow.di.components.ProfileComponent;
import com.olivares.smartgrow.di.injector.DependencyInjector;
import com.olivares.smartgrow.views.camera.CameraPresenter;
import com.olivares.smartgrow.views.camera.result.ResultAdapter;
import com.olivares.smartgrow.views.custom.CustomBottomSheetDialogFragment;
import com.olivares.smartgrow.views.custom.ImageFullScreenScreenActivity;
import com.olivares.smartgrow.views.profile.ProfilePresenter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.BlurTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;
import static com.bumptech.glide.request.RequestOptions.fitCenterTransform;
import static com.olivares.smartgrow.R.anim.*;

public class HomeFragment extends Fragment implements ProfilePresenter.ProfileView, ProfilePresenter.RecognitionsHistoryView {
    public static final String TAG = HomeFragment.class.getName();
    @BindView(R.id.recycler_history_fragment)
    RecyclerView recyclerViewLibrary;
    @BindView(R.id.swipe_refresh_history_fragent)
    SwipeRefreshLayout swipeRefreshLayoutLibrary;
    @BindView(R.id.txt_history_home_frag)
    TextView mTextViewHistory;
    @BindView(R.id.cv_smartgrow_frag_home)
    CardView mCardViewSmartgrow;
    @BindView(R.id.progress_history_home_frag)
    ProgressBar mProgressBar;

    @BindView(R.id.imageBackgroundProfile)
    ImageView mImageViewBackground;
    @BindView(R.id.imageUserProfile)
    ImageView mImageViewUserProfile;
    @BindView(R.id.textNameUserProfile)
    TextView mTextViewNameUser;
    @BindView(R.id.textDescriptionProfile)
    TextView mTextViewDescriptionUser;

    @Inject
    Picasso picasso;

    @Inject
    HistoryAdapter historyAdapter;
    @Inject
    Gson gson;
    List<Query> historyRecognitions;

    @Inject
    ResultAdapter resultAdapter;
    @Inject
    CameraPresenter cameraPresenter;

    @Inject
    ProfilePresenter profilePresenter;
    ProfileComponent profileComponent;
    User user;

    public HomeFragment() {
        // no require implementation
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        historyRecognitions = new ArrayList<>();
        user = new User();
        this.setUpProfileView();
        Animation fadeInAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in_long);
        mImageViewUserProfile.startAnimation(fadeInAnimation); //Set animation to your ImageView
        mImageViewBackground.startAnimation(fadeInAnimation); //Set animation to your ImageView
        mTextViewNameUser.startAnimation(fadeInAnimation); //Set animation to your ImageView
        mTextViewDescriptionUser.startAnimation(fadeInAnimation); //Set animation to your ImageView
        visibleView(mProgressBar, true, fadeInAnimation);
        Animation rigthToLeftAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.item_anim_left_to_rigth_short);
        mTextViewHistory.startAnimation(rigthToLeftAnimation);
        return view;
    }

    private void visibleView(View view, Boolean visible, Animation animation) {
        if (visible) {
            view.setVisibility(View.VISIBLE);
            mCardViewSmartgrow.startAnimation(animation);
        } else {
            view.setVisibility(View.INVISIBLE);
            mCardViewSmartgrow.startAnimation(animation);
        }
    }

    private void setUpProfileView() {
        this.initializeInjectorProfile(DependencyInjector.applicationComponent());
        this.profileComponent.inject(this);
        this.profilePresenter.setProfileView(this);
        this.profilePresenter.setRecognitionsHistoryView(this);
        this.swipeRefreshLayoutLibrary.setOnRefreshListener(this::initLoadHistory);
        this.setUpRecyclerView();
        this.initLoadHistory();
        this.setUpDataProfile();
    }

    private void setUpDataProfile() {
        this.profilePresenter.getUser();
    }

    private void initLoadHistory() {
        Log.d(TAG, "initLoadHistory");
        this.profilePresenter.loadRecognitionsHistory();
    }

    private void setUpRecyclerView() {
        this.historyAdapter.setHistoryItemClickListener(historyItemClickListener);
        this.historyAdapter.setHistoryImageClickListener(historyImageClickListener);
        this.historyAdapter.setHistoryItemMenuClickListener(historyItemMenuClickListener);
        recyclerViewLibrary.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        recyclerViewLibrary.setLayoutManager(layoutManager);
        recyclerViewLibrary.setAdapter(historyAdapter);
        LayoutAnimationController layoutAnimationController = AnimationUtils.
                loadLayoutAnimation(getContext(), R.anim.layout_animation_history2);
        recyclerViewLibrary.setLayoutAnimation(layoutAnimationController);
        recyclerViewLibrary.scheduleLayoutAnimation();
    }

    private void initializeInjectorProfile(ApplicationComponent applicationComponent) {
        this.profileComponent = DaggerProfileComponent.builder()
                .applicationComponent(applicationComponent)
                .build();
    }

    private HistoryAdapter.HistoryItemClickListener historyItemClickListener = (this::goToHistoryItem);
    private HistoryAdapter.HistoryImageClickListener historyImageClickListener = (this::showImageRecognition);
    private HistoryAdapter.HistoryItemMenuClickListener historyItemMenuClickListener = (this::clicMenuItem);

    public void goToHistoryItem(Query query) {
        this.profilePresenter.goToHistoryDetail(query);
    }

    public void showImageRecognition(Query query) {
        this.profilePresenter.showRecognitionImage(query);
    }

    public void clicMenuItem(Query query, View view) {
        PopupMenu popupMenu = new PopupMenu(Objects.requireNonNull(getActivity()), view);
        popupMenu.inflate(R.menu.card_history_menu);
        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.share:
                    goToShareHistory(query);
                    break;
                case R.id.remove:
                    goToRemoveHistory(query);
                    break;
                default:
                    return false;
            }
            return false;
        });
        popupMenu.show();
    }

    public void goToShareHistory(Query query) {
        this.profilePresenter.goToShare(query);
    }

    public void goToRemoveHistory(Query query) {
        this.profilePresenter.goToRemove(query);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.cameraPresenter.destroy();
        this.profilePresenter.destroy();
    }

    @Override
    public void showRecognitionsHistory(List<Query> queryList) {
        Log.d(TAG, "llamada servicio correcto: " + queryList.size());
        try {
            Animation fadeOutAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
            Animation fadeInAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
            visibleView(mProgressBar, false, fadeOutAnimation);
            if (queryList.isEmpty()) {
                swipeRefreshLayoutLibrary.setEnabled(false);
                visibleView(mCardViewSmartgrow, true, fadeInAnimation);
            }
            this.historyRecognitions = queryList;
            this.historyAdapter.setHistoryList(this.historyRecognitions);
            this.recyclerViewLibrary.scheduleLayoutAnimation();
        } catch (Exception e) {
            Log.d(TAG, "error: " + e.getMessage());
        }
    }

    @Override
    public void showRecognitionView(Query query) {
        CustomBottomSheetDialogFragment customBottomSheetDialogFragment = CustomBottomSheetDialogFragment.newInstance();
        customBottomSheetDialogFragment.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), customBottomSheetDialogFragment.getTag());
        customBottomSheetDialogFragment.setCameraPresenter(this.cameraPresenter);
        customBottomSheetDialogFragment.setResultAdapter(this.resultAdapter);
        customBottomSheetDialogFragment.setmGson(this.gson);
        customBottomSheetDialogFragment.setResultList(query.getResultList());
    }

    @Override
    public void showRecognitionImage(Query query) {
        Image image = new Image();
        image.setName(query.getDate());
        image.setLink(query.getUrlImageQuery());
        startActivity(ImageFullScreenScreenActivity.getCallIntent(getActivity(), gson.toJson(image), 0));
    }

    @Override
    public void shareRecognition(Query query) {
        Log.d(TAG, "Compartiendo query");
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, query.getResultList().get(0).getNamePlague());
        sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        sendIntent.setType("text/plain");
        try {
            startActivity(Intent.createChooser(sendIntent, "SmartGrow"));
        } catch (android.content.ActivityNotFoundException e) {
            (new AlertDialog.Builder(Objects.requireNonNull(getContext()))
                    .setMessage("Share failed")
                    .setPositiveButton("OK",
                            (dialog, whichButton) -> {
                            }).create()).show();
        }
    }

    @Override
    public void removeRecognition(Query query) {
        Log.d(TAG, "Removiendo query");
        this.historyRecognitions.remove(query);
        this.historyAdapter.setHistoryList(this.historyRecognitions);
        LayoutAnimationController layoutAnimationController = AnimationUtils.loadLayoutAnimation(getContext(), layout_animation_history_fadeout);
        this.recyclerViewLibrary.setLayoutAnimation(layoutAnimationController);
        this.recyclerViewLibrary.scheduleLayoutAnimation();
    }

    @Override
    public void showLoading() {
        Log.d(TAG, "showLoading");
        this.swipeRefreshLayoutLibrary.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        Log.d(TAG, "hideLoading");
        this.swipeRefreshLayoutLibrary.setRefreshing(false);
    }

    @Override
    public void showError(String messageError) {
        this.swipeRefreshLayoutLibrary.setRefreshing(false);
        Toast.makeText(getContext(), messageError, Toast.LENGTH_LONG).show();
        Log.d(TAG, "showError:" + messageError);
    }

    @Override
    public void showProfile(User userAccout) {
        Log.d(TAG, userAccout.getUsername() + " URL: " + userAccout.getUrlImage());
        this.user = userAccout;
        mTextViewNameUser.setText(this.user.getName());
        if (this.user.getDescription().equals("")) {
            mTextViewDescriptionUser.setVisibility(View.GONE);
        } else {
            mTextViewDescriptionUser.setText(this.user.getDescription());
        }
        RequestOptions requestOptionsBack = new RequestOptions();
        requestOptionsBack.error(R.drawable.ai);
        RequestOptions requestOptionsProfile = new RequestOptions();
        requestOptionsProfile.error(R.drawable.profile);
        Glide.with(this)
                .setDefaultRequestOptions(requestOptionsBack)
                .load(this.user.getUrlImage())
                .apply(bitmapTransform(new BlurTransformation(10, 2)))
                .into(mImageViewBackground);
        Glide.with(this)
                .setDefaultRequestOptions(requestOptionsProfile)
                .load(this.user.getUrlImage())
                .apply(fitCenterTransform())
                .into(mImageViewUserProfile);
    }

    @Override
    public void getProfile(User user) {
        Log.d(TAG, user.getUsername());
        this.profilePresenter.loadUserProfile(user.getUsername());
    }

    @Override
    public void showRefresh() {
        Log.d(TAG, "showRefresh");
        this.swipeRefreshLayoutLibrary.setRefreshing(true);
    }

    @Override
    public void hideRefresh() {
        Log.d(TAG, "hideRefresh");
        this.swipeRefreshLayoutLibrary.setRefreshing(false);
    }

    @Override
    public void showRefreshError(String messageError) {
        this.swipeRefreshLayoutLibrary.setRefreshing(false);
        Log.d(TAG, "showRefreshError:" + messageError);
    }
}
