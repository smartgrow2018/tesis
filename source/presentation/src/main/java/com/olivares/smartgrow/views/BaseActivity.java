package com.olivares.smartgrow.views;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState);
    }

    protected void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    protected void showSnackBarMessage(Activity activityParam, String message, int length) {
        final Activity activity = activityParam;
        if (activity != null) {
            activity.runOnUiThread(() -> {
                View view = activity.findViewById(android.R.id.content).getRootView();
                Snackbar.make(view, message, length).show();
            });
        }
    }

}
