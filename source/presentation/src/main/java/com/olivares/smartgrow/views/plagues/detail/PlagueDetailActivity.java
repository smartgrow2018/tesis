package com.olivares.smartgrow.views.plagues.detail;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.CoordinatorLayout;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alespero.expandablecardview.ExpandableCardView;
import com.google.gson.Gson;
import com.olivares.entities.Control;
import com.olivares.entities.Plague;
import com.olivares.entities.Result;
import com.olivares.smartgrow.R;
import com.olivares.smartgrow.di.components.ApplicationComponent;
import com.olivares.smartgrow.di.components.DaggerLibraryComponent;
import com.olivares.smartgrow.di.components.LibraryComponent;
import com.olivares.smartgrow.di.injector.DependencyInjector;
import com.olivares.smartgrow.views.BaseActivity;
import com.olivares.smartgrow.views.custom.ImageFullScreenScreenActivity;
import com.olivares.smartgrow.views.home.HomeActivity;
import com.olivares.smartgrow.views.plagues.PlaguesPresenter;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PlagueDetailActivity extends BaseActivity implements PlaguesPresenter.PlaguesDetailView {
    private static final String TAG = PlagueDetailActivity.class.getName();
    public static final String INTENT_EXTRA_ITEM = "INTENT_ITEM_MODEL";
    public static final String INTENT_EXTRA_ITEM_TYPE = "INTENT_ITEM_MODEL_TYPE";
    @BindView(R.id.toolbar_detail)
    Toolbar toolbarDetail;
    @BindView(R.id.txtResumenDetail)
    TextView txtResumenDetail;
    @BindView(R.id.txtControlDetail)
    TextView txtTextViewControlDetail;
    @BindView(R.id.txtSintomasDetail)
    TextView txtTextViewSintomasDetail;
    @BindView(R.id.detail_container)
    CoordinatorLayout detailContainer;
    @BindView(R.id.progressBar_detail_activity)
    ProgressBar progressBarDetail;
    @BindView(R.id.image_plague_detail)
    ImageView imageViewDetail;
    @BindView(R.id.control_expandable)
    ExpandableCardView mCardViewControl;

    private Plague plague;

    @Inject
    PlaguesPresenter plaguesPresenter;
    @Inject
    Gson gson;
    @Inject
    Picasso picasso;

    LibraryComponent libraryComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library_detail);
        ButterKnife.bind(this);
        setUpView();
        setUpDataDetail();
    }

    public static Intent getCallIntent(Context context, String item, int type) {
        Intent intent = new Intent(context, PlagueDetailActivity.class);
        intent.putExtra(INTENT_EXTRA_ITEM, item);
        intent.putExtra(INTENT_EXTRA_ITEM_TYPE, type);
        return intent;
    }

    private void setUpView() {
        this.initializeInjector(DependencyInjector.applicationComponent());
        this.libraryComponent.inject(this);
        this.plaguesPresenter.setPlaguesDetailView(this);
    }

    private void initializeInjector(ApplicationComponent applicationComponent) {
        this.libraryComponent = DaggerLibraryComponent.builder()
                .applicationComponent(applicationComponent)
                .build();
    }

    private void setUpDataDetail() {
        int type = getIntent().getIntExtra(INTENT_EXTRA_ITEM_TYPE, 0);
        if (type == 0) {
            this.plague = this.gson.fromJson(getIntent().getStringExtra(INTENT_EXTRA_ITEM), Plague.class);
            showData();
            showToolbar(this.plague.getName());
        } else {
            Result result = this.gson.fromJson(getIntent().getStringExtra(INTENT_EXTRA_ITEM), Result.class);
            this.plaguesPresenter.loadPlagueData(result.getPlagueId());
        }
    }

    private void showData() {
        if (this.plague != null) {
            this.showControlList();
            loadImagePicasso(this.imageViewDetail, this.plague.getUrlImage());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                if (this.plague.getDescription() != null) {
                    this.txtResumenDetail.setText(Html.fromHtml(this.plague.getDescription(), Html.FROM_HTML_MODE_COMPACT));
                }
            } else {
                if (this.plague.getDescription() != null) {
                    this.txtResumenDetail.setText(Html.fromHtml(this.plague.getDescription()));
                }
            }
        }
    }

    private void showControlList() {
        for (Control control : this.plague.getControlList()) {
            if (control.getType() == 1) {
                this.txtTextViewControlDetail.setText(control.getDescription());
            } else {
                this.txtTextViewSintomasDetail.setText(control.getDescription());
            }
        }
    }

    private void showToolbar(String title) {
        setSupportActionBar(toolbarDetail);
        Objects.requireNonNull(getSupportActionBar()).setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    private void loadImagePicasso(ImageView imageView, String url) {
        this.picasso.load(url)
                .placeholder(R.drawable.not_image)
                .into(imageView);
    }

    @Override
    public void showPlagueView(Plague plague) {
        this.plague = plague;
        showToolbar(this.plague.getName());
        loadImagePicasso(this.imageViewDetail, this.plague.getUrlImage());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if (this.plague.getDescription() != null) {
                this.txtResumenDetail.setText(Html.fromHtml(this.plague.getDescription(), Html.FROM_HTML_MODE_COMPACT));
            }
        } else {
            if (this.plague.getDescription() != null) {
                this.txtResumenDetail.setText(Html.fromHtml(this.plague.getDescription()));
            }
        }
    }

    @OnClick(R.id.image_plague_detail)
    public void onClickImage() {
        try {
            if (!this.plague.getUrlImage().isEmpty() && !this.plague.getUrlImage().equalsIgnoreCase("")) {
                this.plaguesPresenter.showImageFullScreen(this.plague);
                return;
            }
            this.showError("NO IMAGEN");
        } catch (Exception e) {
            this.showError("NO IMAGEN");
        }
    }

    @Override
    public void showImageFullScreen(Plague plague) {
        Log.d(TAG, plague.getUrlImage());
        startActivity(ImageFullScreenScreenActivity.getCallIntent(this, gson.toJson(plague), 1));
    }

    @Override
    public void showLoading() {
        Log.d(TAG, "show loading ...");
        this.progressBarDetail.setVisibility(View.VISIBLE);
        this.detailContainer.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideLoading() {
        Log.d(TAG, "hide loading ...");
        this.progressBarDetail.setVisibility(View.INVISIBLE);
        this.detailContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String messageError) {
        Log.d(TAG, "error... " + messageError);
        this.progressBarDetail.setVisibility(View.INVISIBLE);
        this.showToastMessage(messageError);
        startActivity(HomeActivity.getCallIntent(this));
        finish();
    }
}
