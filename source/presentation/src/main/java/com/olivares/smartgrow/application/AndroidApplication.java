package com.olivares.smartgrow.application;

import android.app.Application;

import com.olivares.smartgrow.di.injector.DependencyInjector;

public class AndroidApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        this.initializeInjector();
    }

    private void initializeInjector() {
        DependencyInjector.initialize(this);
    }

}
