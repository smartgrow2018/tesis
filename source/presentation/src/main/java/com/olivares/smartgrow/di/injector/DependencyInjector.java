package com.olivares.smartgrow.di.injector;


import com.olivares.smartgrow.application.AndroidApplication;
import com.olivares.smartgrow.di.components.ApplicationComponent;
import com.olivares.smartgrow.di.components.DaggerApplicationComponent;
import com.olivares.smartgrow.di.modules.ApplicationModule;

public class DependencyInjector {
    private static ApplicationComponent applicationComponent;

    public static void initialize(AndroidApplication androidApplication) {
        ApplicationModule applicationModule = new ApplicationModule(androidApplication);
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(applicationModule)
                .build();
    }

    public static ApplicationComponent applicationComponent() {
        return applicationComponent;
    }
}
