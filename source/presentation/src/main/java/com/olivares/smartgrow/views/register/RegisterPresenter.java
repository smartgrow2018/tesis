package com.olivares.smartgrow.views.register;

import android.annotation.SuppressLint;

import com.olivares.entities.User;
import com.olivares.smartgrow.di.scopes.PerActivity;
import com.olivares.smartgrow.views.LoadView;
import com.olivares.smartgrow.views.Presenter;
import com.olivares.usecase.usecase.user.CreateUserAccountUseCase;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@PerActivity
public class RegisterPresenter implements Presenter {
    private CreateUserAccountUseCase createUserAccountUseCase;
    private RegisterView registerView;

    @Inject
    public RegisterPresenter(CreateUserAccountUseCase createUserAccountUseCase) {
        this.createUserAccountUseCase = createUserAccountUseCase;
    }

    public void setRegisterView(RegisterView registerView) {
        this.registerView = registerView;
    }

    @Override
    public void destroy() {
        this.registerView = null;
    }

    @SuppressLint("CheckResult")
    public void createUserAccount(User user) {
        Single.create((SingleOnSubscribe<User>) emmiter -> emmiter.onSuccess(this.createUserAccountUseCase.persistUserAccount(user)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> {
                    if (this.registerView == null) return;
                    this.registerView.showLoading();
                })
                .doFinally(() -> {
                    if (this.registerView == null) return;
                    this.registerView.hideLoading();
                })
                .subscribe(
                        userAccount -> {
                            if (this.registerView == null) return;
                            this.registerView.goToLogin();
                        },
                        error -> {
                            if (this.registerView == null) return;
                            this.registerView.showError(error.getMessage());
                        }
                );
    }

    public interface RegisterView extends LoadView {

        void goToLogin();
    }
}
