package com.olivares.smartgrow.views.camera;

import android.annotation.SuppressLint;
import android.util.Log;

import com.olivares.entities.Image;
import com.olivares.entities.Query;
import com.olivares.entities.Result;
import com.olivares.entities.User;
import com.olivares.smartgrow.di.scopes.PerActivity;
import com.olivares.smartgrow.views.LoadView;
import com.olivares.smartgrow.views.Presenter;
import com.olivares.usecase.usecase.photography.RegisterPhotographyUseCase;
import com.olivares.usecase.usecase.recognize.RecognizePlaguesInImageUseCase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@PerActivity
public class CameraPresenter implements Presenter {
    private static final String TAG = CameraPresenter.class.getName();
    private final RegisterPhotographyUseCase registerPhotographyUseCase;
    private final RecognizePlaguesInImageUseCase recognizePlaguesInImageUseCase;
    private CameraView cameraView;
    private ResultView resultView;

    @Inject
    public CameraPresenter(RegisterPhotographyUseCase registerPhotographyUseCase, RecognizePlaguesInImageUseCase recognizePlaguesInImageUseCase) {
        this.registerPhotographyUseCase = registerPhotographyUseCase;
        this.recognizePlaguesInImageUseCase = recognizePlaguesInImageUseCase;
    }

    public void setCameraView(CameraView cameraView) {
        this.cameraView = cameraView;
    }

    public void setResultView(ResultView resultView) {
        this.resultView = resultView;
    }

    @Override
    public void destroy() {
        Log.d("ProcessRecogntion", "destroy CameraPresenter ---------------------->");
        this.cameraView = null;
    }

    @SuppressLint("CheckResult")
    public void uploadImage(Image image) {
        try {
            Single.create((SingleOnSubscribe<Image>) emmiter -> emmiter.onSuccess(this.registerPhotographyUseCase.uploadPhotography(image)))
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(__ -> {
                        if (this.cameraView == null) return;
                        this.cameraView.showLoading();
                    })
                    .doFinally(() -> {
                        if (this.cameraView == null) return;
                        this.cameraView.hideLoading();
                    })
                    .subscribe(
                            success -> {
                                if (this.cameraView == null) return;
                                this.cameraView.uploadSuccess(success);
                            },
                            error -> {
                                if (this.cameraView == null) return;
                                this.cameraView.showError(error.getMessage());
                            }
                    );
        } catch (Exception e) {
            if (this.cameraView == null) return;
            this.cameraView.showError(e.getMessage());
        }
    }

    @SuppressLint("CheckResult")
    public void recognitionImage(User user, Image image) {
        try {
            Single.create((SingleOnSubscribe<Query>) emmiter -> emmiter.onSuccess(this.recognizePlaguesInImageUseCase.plaguesRecognitionInImage(user, image)))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(__ -> {
                        if (this.cameraView == null) return;
                        this.cameraView.showLoading();
                    })
                    .doFinally(() -> {
                        if (this.cameraView == null) return;
                        this.cameraView.hideLoading();
                    })
                    .subscribe(
                            success -> {
                                if (this.cameraView == null) return;
                                this.cameraView.reconizeSuccess(success);
                            },
                            error -> {
                                if (this.cameraView == null) return;
                                this.cameraView.showError(error.getMessage());
                            }
                    );
        } catch (Exception e) {
            if (this.cameraView == null) return;
            this.cameraView.showError(e.getMessage());
        }
    }

    public void loadResultList(List<Result> results) {
        Log.d(TAG, results.size() + "");
        List<Result> lista = new ArrayList<>();
        for (Result result : results) {
            Log.d(TAG, result.getPercentResult() + " - " + result.getNamePlague());
            lista.add(result);
        }
        this.resultView.showResultList(lista);
    }

    public void goToResultDetail(Result result) {
        this.resultView.showResultView(result);
    }

    public interface CameraView extends LoadView {

        void uploadSuccess(Image image);

        void reconizeSuccess(Query query);
    }

    public interface ResultView extends LoadView {
        void showResultList(List<Result> resultList);

        void showResultView(Result result);
    }
}
