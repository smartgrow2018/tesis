package com.olivares.remote.entity;

import com.google.gson.annotations.SerializedName;

public class StatusEntity {
    @SerializedName("code")
    private int code;
    @SerializedName("message")
    private String message;

    public StatusEntity() {
        // no require impl
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
