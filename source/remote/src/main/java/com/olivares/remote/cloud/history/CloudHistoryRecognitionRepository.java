package com.olivares.remote.cloud.history;

import android.util.Log;

import com.olivares.entities.Query;
import com.olivares.remote.entity.request.BaseEntityRequest;
import com.olivares.remote.entity.response.HistoryEntityResponseResponse;
import com.olivares.remote.network.RestApi;
import com.olivares.remote.util.RemoteExcepcion;
import com.olivares.usecase.repository.history.HistoryRecognitionRepositoryRemote;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import retrofit2.Call;

public class CloudHistoryRecognitionRepository implements HistoryRecognitionRepositoryRemote {
    private static final String TAG = CloudHistoryRecognitionRepository.class.getName();
    private final RestApi restApi;

    @Inject
    public CloudHistoryRecognitionRepository(RestApi restApi) {
        this.restApi = restApi;
    }

    @Override
    public List<Query> getHistoryRecognition(String user) throws Exception {
        try {
            Call<HistoryEntityResponseResponse> queryEntityCall = restApi.getHistory(user, new BaseEntityRequest(user));
            HistoryEntityResponseResponse historyEntityResponse = queryEntityCall.execute().body();
            Log.d(TAG, Objects.requireNonNull(historyEntityResponse).getStatusEntity().getMessage());
            List<Query> queryList = null;
            if (historyEntityResponse.getStatusEntity().getCode() == 1) {
                queryList = historyEntityResponse.toQueryList();
            } else {
                throw new RemoteExcepcion("error al traer los datos.");
            }
            return queryList;
        } catch (Exception e) {
            throw new RemoteExcepcion(e.getMessage());
        }
    }

    @Override
    public Query hideHistoryRecognition(Query query) {
        return query;
    }
}
