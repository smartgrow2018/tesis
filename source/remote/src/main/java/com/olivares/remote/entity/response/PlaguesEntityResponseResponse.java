package com.olivares.remote.entity.response;

import com.google.gson.annotations.SerializedName;
import com.olivares.entities.Control;
import com.olivares.entities.Plague;
import com.olivares.remote.entity.ControlEntity;
import com.olivares.remote.entity.PlagueEntity;

import java.util.ArrayList;
import java.util.List;

public class PlaguesEntityResponseResponse extends BaseEntityResponse {
    @SerializedName("data")
    List<PlagueEntity> plagueEntityList;

    public PlaguesEntityResponseResponse() {
        // no require impl
    }

    public PlaguesEntityResponseResponse(List<PlagueEntity> plagueEntityList) {
        this.plagueEntityList = plagueEntityList;
    }

    public List<PlagueEntity> getPlagueEntityList() {
        return plagueEntityList;
    }

    public void setPlagueEntityList(List<PlagueEntity> plagueEntityList) {
        this.plagueEntityList = plagueEntityList;
    }

    public List<Plague> toPlagueList() {
        final List<Plague> plagueList = new ArrayList<>();
        if (this.plagueEntityList != null) {
            for (PlagueEntity plagueEntity : plagueEntityList) {
                final Plague plague = this.toPlague(plagueEntity);
                plagueList.add(plague);
            }
        }
        return plagueList;
    }

    public Plague toPlague(PlagueEntity plagueEntity) {
        Plague plague = null;
        if (plagueEntity != null) {
            plague = new Plague();
            plague.setPlagueId(plagueEntity.getPlagueId());
            plague.setName(plagueEntity.getName());
            plague.setNameClarifai(plagueEntity.getNameClarifai());
            plague.setDescription(plagueEntity.getDescription());
            plague.setUrlImage(plagueEntity.getUrlImage());
            for (ControlEntity controlEntity : plagueEntity.getControls()) {
                plague.addControl(new Control(controlEntity.getDescription(), controlEntity.getType()));
            }
        }
        return plague;
    }
}
