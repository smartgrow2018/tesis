package com.olivares.remote.entity.request;

public class DetailEntityRequest extends BaseEntityRequest {
    private int plagueId;

    public DetailEntityRequest(String user, int plagueId) {
        super(user);
        this.plagueId = plagueId;
    }

    public int getConcept() {
        return plagueId;
    }

    public void setConcept(int plagueId) {
        this.plagueId = plagueId;
    }
}
