package com.olivares.remote.util;

public class RemoteExcepcion extends Exception {
    public RemoteExcepcion(String message) {
        super(message);
    }
}
