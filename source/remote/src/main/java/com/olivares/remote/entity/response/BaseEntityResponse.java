package com.olivares.remote.entity.response;

import com.google.gson.annotations.SerializedName;
import com.olivares.remote.entity.StatusEntity;

public class BaseEntityResponse {
    @SerializedName("result")
    private StatusEntity statusEntity;

    public BaseEntityResponse() {
        // no require impl
    }

    public StatusEntity getStatusEntity() {
        return statusEntity;
    }

    public void setStatusEntity(StatusEntity statusEntity) {
        this.statusEntity = statusEntity;
    }
}
