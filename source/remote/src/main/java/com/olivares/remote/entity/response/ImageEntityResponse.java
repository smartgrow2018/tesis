package com.olivares.remote.entity.response;

import com.google.gson.annotations.SerializedName;

public class ImageEntityResponse {
    @SerializedName("success")
    public boolean success;
    @SerializedName("status")
    public int status;
    @SerializedName("data")
    public UploadedImage data;

    public ImageEntityResponse(boolean success, int status, UploadedImage data) {
        this.success = success;
        this.status = status;
        this.data = data;
    }

    public ImageEntityResponse() {
        // no require impl
    }

    public ImageEntityResponse(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public UploadedImage getData() {
        return data;
    }

    public void setData(UploadedImage data) {
        this.data = data;
    }

    public class UploadedImage {
        @SerializedName("id")
        public String id;
        @SerializedName("datetime")
        public int datetime;
        @SerializedName("type")
        public String type;
        @SerializedName("link")
        public String link;

        public UploadedImage(String id, int datetime, String type, String link) {
            this.id = id;
            this.datetime = datetime;
            this.type = type;
            this.link = link;
        }

        public UploadedImage() {
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getDatetime() {
            return datetime;
        }

        public void setDatetime(int datetime) {
            this.datetime = datetime;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }
    }

}
