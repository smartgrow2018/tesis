package com.olivares.remote.cloud.photography;

import android.util.Log;

import com.olivares.entities.Image;
import com.olivares.remote.entity.response.ImageEntityResponse;
import com.olivares.remote.entity.request.ImageEntityRequest;
import com.olivares.remote.network.RestApi2;
import com.olivares.remote.util.RemoteExcepcion;
import com.olivares.usecase.repository.photography.PhotographyRepositoryRemote;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;

@Singleton
public class CloudPhotographyRepository implements PhotographyRepositoryRemote {
    private static final String TAG = CloudPhotographyRepository.class.getName();
    private final RestApi2 restApi2;

    @Inject
    public CloudPhotographyRepository(RestApi2 restApi2) {
        this.restApi2 = restApi2;
    }

    @Override
    public Image uploadPhotography(Image image) throws Exception {
        try {
            Log.d(TAG, "--------------> " + image.getImageData());
            Call<ImageEntityResponse> imageEntityCall = restApi2.
                    uploadImage("Bearer 257c4c700eb2e7c9e85f6f268267e80b93c96928",
                            new ImageEntityRequest(image.getImageData(), image.getName()));

            ImageEntityResponse imageEntityResponse = imageEntityCall.execute().body();

            if (imageEntityResponse != null && imageEntityResponse.getStatus() == 200) {
                Log.d(TAG, imageEntityResponse.getStatus() + " link image: " +
                        imageEntityResponse.getData().getLink());
                return new Image(imageEntityResponse.getData().getLink());
            } else {
                throw new RemoteExcepcion("error subir imagen");
            }
        } catch (Exception e) {
            throw new RemoteExcepcion(e.getMessage());
        }
    }
}
