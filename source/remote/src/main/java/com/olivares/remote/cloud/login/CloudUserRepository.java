package com.olivares.remote.cloud.login;

import android.util.Log;

import com.olivares.entities.User;
import com.olivares.remote.entity.request.CreateAccountEntityRequest;
import com.olivares.remote.entity.response.UserEntityResponse;
import com.olivares.remote.entity.request.LoginUserEntityRequest;
import com.olivares.remote.network.RestApi;
import com.olivares.remote.util.RemoteExcepcion;
import com.olivares.usecase.repository.user.UserRepositoryRemote;

import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;

@Singleton
public class CloudUserRepository implements UserRepositoryRemote {
    private static final String TAG = CloudUserRepository.class.getName();
    private final RestApi restApi;

    @Inject
    public CloudUserRepository(RestApi restApi) {
        this.restApi = restApi;
    }

    @Override
    public User loginUser(User user) throws Exception {
        try {
            Call<UserEntityResponse> userEntityCall = restApi.loginUser(new LoginUserEntityRequest(user));
            UserEntityResponse userEntity = userEntityCall.execute().body();
            Log.d(TAG, Objects.requireNonNull(userEntity).getStatusEntity().toString());
            if (userEntity.getStatusEntity().getCode() == 1) {
                return userEntity.toUser();
            } else {
                throw new RemoteExcepcion("error al traer los datos.");
            }
        } catch (Exception ex) {
            throw new RemoteExcepcion(ex.getMessage());
        }
    }

    @Override
    public User persistProfileUser(User user) {
        return user;
    }

    @Override
    public User persistUserAccount(User user) throws Exception {
        try {
            Call<UserEntityResponse> userEntityCall = restApi.createAccountUser(new CreateAccountEntityRequest(user));
            UserEntityResponse userEntity = userEntityCall.execute().body();
            Log.d(TAG, Objects.requireNonNull(userEntity).getStatusEntity().getMessage());
            if (userEntity.getStatusEntity().getCode() != 1) {
                String message;
                switch (userEntity.getStatusEntity().getMessage()) {
                    case "USER_EXIST":
                        message = "El usuario " + user.getUsername() + " ya existe. Intente ingresar otro usuario.";
                        break;
                    default:
                        message = "Error";
                        break;
                }
                throw new RemoteExcepcion(message);
            }
            return user;
        } catch (Exception ex) {
            throw new RemoteExcepcion(ex.getMessage());
        }
    }

    @Override
    public User getUserData(String username) {
        Log.d(TAG, username);
        User user = new User();
        user.setName("Mayer Olivares Salinas");
        user.setDescription("Soy ingeniero de sistemas con 2 años de experiencia en desarrollo de aplicaciones móviles.");
        user.setUrlImage("https://chiefexecutive.net/wp-content/uploads/2018/02/GettyImages-870184586-compressor.jpg");
        return user;
    }
}
