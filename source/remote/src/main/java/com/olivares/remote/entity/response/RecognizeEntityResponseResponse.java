package com.olivares.remote.entity.response;

import com.google.gson.annotations.SerializedName;
import com.olivares.entities.Query;
import com.olivares.entities.Result;
import com.olivares.remote.entity.QueryEntity;
import com.olivares.remote.entity.ResultEntity;

public class RecognizeEntityResponseResponse extends BaseEntityResponse {
    @SerializedName("data")
    private QueryEntity queryEntity;

    public RecognizeEntityResponseResponse() {
        // no require impl
    }

    public RecognizeEntityResponseResponse(QueryEntity queryEntity) {
        this.queryEntity = queryEntity;
    }

    public QueryEntity getQueryEntity() {
        return queryEntity;
    }

    public void setQueryEntity(QueryEntity queryEntity) {
        this.queryEntity = queryEntity;
    }

    public Query toQuery() {
        Query query = null;
        if (this.queryEntity != null) {
            query = new Query();
            query.setQueryId(this.queryEntity.getQueryId());
            query.setUrlImageQuery(this.queryEntity.getUrlImage());
            query.setDate(this.queryEntity.getDate());
            for (ResultEntity resultEntity : queryEntity.getResultEntityList()) {
                query.addResult(new Result(resultEntity.getPlagueId(),
                        resultEntity.getName(), resultEntity.getDescriptionPlague(),
                        resultEntity.getValue(),
                        resultEntity.getNameClarifai(), resultEntity.getUrlImagePlague()));
            }
        }
        return query;
    }
}
