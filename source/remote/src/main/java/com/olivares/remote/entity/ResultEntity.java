package com.olivares.remote.entity;

import com.google.gson.annotations.SerializedName;

public class ResultEntity {
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String descriptionPlague;
    @SerializedName("name_clarifai")
    private String nameClarifai;
    @SerializedName("value")
    private double value;
    @SerializedName("url_image_plague")
    private String urlImagePlague;
    @SerializedName("plague_id")
    private int plagueId;

    public ResultEntity(String name, double value) {
        this.name = name;
        this.value = value;
    }

    public ResultEntity(String name, String nameClarifai, double value, String urlImagePlague, int plagueId) {
        this.name = name;
        this.nameClarifai = nameClarifai;
        this.value = value;
        this.urlImagePlague = urlImagePlague;
        this.plagueId = plagueId;
    }

    public ResultEntity() {
    }

    public String getNameClarifai() {
        return nameClarifai;
    }

    public void setNameClarifai(String nameClarifai) {
        this.nameClarifai = nameClarifai;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriptionPlague() {

        return descriptionPlague;
    }

    public void setDescriptionPlague(String descriptionPlague) {
        this.descriptionPlague = descriptionPlague;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getUrlImagePlague() {
        return urlImagePlague;
    }

    public void setUrlImagePlague(String urlImagePlague) {
        this.urlImagePlague = urlImagePlague;
    }

    public int getPlagueId() {
        return plagueId;
    }

    public void setPlagueId(int plagueId) {
        this.plagueId = plagueId;
    }
}
