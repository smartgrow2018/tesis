package com.olivares.remote.entity.request;

public class RecognizeEntityRequest extends BaseEntityRequest {
    private String urlImage;

    public RecognizeEntityRequest(String user, String urlImage) {
        super(user);
        this.urlImage = urlImage;
    }

    public RecognizeEntityRequest(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }
}
