package com.olivares.remote.cloud.recognize;

import android.util.Log;

import com.olivares.entities.Image;
import com.olivares.entities.Query;
import com.olivares.entities.User;
import com.olivares.remote.entity.response.RecognizeEntityResponseResponse;
import com.olivares.remote.entity.request.RecognizeEntityRequest;
import com.olivares.remote.network.RestApi;
import com.olivares.remote.util.RemoteExcepcion;
import com.olivares.usecase.repository.recognize.PlaguesRecognitionRepositoryRemote;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;

@Singleton
public class CloudPlaguesRecognitionRepository implements PlaguesRecognitionRepositoryRemote {
    private static final String TAG = CloudPlaguesRecognitionRepository.class.getName();
    private final RestApi restApi;

    @Inject
    public CloudPlaguesRecognitionRepository(RestApi restApi) {
        this.restApi = restApi;
    }

    @Override
    public Query plaguesRecognitionInImage(User user, Image image) throws Exception {
        try {
            Call<RecognizeEntityResponseResponse> queryEntityCall =
                    restApi.recognitionImage(new RecognizeEntityRequest(user.getUsername(), image.getLink()));
            RecognizeEntityResponseResponse recognizeEntityResponse = queryEntityCall.execute().body();
            if (recognizeEntityResponse != null && recognizeEntityResponse.getStatusEntity().getCode() == 1) {
                Log.d(TAG, recognizeEntityResponse.getStatusEntity().getMessage());
                return recognizeEntityResponse.toQuery();
            } else if (recognizeEntityResponse != null && recognizeEntityResponse.getStatusEntity() != null &&
                    recognizeEntityResponse.getStatusEntity().getMessage().equalsIgnoreCase("NOT_FOUND_OBJECT")) {
                throw new RemoteExcepcion("NOT_FOUND_OBJECT");
            } else {
                throw new RemoteExcepcion("Problemas de conexión con el servidor.");
            }
        } catch (Exception e) {
            Log.d(TAG, "ERRO ------------------->");
            throw new RemoteExcepcion(e.getMessage());
        }
    }
}
