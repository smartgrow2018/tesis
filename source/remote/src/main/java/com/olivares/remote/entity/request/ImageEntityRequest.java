package com.olivares.remote.entity.request;

import com.google.gson.annotations.SerializedName;

public class ImageEntityRequest {
    @SerializedName("image")
    public String image;
    @SerializedName("name")
    public String name;
    @SerializedName("album")
    public String album;

    public ImageEntityRequest(String image, String name) {
        this.image = image;
        this.name = name;
        this.album = "t4M3uvV";
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }
}
