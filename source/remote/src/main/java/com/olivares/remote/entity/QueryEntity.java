package com.olivares.remote.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QueryEntity {
    @SerializedName("query_id")
    private int queryId;
    @SerializedName("url_image")
    private String urlImage;
    @SerializedName("date")
    private String date;
    @SerializedName("recognitions")
    private List<ResultEntity> resultEntityList;

    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(int queryId) {
        this.queryId = queryId;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public List<ResultEntity> getResultEntityList() {
        return resultEntityList;
    }

    public void setResultEntityList(List<ResultEntity> resultEntityList) {
        this.resultEntityList = resultEntityList;
    }

    public String getDate() {
        return (date == null) ? "" : date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
