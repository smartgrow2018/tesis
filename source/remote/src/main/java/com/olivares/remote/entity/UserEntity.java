package com.olivares.remote.entity;

import com.google.gson.annotations.SerializedName;

public class UserEntity {
    @SerializedName("code_user")
    private int id;
    @SerializedName("username_user")
    private String username;
    @SerializedName("name_user")
    private String name;
    @SerializedName("email_user")
    private String email;

    public UserEntity() {
        // no require impl
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
