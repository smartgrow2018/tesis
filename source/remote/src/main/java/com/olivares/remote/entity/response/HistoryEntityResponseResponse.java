package com.olivares.remote.entity.response;

import com.google.gson.annotations.SerializedName;
import com.olivares.entities.Query;
import com.olivares.entities.Result;
import com.olivares.remote.entity.QueryEntity;
import com.olivares.remote.entity.ResultEntity;

import java.util.ArrayList;
import java.util.List;

public class HistoryEntityResponseResponse extends BaseEntityResponse {
    @SerializedName("data")
    List<QueryEntity> queryEntityList;

    public HistoryEntityResponseResponse() {
        // no require impl
    }

    public List<QueryEntity> getQueryEntityList() {
        return queryEntityList;
    }

    public void setQueryEntityList(List<QueryEntity> queryEntityList) {
        this.queryEntityList = queryEntityList;
    }

    public List<Query> toQueryList() {
        final List<Query> queryList = new ArrayList<>();
        if (this.queryEntityList != null) {
            for (QueryEntity queryEntity : queryEntityList) {
                final Query query = this.toQuery(queryEntity);
                queryList.add(query);
            }
        }
        return queryList;
    }

    public Query toQuery(QueryEntity queryEntity) {
        Query query = null;
        if (queryEntity != null) {
            query = new Query();
            query.setUrlImageQuery(queryEntity.getUrlImage());
            query.setQueryId(queryEntity.getQueryId());
            query.setDate(queryEntity.getDate());
            for (ResultEntity resultEntity : queryEntity.getResultEntityList()) {
                query.addResult(
                        new Result(resultEntity.getPlagueId(), resultEntity.getName(),
                                resultEntity.getDescriptionPlague(),
                                resultEntity.getValue(),
                                resultEntity.getNameClarifai(),
                                resultEntity.getUrlImagePlague()));
            }
        }
        return query;
    }
}
