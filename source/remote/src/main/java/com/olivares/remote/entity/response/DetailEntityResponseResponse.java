package com.olivares.remote.entity.response;

import com.google.gson.annotations.SerializedName;
import com.olivares.entities.Control;
import com.olivares.entities.Plague;
import com.olivares.remote.entity.ControlEntity;
import com.olivares.remote.entity.PlagueEntity;

public class DetailEntityResponseResponse extends BaseEntityResponse {
    @SerializedName("data")
    PlagueEntity plagueEntity;

    public DetailEntityResponseResponse(PlagueEntity plagueEntity) {
        this.plagueEntity = plagueEntity;
    }

    public PlagueEntity getPlagueEntity() {
        return plagueEntity;
    }

    public void setPlagueEntity(PlagueEntity plagueEntity) {
        this.plagueEntity = plagueEntity;
    }

    public Plague toPlague() {
        Plague plague = null;
        if (plagueEntity != null) {
            plague = new Plague();
            plague.setName(plagueEntity.getName());
            plague.setNameClarifai(plagueEntity.getNameClarifai());
            plague.setDescription(plagueEntity.getDescription());
            plague.setUrlImage(plagueEntity.getUrlImage());
            for (ControlEntity controlEntity : plagueEntity.getControls()) {
                plague.addControl(new Control(controlEntity.getDescription(), controlEntity.getType()));
            }
        }
        return plague;
    }

}
