package com.olivares.remote.entity.response;

import com.google.gson.annotations.SerializedName;
import com.olivares.entities.User;
import com.olivares.remote.entity.UserEntity;

public class UserEntityResponse extends BaseEntityResponse {
    @SerializedName("data")
    private UserEntity userEntity;

    public UserEntityResponse() {
        // no require impl
    }

    public User toUser() {
        User user = new User();
        user.setId(userEntity.getId());
        user.setName(userEntity.getName());
        user.setDescription("");
        user.setUrlImage("");
        user.setEmail(userEntity.getEmail());
        user.setUsername(userEntity.getUsername());
        return user;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }
}