package com.olivares.remote.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlagueEntity {
    @SerializedName("plague_id")
    private int plagueId;
    @SerializedName("name_plague")
    private String name;
    @SerializedName("name_clarifai")
    private String nameClarifai;
    @SerializedName("description_plague")
    private String description;
    @SerializedName("url_image_plague")
    private String urlImage;
    @SerializedName("control")
    private List<ControlEntity> controls;

    public PlagueEntity(String name, String description, List<ControlEntity> controls) {
        this.name = name;
        this.description = description;
        this.controls = controls;
    }

    public PlagueEntity() {
    }

    public int getPlagueId() {
        return plagueId;
    }

    public void setPlagueId(int plagueId) {
        this.plagueId = plagueId;
    }

    public String getNameClarifai() {
        return nameClarifai;
    }

    public void setNameClarifai(String nameClarifai) {
        this.nameClarifai = nameClarifai;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ControlEntity> getControls() {
        return controls;
    }

    public void setControls(List<ControlEntity> controls) {
        this.controls = controls;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }
}
