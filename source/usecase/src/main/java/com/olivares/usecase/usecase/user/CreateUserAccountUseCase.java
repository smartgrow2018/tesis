package com.olivares.usecase.usecase.user;

import com.olivares.entities.User;
import com.olivares.usecase.repository.user.UserRepositoryRemote;

import javax.inject.Inject;

public class CreateUserAccountUseCase {
    private final UserRepositoryRemote userRepositoryRemote;


    @Inject
    public CreateUserAccountUseCase(UserRepositoryRemote userRepositoryRemote) {
        this.userRepositoryRemote = userRepositoryRemote;
    }

    public User persistUserAccount(User user) throws Exception {
        return this.userRepositoryRemote.persistUserAccount(user);
    }
}
