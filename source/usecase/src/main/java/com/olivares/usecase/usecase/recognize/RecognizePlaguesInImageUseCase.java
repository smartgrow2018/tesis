package com.olivares.usecase.usecase.recognize;

import android.util.Log;

import com.olivares.entities.Image;
import com.olivares.entities.Query;
import com.olivares.entities.User;
import com.olivares.usecase.repository.recognize.PlaguesRecognitionRepositoryRemote;
import com.olivares.usecase.usecase.history.SaveHistoryRecognitionUseCase;
import com.olivares.usecase.usecase.user.GetUserAccountDataUseCase;
import com.olivares.usecase.util.UseCaseExcepcion;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class RecognizePlaguesInImageUseCase {
    private static final String TAG = RecognizePlaguesInImageUseCase.class.getName();
    private final PlaguesRecognitionRepositoryRemote plaguesRecognitionRepositoryRemote;
    private final GetUserAccountDataUseCase getUserAccountDataUseCase;
    private final SaveHistoryRecognitionUseCase saveHistoryRecognitionUseCase;

    @Inject
    public RecognizePlaguesInImageUseCase(
            PlaguesRecognitionRepositoryRemote plaguesRecognitionRepositoryRemote,
            GetUserAccountDataUseCase getUserAccountDataUseCase,
            SaveHistoryRecognitionUseCase saveHistoryRecognitionUseCase) {
        this.plaguesRecognitionRepositoryRemote = plaguesRecognitionRepositoryRemote;
        this.getUserAccountDataUseCase = getUserAccountDataUseCase;
        this.saveHistoryRecognitionUseCase = saveHistoryRecognitionUseCase;
    }

    public Query plaguesRecognitionInImage(User user, Image image) throws Exception {
        try {
            User userLogged = getUserAccountDataUseCase.getUserLogged();
            Log.d(TAG, "User query: " + user.getUsername());
            Query query = this.plaguesRecognitionRepositoryRemote.
                    plaguesRecognitionInImage(userLogged, image);
            List<Query> queries = new ArrayList<>();
            queries.add(query);
            this.saveHistoryRecognitionUseCase.saveRecognitionLocal(queries);
            Log.d(TAG, "recognition save: " + query.getQueryId());
            return query;
        } catch (Exception e) {
            throw new UseCaseExcepcion(e.getMessage());
        }
    }
}