package com.olivares.usecase.repository.user;

import com.olivares.entities.User;

public interface UserRepositoryRemote {

    User loginUser(User user) throws Exception;

    User persistProfileUser(User user) throws Exception;

    User persistUserAccount(User user) throws Exception;

    User getUserData(String username) throws Exception;
}
