package com.olivares.usecase.usecase.plagues;

import com.olivares.usecase.repository.plagues.PlaguesRepositoryLocal;

import javax.inject.Inject;

public class RemovePlagueUseCase {
    private final PlaguesRepositoryLocal plaguesRepositoryLocal;

    @Inject
    public RemovePlagueUseCase(PlaguesRepositoryLocal plaguesRepositoryLocal) {
        this.plaguesRepositoryLocal = plaguesRepositoryLocal;
    }

    public Boolean deleteAllPagues() throws Exception {
        return this.plaguesRepositoryLocal.deleteAllPlaguesLocal();
    }
}
