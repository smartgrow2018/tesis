package com.olivares.usecase.util;

public class UseCaseExcepcion extends Exception {
    public UseCaseExcepcion(String message) {
        super(message);
    }
}
