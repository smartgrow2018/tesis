package com.olivares.usecase.usecase.user;

import android.util.Log;

import com.olivares.usecase.repository.user.UserRepositoryPref;
import com.olivares.usecase.util.UseCaseExcepcion;

import javax.inject.Inject;

public class SeeUserTutorialUseCase {
    private static final String TAG = SeeUserTutorialUseCase.class.getName();
    private final UserRepositoryPref userRepositoryPref;

    @Inject
    public SeeUserTutorialUseCase(UserRepositoryPref userRepositoryPref) {
        this.userRepositoryPref = userRepositoryPref;
    }

    public Boolean seePlagueTutorial() throws Exception {
        Log.d(TAG, "seeTutorial");
        try {
            Boolean canSee = this.userRepositoryPref.seePlagueTutorial();
            if (canSee) {
                return this.userRepositoryPref.saveSeePlagueTutorial();
            }
            return canSee;
        } catch (Exception e) {
            throw new UseCaseExcepcion(e.getMessage());
        }
    }

    public Boolean seeHistoryTutorial() throws Exception {
        Log.d(TAG, "seeHistoryTutorial");
        try {
            Boolean canSee = this.userRepositoryPref.seeHistoryTutorial();
            if (canSee) {
                return this.userRepositoryPref.saveSeeHistoryTutorial();
            }
            return canSee;
        } catch (Exception e) {
            throw new UseCaseExcepcion(e.getMessage());
        }
    }

    public Boolean deleteAllUserPref() throws Exception {
        return this.userRepositoryPref.deleteAllUserPref();
    }

}
