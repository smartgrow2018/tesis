package com.olivares.usecase.usecase.history;

import com.olivares.entities.Query;
import com.olivares.usecase.repository.history.HistoryRecognitionRepositoryLocal;
import com.olivares.usecase.repository.history.HistoryRecognitionRepositoryRemote;

import java.util.List;

import javax.inject.Inject;

public class HideHistoryRecognitionUseCase {
    private final HistoryRecognitionRepositoryRemote historyRecognitionRepositoryRemote;
    private final HistoryRecognitionRepositoryLocal historyRecognitionRepositoryLocal;

    @Inject
    public HideHistoryRecognitionUseCase(HistoryRecognitionRepositoryRemote historyRecognitionRepositoryRemote,
                                         HistoryRecognitionRepositoryLocal historyRecognitionRepositoryLocal) {
        this.historyRecognitionRepositoryRemote = historyRecognitionRepositoryRemote;
        this.historyRecognitionRepositoryLocal = historyRecognitionRepositoryLocal;
    }

    public Query hideHistoryRecognition(Query query) throws Exception {
        return this.historyRecognitionRepositoryRemote.hideHistoryRecognition(query);
    }

    public Query hideHistoryRecognitionLocal(Query query) throws Exception {
        return historyRecognitionRepositoryLocal.hideQueryLocal(query);
    }

    public Boolean deleteAllHistoryRecognitions() throws Exception {
        return historyRecognitionRepositoryLocal.deleteAllHistoryRecognitions();
    }
}
