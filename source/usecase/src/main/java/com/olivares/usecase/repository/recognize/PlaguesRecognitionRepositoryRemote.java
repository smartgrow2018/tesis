package com.olivares.usecase.repository.recognize;

import com.olivares.entities.Image;
import com.olivares.entities.Query;
import com.olivares.entities.User;

public interface PlaguesRecognitionRepositoryRemote {

    Query plaguesRecognitionInImage(User user, Image image) throws Exception;

}
