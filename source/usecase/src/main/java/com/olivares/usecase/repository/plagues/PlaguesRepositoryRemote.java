package com.olivares.usecase.repository.plagues;

import com.olivares.entities.Plague;

import java.util.List;

public interface PlaguesRepositoryRemote {

    Plague getPlagueData(int plagueId) throws Exception;

    List<Plague> getPlagues() throws Exception;
}
