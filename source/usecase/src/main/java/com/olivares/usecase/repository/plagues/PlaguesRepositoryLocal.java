package com.olivares.usecase.repository.plagues;

import com.olivares.entities.Plague;

import java.util.List;

public interface PlaguesRepositoryLocal {

    Boolean savePlaguesLocal(List<Plague> plagues) throws Exception;

    List<Plague> getPlaguesLocal() throws Exception;

    Boolean deleteAllPlaguesLocal() throws Exception;

    Plague getPlague(int plagueId) throws Exception;
}
