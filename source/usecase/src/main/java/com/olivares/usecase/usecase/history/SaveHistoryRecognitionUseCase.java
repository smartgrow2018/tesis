package com.olivares.usecase.usecase.history;

import com.olivares.entities.Query;
import com.olivares.usecase.repository.history.HistoryRecognitionRepositoryLocal;

import java.util.List;
import java.util.Queue;

import javax.inject.Inject;

public class SaveHistoryRecognitionUseCase {
    private final HistoryRecognitionRepositoryLocal historyRecognitionRepositoryLocal;

    @Inject
    public SaveHistoryRecognitionUseCase(HistoryRecognitionRepositoryLocal historyRecognitionRepositoryLocal) {
        this.historyRecognitionRepositoryLocal = historyRecognitionRepositoryLocal;
    }

    public Boolean saveRecognitionLocal(List<Query> queries) throws Exception {
        return historyRecognitionRepositoryLocal.saveHistoryRecognitionLocal(queries);
    }

}
