package com.olivares.usecase.repository.history;

import com.olivares.entities.Query;

import java.util.List;

public interface HistoryRecognitionRepositoryRemote {

    List<Query> getHistoryRecognition(String user) throws Exception;

    Query hideHistoryRecognition(Query query) throws Exception;
}
