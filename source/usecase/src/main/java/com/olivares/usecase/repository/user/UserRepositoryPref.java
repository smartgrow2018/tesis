package com.olivares.usecase.repository.user;

public interface UserRepositoryPref {

    Boolean saveSeePlagueTutorial() throws Exception;

    Boolean seePlagueTutorial() throws Exception;

    Boolean saveSeeHistoryTutorial() throws Exception;

    Boolean seeHistoryTutorial() throws Exception;

    Boolean deleteAllUserPref() throws Exception;

}
