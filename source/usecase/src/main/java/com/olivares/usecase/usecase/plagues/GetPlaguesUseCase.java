package com.olivares.usecase.usecase.plagues;

import com.olivares.entities.Plague;
import com.olivares.usecase.repository.plagues.PlaguesRepositoryLocal;
import com.olivares.usecase.repository.plagues.PlaguesRepositoryRemote;

import java.util.List;

import javax.inject.Inject;

public class GetPlaguesUseCase {
    private final PlaguesRepositoryRemote plaguesRepositoryRemote;
    private final PlaguesRepositoryLocal plaguesRepositoryLocal;

    @Inject
    public GetPlaguesUseCase(PlaguesRepositoryRemote plaguesRepositoryRemote,
                             PlaguesRepositoryLocal plaguesRepositoryLocal) {
        this.plaguesRepositoryRemote = plaguesRepositoryRemote;
        this.plaguesRepositoryLocal = plaguesRepositoryLocal;
    }

    public List<Plague> getPlagues() throws Exception {
        List<Plague> plagues = this.plaguesRepositoryLocal.getPlaguesLocal();
        if (plagues.size() <= 0) {
            plagues = this.plaguesRepositoryRemote.getPlagues();
            this.plaguesRepositoryLocal.savePlaguesLocal(plagues);
        }
        return plagues;
    }

}
