package com.olivares.usecase.usecase.user;

import com.olivares.entities.User;
import com.olivares.usecase.repository.user.UserRepositoryLocal;
import com.olivares.usecase.repository.user.UserRepositoryRemote;

import javax.inject.Inject;

public class UpdateProfileUserUseCase {
    private final UserRepositoryRemote userRepositoryRemote;
    private final UserRepositoryLocal userRepositoryLocal;

    @Inject
    public UpdateProfileUserUseCase(UserRepositoryRemote userRepositoryRemote,
                                    UserRepositoryLocal userRepositoryLocal) {
        this.userRepositoryRemote = userRepositoryRemote;
        this.userRepositoryLocal = userRepositoryLocal;
    }

    public User updateProfileUser(User user) throws Exception {
        this.userRepositoryLocal.saveUser(user);
        return this.userRepositoryRemote.persistProfileUser(user);
    }
}
