package com.olivares.usecase.repository.photography;

import com.olivares.entities.Image;

public interface PhotographyRepositoryRemote {

    Image uploadPhotography(Image image) throws Exception;
}
