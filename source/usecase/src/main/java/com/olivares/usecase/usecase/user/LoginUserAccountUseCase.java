package com.olivares.usecase.usecase.user;

import com.olivares.entities.User;
import com.olivares.usecase.repository.user.UserRepositoryLocal;
import com.olivares.usecase.repository.user.UserRepositoryRemote;
import com.olivares.usecase.usecase.history.HideHistoryRecognitionUseCase;
import com.olivares.usecase.usecase.plagues.RemovePlagueUseCase;

import javax.inject.Inject;

public class LoginUserAccountUseCase {
    private final UserRepositoryRemote userRepositoryRemote;
    private final UserRepositoryLocal userRepositoryLocal;
    private final HideHistoryRecognitionUseCase hideHistoryRecognitionUseCase;
    private final RemovePlagueUseCase removePlagueUseCase;

    @Inject
    public LoginUserAccountUseCase(UserRepositoryRemote userRepositoryRemote,
                                   UserRepositoryLocal userRepositoryLocal,
                                   HideHistoryRecognitionUseCase hideHistoryRecognitionUseCase,
                                   RemovePlagueUseCase removePlagueUseCase) {
        this.userRepositoryRemote = userRepositoryRemote;
        this.userRepositoryLocal = userRepositoryLocal;
        this.hideHistoryRecognitionUseCase = hideHistoryRecognitionUseCase;
        this.removePlagueUseCase = removePlagueUseCase;
    }

    public User loginUser(User user) throws Exception {
        return this.userRepositoryRemote.loginUser(user);
    }

    public Boolean saveUserLogged(User user) throws Exception {
        return this.userRepositoryLocal.saveUser(user);
    }

    public Boolean logoutUser() throws Exception {
        this.hideHistoryRecognitionUseCase.deleteAllHistoryRecognitions();
        this.removePlagueUseCase.deleteAllPagues();
        return this.userRepositoryLocal.deleteAllUser();
    }
}
