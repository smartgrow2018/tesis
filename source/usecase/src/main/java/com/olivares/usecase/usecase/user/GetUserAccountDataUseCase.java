package com.olivares.usecase.usecase.user;

import com.olivares.entities.User;
import com.olivares.usecase.repository.user.UserRepositoryLocal;
import com.olivares.usecase.repository.user.UserRepositoryRemote;

import javax.inject.Inject;

public class GetUserAccountDataUseCase {
    private final UserRepositoryRemote userRepositoryRemote;
    private final UserRepositoryLocal userRepositoryLocal;

    @Inject
    public GetUserAccountDataUseCase(UserRepositoryRemote userRepositoryRemote, UserRepositoryLocal userRepositoryLocal) {
        this.userRepositoryRemote = userRepositoryRemote;
        this.userRepositoryLocal = userRepositoryLocal;
    }

    public User getUserAccount(String username) throws Exception {
        return this.userRepositoryRemote.getUserData(username);
    }

    public User getUserLogged() throws Exception {
        return this.userRepositoryLocal.getUser();
    }

}
