package com.olivares.usecase.usecase.history;

import android.util.Log;

import com.olivares.entities.Query;
import com.olivares.entities.User;
import com.olivares.usecase.repository.history.HistoryRecognitionRepositoryLocal;
import com.olivares.usecase.repository.history.HistoryRecognitionRepositoryRemote;
import com.olivares.usecase.usecase.user.GetUserAccountDataUseCase;
import com.olivares.usecase.util.UseCaseExcepcion;

import java.util.List;

import javax.inject.Inject;

public class GetHistoryRecognitionUseCase {
    private static final String TAG = GetHistoryRecognitionUseCase.class.getName();
    private final HistoryRecognitionRepositoryRemote historyRecognitionRepositoryRemote;
    private final HistoryRecognitionRepositoryLocal historyRecognitionRepositoryLocal;
    private final GetUserAccountDataUseCase getUserAccountDataUseCase;

    @Inject
    public GetHistoryRecognitionUseCase(HistoryRecognitionRepositoryRemote historyRecognitionRepositoryRemote,
                                        GetUserAccountDataUseCase getUserAccountDataUseCase,
                                        HistoryRecognitionRepositoryLocal historyRecognitionRepositoryLocal) {
        this.historyRecognitionRepositoryRemote = historyRecognitionRepositoryRemote;
        this.getUserAccountDataUseCase = getUserAccountDataUseCase;
        this.historyRecognitionRepositoryLocal = historyRecognitionRepositoryLocal;
    }

    public Boolean loadHistoryRecognition() throws Exception {
        try {
            User user = getUserAccountDataUseCase.getUserLogged();
            Log.d(TAG, "User query: " + user.getUsername());
            return this.historyRecognitionRepositoryLocal.
                    saveHistoryRecognitionLocal(
                            this.historyRecognitionRepositoryRemote.
                                    getHistoryRecognition(user.getUsername()));
        } catch (Exception e) {
            throw new UseCaseExcepcion(e.getMessage());
        }
    }

    public List<Query> getHistoryRecognition() throws Exception {
        return this.historyRecognitionRepositoryLocal.getHistoryRecognitionLocal();
    }

}
