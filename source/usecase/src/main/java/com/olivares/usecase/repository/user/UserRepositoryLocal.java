package com.olivares.usecase.repository.user;

import com.olivares.entities.User;

public interface UserRepositoryLocal {

    User getUser() throws Exception;

    Boolean saveUser(User user) throws Exception;

    Boolean deleteAllUser() throws Exception;
}
