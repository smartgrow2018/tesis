package com.olivares.usecase.repository.history;

import com.olivares.entities.Query;

import java.util.List;

public interface HistoryRecognitionRepositoryLocal {

    List<Query> getHistoryRecognitionLocal() throws Exception;

    Boolean saveHistoryRecognitionLocal(List<Query> historyQueries) throws Exception;

    Query hideQueryLocal(Query query) throws Exception;

    Boolean deleteAllHistoryRecognitions() throws Exception;
}