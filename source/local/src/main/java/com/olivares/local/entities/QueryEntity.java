package com.olivares.local.entities;

import com.olivares.entities.Query;
import com.olivares.entities.Result;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

public class QueryEntity extends RealmObject {
    @PrimaryKey
    private int queryId;
    private String urlImageQuery;
    private String date;
    private RealmList<ResultEntity> resultList;

    public QueryEntity() {
        // no require impl
    }

    public QueryEntity(Query query) {
        this.queryId = query.getQueryId();
        this.urlImageQuery = query.getUrlImageQuery();
        this.date = query.getDate();
        this.resultList = ResultEntity.toResultEntities(query.getResultList());
    }

    public static RealmList<QueryEntity> toQueryEntities(List<Query> queries) {
        RealmList<QueryEntity> queryEntities = new RealmList<>();
        for (Query query : queries) {
            queryEntities.add(new QueryEntity(query));
        }
        return queryEntities;
    }

    public static List<Query> toQueries(RealmResults<QueryEntity> queriesEntities) {
        List<Query> queries = new ArrayList<>();
        for (QueryEntity queryEntity : queriesEntities) {
            queries.add(toQuery(queryEntity));
        }
        return queries;
    }

    private static Query toQuery(QueryEntity queryEntity) {
        Query query = new Query();
        query.setQueryId(queryEntity.getQueryId());
        query.setUrlImageQuery(queryEntity.getUrlImageQuery());
        query.setDate(queryEntity.getDate());
        for (Result result : ResultEntity.toResults(queryEntity.getResultList())) {
            query.addResult(result);
        }
        return query;
    }

    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(int queryId) {
        this.queryId = queryId;
    }

    public String getUrlImageQuery() {
        return urlImageQuery;
    }

    public void setUrlImageQuery(String urlImageQuery) {
        this.urlImageQuery = urlImageQuery;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public RealmList<ResultEntity> getResultList() {
        return resultList;
    }
}
