package com.olivares.local.db;

import com.olivares.entities.Query;
import com.olivares.local.entities.QueryEntity;
import com.olivares.local.util.DbExcepcion;
import com.olivares.usecase.repository.history.HistoryRecognitionRepositoryLocal;

import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;

public class DbQueryRepository implements HistoryRecognitionRepositoryLocal {
    private final RealmConfiguration realmConfiguration;

    @Inject
    public DbQueryRepository(RealmConfiguration realmConfiguration) {
        this.realmConfiguration = realmConfiguration;
    }

    @Override
    public List<Query> getHistoryRecognitionLocal() throws Exception {
        try (Realm realm = Realm.getInstance(realmConfiguration)) {
            RealmResults<QueryEntity> queryEntities = realm.where(QueryEntity.class).findAll();
            queryEntities = queryEntities.sort("queryId", Sort.DESCENDING);
            return QueryEntity.toQueries(queryEntities);
        } catch (Exception e) {
            throw new DbExcepcion(e.getMessage());
        }
    }

    @Override
    public Boolean saveHistoryRecognitionLocal(List<Query> historyQueries) throws Exception {
        try (Realm realm = Realm.getInstance(realmConfiguration)) {
            final RealmList<QueryEntity> queryEntities = QueryEntity.toQueryEntities(historyQueries);
            realm.executeTransaction(realm1 -> realm1.insert(queryEntities));
            return true;
        } catch (Exception e) {
            throw new DbExcepcion(e.getMessage());
        }
    }

    @Override
    public Query hideQueryLocal(Query query) throws Exception {
        try (Realm instanceRealm = Realm.getInstance(realmConfiguration)) {
            instanceRealm.executeTransaction(realm -> {
                RealmResults<QueryEntity> queries = realm.where(QueryEntity.class)
                        .equalTo("queryId", query.getQueryId())
                        .findAll();
                queries.deleteAllFromRealm();
            });
            return query;
        } catch (Exception e) {
            throw new DbExcepcion(e.getMessage());
        }
    }

    @Override
    public Boolean deleteAllHistoryRecognitions() throws Exception {
        try (Realm realmInstance = Realm.getInstance(realmConfiguration)) {
            realmInstance.executeTransaction(realm -> realm.delete(QueryEntity.class));
            return true;
        } catch (Exception e) {
            throw new DbExcepcion(e.getMessage());
        }
    }
}
