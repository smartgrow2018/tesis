package com.olivares.local.entities;

import com.olivares.entities.Control;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

public class ControlEntity extends RealmObject {
    private int type;
    private String description;

    public ControlEntity() {
        // no require impl
    }

    public ControlEntity(Control control) {
        this.type = control.getType();
        this.description = control.getDescription();
    }

    public static RealmList<ControlEntity> toControlEntities(List<Control> controls) {
        RealmList<ControlEntity> controlEntities = new RealmList<>();
        for (Control control : controls) {
            controlEntities.add(new ControlEntity(control));
        }
        return controlEntities;
    }

    public static List<Control> toControls(RealmList<ControlEntity> controlEntities) {
        List<Control> controls = new ArrayList<>();
        for (ControlEntity controlEntity : controlEntities) {
            controls.add(toControl(controlEntity));
        }
        return controls;
    }

    public static Control toControl(ControlEntity controlEntity) {
        return new Control(controlEntity.getDescription(), controlEntity.getType());
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
