package com.olivares.local.prefs;

import android.content.SharedPreferences;

import com.olivares.usecase.repository.user.UserRepositoryPref;

import javax.inject.Inject;

public class PrefUserRepository implements UserRepositoryPref {
    private final SharedPreferences sharedPreferences;
    private static final String SEE_PLAGE_TUTORIAL = "SEE_PLAGE_TUTORIAL";
    private static final String SEE_HISTORY_TUTORIAL = "SEE_HISTORY_TUTORIAL";

    @Inject
    public PrefUserRepository(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public Boolean saveSeePlagueTutorial() throws Exception {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(SEE_PLAGE_TUTORIAL, false); // Storing boolean - true/false
        editor.commit();
        return true;
    }

    @Override
    public Boolean seePlagueTutorial() throws Exception {
        return sharedPreferences.getBoolean(SEE_PLAGE_TUTORIAL, true);
    }

    @Override
    public Boolean saveSeeHistoryTutorial() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(SEE_HISTORY_TUTORIAL, false); // Storing boolean - true/false
        editor.commit();
        return true;
    }

    @Override
    public Boolean seeHistoryTutorial() {
        return sharedPreferences.getBoolean(SEE_HISTORY_TUTORIAL, true);
    }

    @Override
    public Boolean deleteAllUserPref() throws Exception {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
        return true;
    }
}
