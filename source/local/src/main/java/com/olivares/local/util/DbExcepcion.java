package com.olivares.local.util;

public class DbExcepcion extends Exception {
    public DbExcepcion(String message) {
        super(message);
    }
}
