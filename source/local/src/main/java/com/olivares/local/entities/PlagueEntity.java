package com.olivares.local.entities;

import com.olivares.entities.Control;
import com.olivares.entities.Plague;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class PlagueEntity extends RealmObject {
    private int plagueId;
    private String name;
    private String description;
    private RealmList<ControlEntity> controlList;
    private String urlImage;
    private String nameClarifai;

    public PlagueEntity() {
        // no require impl
    }

    public PlagueEntity(Plague plague) {
        this.plagueId = plague.getPlagueId();
        this.name = plague.getName();
        this.description = plague.getDescription();
        this.urlImage = plague.getUrlImage();
        this.nameClarifai = plague.getNameClarifai();
        this.controlList = ControlEntity.toControlEntities(plague.getControlList());
    }

    public static List<Plague> toPlagues(RealmResults<PlagueEntity> plagueEntities) {
        List<Plague> plagues = new ArrayList<>();
        for (PlagueEntity plagueEntity : plagueEntities) {
            plagues.add(toPlague(plagueEntity));
        }
        return plagues;
    }

    public static RealmList<PlagueEntity> toPlagueEntities(List<Plague> plagues) {
        RealmList<PlagueEntity> plagueEntities = new RealmList<>();
        for (Plague plague : plagues) {
            plagueEntities.add(new PlagueEntity(plague));
        }
        return plagueEntities;
    }

    public static Plague toPlague(PlagueEntity plagueEntity) {
        Plague plague = new Plague();
        plague.setPlagueId(plagueEntity.getPlagueId());
        plague.setName(plagueEntity.getName());
        plague.setDescription(plagueEntity.getDescription());
        plague.setUrlImage(plagueEntity.getUrlImage());
        plague.setNameClarifai(plagueEntity.getNameClarifai());
        for (Control control : ControlEntity.toControls(plagueEntity.getControlList())) {
            plague.addControl(control);
        }
        return plague;
    }

    public int getPlagueId() {
        return plagueId;
    }

    public void setPlagueId(int plagueId) {
        this.plagueId = plagueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RealmList<ControlEntity> getControlList() {
        return controlList;
    }

    public void setControlList(RealmList<ControlEntity> controlList) {
        this.controlList = controlList;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getNameClarifai() {
        return nameClarifai;
    }

    public void setNameClarifai(String nameClarifai) {
        this.nameClarifai = nameClarifai;
    }
}
