package com.olivares.local.entities;

import com.olivares.entities.User;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class UserEntity extends RealmObject {

    @PrimaryKey
    private int id;
    private String username;
    private String password;
    private String description;
    private String name;
    private String urlImage;
    private String email;

    public UserEntity() {
        // no require impl
    }

    public UserEntity(int id, String name, String username, String password, String description) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.description = description;
    }

    public UserEntity(User user) {
        this.setId(user.getId());
        this.setName(user.getName());
        this.setUsername(user.getUsername());
        this.setPassword(user.getPassword());
        this.setDescription(user.getDescription());
    }

    public User toUser() {
        User user = new User();
        user.setId(this.getId());
        user.setName(this.getName());
        user.setUsername(this.getUsername());
        user.setPassword(this.getPassword());
        user.setDescription(this.getDescription());
        return user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
