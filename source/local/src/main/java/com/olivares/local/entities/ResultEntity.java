package com.olivares.local.entities;

import com.olivares.entities.Result;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

public class ResultEntity extends RealmObject {
    private double percentResult;
    private String namePlague;
    private String descriptionPlague;
    private String nameClarifai;
    private String urlImagePlague;
    private int plagueId;

    public ResultEntity() {
        // no require impl
    }

    public ResultEntity(Result result) {
        this.percentResult = result.getPercentResult();
        this.namePlague = result.getNamePlague();
        this.descriptionPlague = result.getDescriptionPlague();
        this.nameClarifai = result.getNameClarifai();
        this.urlImagePlague = result.getUrlImagePlague();
        this.plagueId = result.getPlagueId();
    }

    public static RealmList<ResultEntity> toResultEntities(List<Result> results) {
        RealmList<ResultEntity> resultEntities = new RealmList<>();
        for (Result result : results) {
            resultEntities.add(new ResultEntity(result));
        }
        return resultEntities;
    }

    public static List<Result> toResults(RealmList<ResultEntity> resultEntities) {
        List<Result> results = new ArrayList<>();
        for (ResultEntity resultEntity : resultEntities) {
            results.add(toResult(resultEntity));
        }
        return results;
    }

    public static Result toResult(ResultEntity resultEntity) {
        Result result = new Result();
        result.setPercentResult(resultEntity.getPercentResult());
        result.setNamePlague(resultEntity.getNamePlague());
        result.setDescriptionPlague(resultEntity.getDescriptionPlague());
        result.setNameClarifai(resultEntity.getNameClarifai());
        result.setUrlImagePlague(resultEntity.getUrlImagePlague());
        result.setPlagueId(resultEntity.getPlagueId());
        return result;
    }

    public double getPercentResult() {
        return percentResult;
    }

    public void setPercentResult(double percentResult) {
        this.percentResult = percentResult;
    }

    public String getNamePlague() {
        return namePlague;
    }

    public void setNamePlague(String namePlague) {
        this.namePlague = namePlague;
    }

    public String getDescriptionPlague() {
        return descriptionPlague;
    }

    public void setDescriptionPlague(String descriptionPlague) {
        this.descriptionPlague = descriptionPlague;
    }

    public String getNameClarifai() {
        return nameClarifai;
    }

    public void setNameClarifai(String nameClarifai) {
        this.nameClarifai = nameClarifai;
    }

    public String getUrlImagePlague() {
        return urlImagePlague;
    }

    public void setUrlImagePlague(String urlImagePlague) {
        this.urlImagePlague = urlImagePlague;
    }

    public int getPlagueId() {
        return plagueId;
    }

    public void setPlagueId(int plagueId) {
        this.plagueId = plagueId;
    }
}
